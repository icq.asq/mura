<?php
function routex($route, $params = [])
{
    if (!is_array($params)) {
        $params = [$params];
    }

    // Set the first parameter to App::getLocale()
    array_unshift($params, App::getLocale());
    return route($route, $params);
}