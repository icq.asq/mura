<?php
namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\Test;
use App\Models\TestHistory;
use App\Models\UserAnswer;
use Illuminate\Http\Request;
use App\Http\Requests\FrontUserUpdateRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{

    public function index() {
        $user = auth()->user();
        return view('account', compact('user'));
    }

    public function history() {
        $user = auth()->user();
        $user_id = $user->getAuthIdentifier();

        $userAnswers = UserAnswer::where('user_id', $user_id)->get();

        $result = [];
        $name = "name_".App::getLocale();
        if ($userAnswers) {

            foreach ($userAnswers as $answer) {
                $test = Test::with('lesson')->find($answer->test_id);

                $questions = Question::where('test_id', $test->id)->get();
                $userCorrectAnswers = UserAnswer::where('user_id', $user_id)->where('test_id', $test->id)->whereHas('answer', function($query) {
                    $query->correct();
                })->get();

                $result[] = [
                    'lesson_name' => $test->lesson->$name,
                    'balls' => $userCorrectAnswers->sum('ball'),
                    'total' => $questions->count()
                    ];

            }
        }

        dd($result);

        return view('office', compact('result', 'user'));
    }

    public function feedback() {
        $user = auth()->user();
        return view('feedback', compact('user'));
    }

    public function feedbackForm() {
        $user = auth()->user();
        return view('feedback_form', compact('user'));
    }

    public function edit() {
        $user = auth()->user();
        return view('personal', compact('user'));
    }

    public function editPost(FrontUserUpdateRequest $request) {
        $hasher = app('hash');
        if ($hasher->check($request->password, auth()->user()->password)) {
            // Success
            $user = auth()->user();
            $user->region = $request->region;
            /*if (!$user->INN) {
                $user->INN = $request->INN;
            }*/
            if (!$user->fio() && $user->fio() != "") {
                $fio = explode(" ", $request->fio);
                $user->surname = $fio[0] ?? "";
                $user->name = $fio[1] ?? "";
                $user->patronymic = $fio[2] ?? "";
            }
            if (!$user->birth_date) {
                $user->birth_date = $request->birth_date;
            }
            $user->phone_number = $request->phone_number;
            $user->city = $request->city;
            $user->class = $request->class;
           // $user->user_type = $request->user_type;
            if ($request->file('avatar')) {
                $user->avatar = "data:image/png;base64," . base64_encode(file_get_contents($request->file('avatar')));
            }
            $user->save();
            return redirect(routex('personal'));
        } else {
            return redirect()->back()->withErrors(['password' => __('validation.password')]);
        }
    }

    public function adminLogin()
    {
        return view('admin.login');
    }

    public function isAdmin(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_admin' => 1])) {
            return redirect()->route('main');
        }
        else return redirect()->route('admin.login')->withErrors(['Неверный логин или пароль'], 'error');
    }
}

