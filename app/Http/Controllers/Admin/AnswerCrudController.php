<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AnswerRequest;
use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\AnswerDescription;
use App\Models\Answer;


/**
 * Class AnswerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AnswerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        $this->crud->denyAccess(['create']);
        CRUD::setModel(\App\Models\Answer::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/answer');
        CRUD::setEntityNameStrings(__('admin.answer'), __('admin.answers'));
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'Label' => __('admin.answer name'),
            'name' => __('admin.title'),
            'type'     => 'closure',
            'function' => function($entry) {
                return AnswerDescription::where([
                    ["answer_id", $entry->id],
                    ["language", config("app.locale")],
                ])->first()->text ?? __('admin.not found');
            }
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AnswerRequest::class);
        
        CRUD::addField([
            'type' => 'checkbox',
            'name' => 'is_correct',
            'label' => __('admin.correct')
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

        $id = $this->crud->getCurrentEntry()->id;
        foreach(config("app.languages") as $locale_key => $locale) {
            CRUD::addField([
                'type' => 'text',
                'label' => __('admin.name ') . $locale,
                'name' => 'name_' . $locale_key,
                'default' => AnswerDescription::where([
                    ['answer_id', $id],
                    ['language', $locale_key]
                ])->first()->text ?? ""
            ]);
        }
    }

    public function update(Request $request) {
        foreach(config("app.languages") as $locale_key => $locale) {
            $desc = AnswerDescription::firstOrNew([
                'answer_id' => $request->id, 
                'language' => $locale_key
            ]);
            $desc->answer_id = $request->id;
            $desc->text = $request->{ 'name_' . $locale_key };
            $desc->language = $locale_key;
            $desc->save();
        }
        return $this->traitUpdate($request);
    }
}
