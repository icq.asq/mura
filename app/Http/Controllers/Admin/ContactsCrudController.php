<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ContactsRequest;
use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\ContactsDescription;


/**
 * Class ContactsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ContactsCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Contacts::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/contacts');
        CRUD::setEntityNameStrings(__('admin.contact'), __('admin.contacts'));
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'Label' => __('admin.сontact name'),
            'name' => __('admin.title'),
            'type'     => 'closure',
            'function' => function($entry) {
                $descr = ContactsDescription::where("contacts_id", $entry->id)->first();
                return $descr->text ?? __('admin.not found');
            }
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ContactsRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

        $id = $this->crud->getCurrentEntry()->id;
        foreach(config("app.languages") as $locale_key => $locale) {
            CRUD::addField([
                'type' => 'text',
                'label' => __('admin.name ') . $locale,
                'name' => 'name_' . $locale_key,
                'default' => ContactsDescription::where([
                    ['contacts_id', $id],
                    ['language', $locale_key]
                ])->first()->text ?? ""
            ]);
        }
    }

    public function update(Request $request) {
        foreach(config("app.languages") as $locale_key => $locale) {
            $desc = ContactsDescription::firstOrNew([
                'contacts_id' => $request->id, 
                'language' => $locale_key
            ]);
            $desc->contacts_id = $request->id;
            $desc->text = $request->{ 'name_' . $locale_key };
            $desc->language = $locale_key;
            $desc->save();
        }
        return $this->traitUpdate($request);
    }
}
