<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostsRequest;
use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\PostContents;

/**
 * Class PostsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PostsCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Posts::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/posts');
        CRUD::setEntityNameStrings(__('admin.post'), __('admin.posts'));
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'Label' => __('admin.posts name'),
            'name' => __('admin.title'),
            'type'     => 'closure',
            'function' => function($entry) {
                return PostContents::where([
                    ["posts_id", $entry->id],
                    ["language", config("app.locale")],
                ])->first()->title ?? __('admin.not found');
                //return $entry->description_by_lang(config("app.locale"))->first()->contents->title ?? __('admin.not found');
            }
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PostsRequest::class);

        CRUD::addField([
            'name'      => 'image',
            'label'     => __('admin.image'),
            'type'      => 'image',
            'prefix' => '/',
            
            'disk'   => 'public_root', 
            'crop' => true, // set to true to allow cropping, false to disable
            // 'aspect_ratio' => 1, 
        ]);

        $id = $this->crud->getCurrentEntry()->id ?? null;
        foreach(config("app.languages") as $locale_key => $locale) {
            CRUD::addField([
                'type' => 'text',
                'label' => __('admin.name ') . $locale,
                'name' => 'title_' . $locale_key,
                'default' => PostContents::where([
                    ['posts_id', $id],
                    ['language', $locale_key]
                ])->first()->title ?? ""
            ]);

            CRUD::addField([
                'type' => 'text',
                'label' => __('admin.content ') . $locale,
                'name' => 'content_' . $locale_key,
                'default' => PostContents::where([
                    ['posts_id', $id],
                    ['language', $locale_key]
                ])->first()->content ?? ""
            ]);
        }
        
        // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function update(Request $request) {
        foreach(config("app.languages") as $locale_key => $locale) {
            $desc = PostContents::firstOrNew([
                'posts_id' => $request->id, 
                'language' => $locale_key
            ]);
            $desc->posts_id = $request->id;
            $desc->title = $request->{ 'title_' . $locale_key };
            $desc->content = $request->{ 'content_' . $locale_key };
            $desc->language = $locale_key;
            $desc->save();
        }
        return $this->traitUpdate($request);
    }

    public function store(Request $request) {
        $response = $this->traitStore($request);
        foreach(config("app.languages") as $locale_key => $locale) {
            $desc = PostContents::firstOrNew([
                'posts_id' => $this->data['entry']->id, 
                'language' => $locale_key
            ]);
            $desc->posts_id = $this->data['entry']->id;
            $desc->title = $request->{ 'title_' . $locale_key };
            $desc->content = $request->{ 'content_' . $locale_key };
            $desc->language = $locale_key;
            $desc->save();
        }
        return $response;
    }
}
