<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\QuestionRequest;
use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\Answer;
use App\Models\AnswerDescription;
use App\Models\Question;
use App\Models\QuestionDescription;

/**
 * Class QuestionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class QuestionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        $this->crud->denyAccess(['create']);
        CRUD::setModel(\App\Models\Question::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/question');
        CRUD::setEntityNameStrings( __('admin.question'), __('admin.questions'));
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'Label' => __('admin.question name'),
            'name' => __('admin.title'),
            'type'     => 'closure',
            'function' => function($entry) {
                return QuestionDescription::where([
                    ["question_id", $entry->id],
                    ["language", config("app.locale")],
                ])->first()->text ?? "Not found";
                return $entry->description_by_lang(config("app.locale"))->first()->description->text ?? __('admin.not found');
            }
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(QuestionRequest::class);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

        $id = $this->crud->getCurrentEntry()->id;
        foreach(config("app.languages") as $locale_key => $locale) {
            CRUD::addField([
                'type' => 'text',
                'label' => __('admin.name ') . $locale,
                'name' => 'name_' . $locale_key,
                'default' => QuestionDescription::where([
                    ['question_id', $id],
                    ['language', $locale_key]
                ])->first()->text ?? "",
                'tab' => __('admin.tab_main'),
            ]);
        }

        $fields = [
            [
                'name'    => 'name_ru',
                'type'    => 'text',
                'label'   => 'name ru',
                'wrapper' => ['class' => 'form-group col-md-4'],
            ],
            [
                'name'    => 'name_kz',
                'type'    => 'text',
                'label'   => 'name kz',
                'wrapper' => ['class' => 'form-group col-md-4'],
            ],
            [
                'name'    => 'is_correct',
                'type'    => 'checkbox',
                'label'   => 'Is correct',
                'wrapper' => ['class' => 'form-group col-md-4 d-flex align-items-center mt-4 pt-1'],
            ],
            [
                'name'    => 'visit_link',
                'type'    => 'visit_link',
                'label'   => 'Edit',
                'label_link' => __('admin.edit'),
            ],
        ];

        /*
        [
            {"name_ru":"dsdasd","name_kz":"sadasdas","visit_link":"hello"},
            {"name_ru":"dasda","name_kz":"sdsadas","visit_link":"hello"}
        ]
        */
        $tests = Answer::where([
            ['question_id', $id],
        ])
            ->with('description')
            ->get();

        $default = [];
        foreach ($tests as $test) {
            $row = [
                "visit_link" => "/admin/answer/" . $test->id . "/edit",
                "name_ru" => "",
                "name_kz" => "",
                "is_correct" => $test->is_correct
            ];
            if (isset($test->description)) {
                foreach ($test->description as $description) {
                    if ($description->language == 'ru') {
                        $row['name_ru'] = $description->text;
                    }
                    if ($description->language == 'kz') {
                        $row['name_kz'] = $description->text;
                    }
                }
            }
            $default[] = $row;
        }
        CRUD::addField([   // repeatable
            'name'  => 'relations',
            'label' => 'Testimonials',
            'type'  => 'repeatable',
            'fields' => $fields,
            'default' => $default,

            // optional
            'tab' => __('admin.tab_relations'),
            'new_item_label'  => __('admin.add'), // customize the text of the button
        ]);
    }

    public function update(Request $request) {
        $data = \json_decode($request->input('relations'));
        $questions = Answer::where([
            ['question_id', $request->id],
            ])
            ->get();
        foreach ($questions as $question) {
            $question->delete();
        }
        foreach($data as $row) {
            $question = new Answer();
            $question->question_id = $request->id;
            $question->is_correct = $row->is_correct;
            $question->save();
            
            $question_ru = new AnswerDescription();
            $question_ru->answer_id = $question->id;
            $question_ru->text = $row->name_ru;
            $question_ru->language = 'ru';
            $question_ru->save();
            
            $question_kz = new AnswerDescription();
            $question_kz->answer_id = $question->id;
            $question_kz->text = $row->name_kz;
            $question_kz->language = 'kz';
            $question_kz->save();
        }

        foreach(config("app.languages") as $locale_key => $locale) {
            $desc = QuestionDescription::firstOrNew([
                'question_id' => $request->id, 
                'language' => $locale_key
            ]);
            $desc->question_id = $request->id;
            $desc->text = $request->{ 'name_' . $locale_key };
            $desc->language = $locale_key;
            $desc->save();
        }
        return $this->traitUpdate($request);
    }
}
