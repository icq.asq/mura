<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TestRequest;
use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\Test;
use App\Models\TestDescription;
use App\Models\Question;
use App\Models\QuestionDescription;

/**
 * Class TestCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TestCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Test::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/test');
        CRUD::setEntityNameStrings(__('admin.test'), __('admin.tests'));
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'Label' => __('admin.test name'),
            'name' => __('admin.title'),
            'type'     => 'closure',
            'function' => function($entry) {
                return TestDescription::where([
                    ["test_id", $entry->id],
                    ["language", config("app.locale")],
                ])->first()->text ?? __('admin.not found');
            }
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(TestRequest::class);

        CRUD::addField([
            'name' => 'finale_date',
            'type' => 'date_picker',
            'label' => __('admin.end date'),
            'date_picker_options' => [
               'todayBtn' => 'linked',
               'format' => 'dd-mm-yyyy',
               'language' => 'ru'
            ],
            'tab' => __('admin.tab_main'),
        ]);        

        $id = $this->crud->getCurrentEntry()->id ?? null;
        foreach(config("app.languages") as $locale_key => $locale) {
            CRUD::addField([
                'type' => 'text',
                'label' => __('admin.name ') . $locale,
                'name' => 'name_' . $locale_key,
                'default' => $id ? TestDescription::where([
                    ['test_id', $id],
                    ['language', $locale_key]
                ])->first()->text ?? "" : "",
                'tab' => __('admin.tab_main'),
            ]);
        }


        $fields = [
            [
                'name'    => 'name_ru',
                'type'    => 'text',
                'label'   => 'Name',
                'wrapper' => ['class' => 'form-group col-md-4'],
            ],
            [
                'name'    => 'name_kz',
                'type'    => 'text',
                'label'   => 'Position',
                'wrapper' => ['class' => 'form-group col-md-4'],
            ],
        ];
        CRUD::addField([   // repeatable
            'name'  => 'testimonials',
            'label' => 'Testimonials',
            'type'  => 'repeatable',
            'fields' => $fields,
        
            // optional
            'tab' => __('admin.tab_relations'),
            'new_item_label'  => 'Add Group', // customize the text of the button
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(TestRequest::class);

        // CRUD::addField([
        //     'name' => 'finale_date',
        //     'type' => 'date_picker',
        //     'label' => __('admin.end date'),
        //     'date_picker_options' => [
        //        'todayBtn' => 'linked',
        //        'format' => 'dd-mm-yyyy',
        //        'language' => 'ru'
        //     ],
        //     'tab' => __('admin.tab_main'),
        // ]);        

        $id = $this->crud->getCurrentEntry()->id ?? null;
        foreach(config("app.languages") as $locale_key => $locale) {
            CRUD::addField([
                'type' => 'text',
                'label' => __('admin.name ') . $locale,
                'name' => 'name_' . $locale_key,
                'default' => $id ? TestDescription::where([
                    ['test_id', $id],
                    ['language', $locale_key]
                ])->first()->text ?? "" : "",
                'tab' => __('admin.tab_main'),
            ]);
        }

        $fields = [
            [
                'name'    => 'name_ru',
                'type'    => 'text',
                'label'   => 'name ru',
                'wrapper' => ['class' => 'form-group col-md-4'],
            ],
            [
                'name'    => 'name_kz',
                'type'    => 'text',
                'label'   => 'name kz',
                'wrapper' => ['class' => 'form-group col-md-4'],
            ],
            [
                'name'    => 'visit_link',
                'type'    => 'visit_link',
                'label'   => 'Edit',
                'label_link' => __('admin.edit'),
            ],
        ];

        /*
        [
            {"name_ru":"dsdasd","name_kz":"sadasdas","visit_link":"hello"},
            {"name_ru":"dasda","name_kz":"sdsadas","visit_link":"hello"}
        ]
        */
        $tests = Question::where([
            ['test_id', $id],
        ])
            ->with('description')
            ->get();

        $default = [];
        foreach ($tests as $test) {
            $row = [
                "visit_link" => "/admin/question/" . $test->id . "/edit",
                "name_ru" => "",
                "name_kz" => ""
            ];
            if (isset($test->description)) {
                foreach ($test->description as $description) {
                    if ($description->language == 'ru') {
                        $row['name_ru'] = $description->text;
                    }
                    if ($description->language == 'kz') {
                        $row['name_kz'] = $description->text;
                    }
                }
            }
            $default[] = $row;
        }
        CRUD::addField([   // repeatable
            'name'  => 'relations',
            'label' => 'Testimonials',
            'type'  => 'repeatable',
            'fields' => $fields,
            'default' => $default,

            // optional
            'tab' => __('admin.tab_relations'),
            'new_item_label'  => __('admin.add'), // customize the text of the button
        ]);
        // $this->setupCreateOperation();
    }

    public function update(Request $request) {
        $data = \json_decode($request->input('relations'));
        $questions = Question::where([
            ['test_id', $request->id],
            ])
            ->get();
        foreach ($questions as $question) {
            $question->delete();
        }
        foreach($data as $row) {
            $question = new Question();
            $question->test_id = $request->id;
            $question->save();
            
            $question_ru = new QuestionDescription();
            $question_ru->question_id = $question->id;
            $question_ru->text = $row->name_ru;
            $question_ru->language = 'ru';
            $question_ru->save();
            
            $question_kz = new QuestionDescription();
            $question_kz->question_id = $question->id;
            $question_kz->text = $row->name_kz;
            $question_kz->language = 'kz';
            $question_kz->save();
        }


        foreach(config("app.languages") as $locale_key => $locale) {
            $desc = TestDescription::firstOrNew([
                'test_id' => $request->id, 
                'language' => $locale_key
            ]);
            $desc->test_id = $request->id;
            $desc->text = $request->{ 'name_' . $locale_key };
            $desc->language = $locale_key;
            $desc->save();
        }
        return $this->traitUpdate($request);
    }

    public function store(Request $request) {
        $response = $this->traitStore($request);
        foreach(config("app.languages") as $locale_key => $locale) {
            $desc = TestDescription::firstOrNew([
                'test_id' => $this->data['entry']->id, 
                'language' => $locale_key
            ]);
            $desc->test_id = $this->data['entry']->id;
            $desc->text = $request->{ 'name_' . $locale_key };
            $desc->language = $locale_key;
            $desc->save();
        }
        return $response;
    }
}
