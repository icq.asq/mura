<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\UserRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

use Illuminate\Support\Facades\Hash;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\User::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/user');
        CRUD::setEntityNameStrings(__('admin.user'), __('admin.users'));
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns
        $this->crud->addColumn(['name' => 'name', 'type' => 'text', 'label' => __('admin.title')]);
        $this->crud->addColumn(['name' => 'email', 'type' => 'email', 'label' => 'Email']);
        $this->crud->addColumn(['name' => 'is_admin', 'type' => 'boolean', 'label' => __('admin.is admin')]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(UserRequest::class);

        $this->crud->addField(['name' => 'name', 'type' => 'text', 'label' => __('admin.Name')]);
        $this->crud->addField(['name' => 'surname', 'type' => 'text', 'label' => __('admin.surname')]);
        $this->crud->addField(['name' => 'patronymic', 'type' => 'text', 'label' => __('admin.patronymic')]);
        $this->crud->addField(['name' => 'INN', 'type' => 'text', 'label' => __('admin.INN')]);
        $this->crud->addField(['name' => 'city', 'type' => 'text', 'label' => __('admin.city')]);
        $this->crud->addField(['name' => 'region', 'type' => 'text', 'label' => __('admin.region')]);
        $this->crud->addField(['name' => 'phone_number', 'type' => 'text', 'label' => __('admin.phone')]);
        $this->crud->addField([
            'name'        => 'user_type',
            'type'        => 'select2_from_array',
            'label'       => __('admin.user type'),
            'allows_null' => false,
            'default'     => 1,
            'options'     => [
                1 => __('admin.school'),
                2 => __('admin.student'),
                3 => __('admin.parent'),
                4 => __('admin.elder'),
                5 => __('admin.specialist'),
            ]
        ]);
        $this->crud->addField([
            'name' => 'birth_date',
            'type' => 'date_picker',
            'label' => __('admin.birth date'),
            'date_picker_options' => [
               'todayBtn' => 'linked',
               'format' => 'dd-mm-yyyy',
               'language' => 'ru'
            ],
         ]);
        CRUD::addField([
            'name'      => 'avatar', // The db column name
            'label'     => __('admin.image'), // Table column heading
            'type'      => 'image',
            'prefix' => '/',
            // image from a different disk (like s3 bucket)
            'disk'   => 'public_root', 
            'crop' => true, // set to true to allow cropping, false to disable
            // 'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
        ]);
        $this->crud->addField(['name' => 'email', 'type' => 'email', 'label' => 'Email']);
        $this->crud->addField(['name' => 'email_verified_at', 'type' => 'datetime', 'label' => __('admin.email verified')]);
        $this->crud->addField(['name' => 'password', 'type' => 'password', 'label' => __('admin.password')]);
        $this->crud->addField(['name' => 'is_admin', 'type' => 'checkbox', 'label' => __('admin.is admin')]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store(UserStoreRequest $request)
    {
        $this->crud->hasAccessOrFail('create');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        // insert item in the db
        $stripped_request = $this->crud->getStrippedSaveRequest();
        $stripped_request['password'] = Hash::make($request->input('password'));

        $item = $this->crud->create($stripped_request);
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }

    public function update(UserUpdateRequest $request)
    {
        $this->crud->hasAccessOrFail('update');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();
        // update the row in the db

        $stripped_request = $this->crud->getStrippedSaveRequest();
        $stripped_request['password'] = Hash::make($request->input('password'));

        $item = $this->crud->update(
            $request->get($this->crud->model->getKeyName()),
            $stripped_request
            // $this->crud->getStrippedSaveRequest()
        );
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }
}
