<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\BannerType;
use App\Services\ImageService;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    private $imageService;
    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::all();
        return view('admin.banners_all', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $types = BannerType::all();
        return view('admin.banner', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'type_id'  => $request->input('type_id'),
            'image_id' => $request->has('img') ?
                $this->imageService->upload($request->file('img'))->image()->id:"",
        ]);

        $banner = Banner::create($request->all());
        if ($banner){
            $banners = Banner::all();
            return redirect()->route('banners.index', compact('banners'));
        }
        else{
            redirect()->back()->withErrors(['Ошибка']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::find($id);
        $types = BannerType::all();
        return view('admin.banner', compact('banner', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banner = Banner::find($id);

        if ($banner) {

            $data = [];
            if ($request->has('img')) {
                $data['image_id'] = $this->imageService->upload($request->file('img'))->image()->id;
            }
            $data['type_id'] = $request->type_id;

            $banner->update($data);

            $banners = Banner::all();
            return redirect()->route('banners.index', compact('banners'));

        }
        else{
            redirect()->back()->withErrors(['Ошибка']);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::find($id);
        if ($banner) {
            $banner->image->delete();
            $banner->delete();
        }
        $banners = Banner::all();
        return redirect()->route('banners.index', compact('banners'));

    }
}
