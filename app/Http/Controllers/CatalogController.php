<?php
namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Category;
use App\Models\TestHistory;
use Illuminate\Http\Request;
use App\Http\Requests\FrontUserUpdateRequest;
use Illuminate\Support\Facades\App;

class CatalogController extends Controller
{
    public function about () {
        $categories = Category::all();
        $banners = Banner::where('type_id', 1)->get();
        $locale = App::getLocale();
        return view("about")->with(compact('categories', 'locale', 'banners'));
    }

    public function etiquette () {
        return view("etiquette");
    }

    public function help () {
        $banners = Banner::where('type_id', 2)->get();

        return view("help", compact('banners'));
    }

    public function patriot () {
        return view("patriot");
    }

    public function usefull () {
        return view("usefull.usefull");
    }

    public function usefullMusical () {
        return view("usefull.musical");
    }
}