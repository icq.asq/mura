<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Theme;
use Illuminate\Support\Facades\App;

class CategoryController extends Controller
{

    public function showCategory($locale = null, $category, $class = null)
    {

        $subcategories = Subcategory::where('category_id', $category)->get();

        if (!empty($class)) {
            $themes = Theme::orderBy('queue')->get();
            $name = "name_".App::getLocale();
            $image = "image_".App::getLocale();
            $url = ['category' => $category, 'class' => $class];
            return view('first_category', compact('themes', 'name', 'image', 'url'));
        }
        return view('first_category_class_selector', compact('subcategories'));

    }

}
