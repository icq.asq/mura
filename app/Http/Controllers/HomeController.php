<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function main()
    {
        // session()->flash('notification', __('main.registerSuccess'));
        $categories = Category::all();
        $banners = Banner::where('type_id', 1)->get();
        $locale = App::getLocale();
        return view('welcome')->with(compact('categories', 'locale', 'banners'));
    }

    public function showAdmin()
    {
        return view('admin.index');
    }
}
