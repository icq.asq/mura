<?php

namespace App\Http\Controllers;

use App\Models\Lesson;
use App\Models\Media;
use App\Models\Quarter;
use App\Models\Question;
use App\Models\Quote;
use App\Models\Subcategory;
use App\Models\Test;
use App\Models\Theme;
use App\Models\UserAnswer;
use App\Models\Year;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class LessonController extends Controller
{

    public function showLesson($locale = null,$category, $class, $theme_id, $lesson = null)
    {

        $lesson = Lesson::where('theme_id', $theme_id)->where('subcategory_id', $class)->first();
       // dd($lesson);
        $quotes = Quote::where('theme_id', $theme_id)->get();
        $theme = Theme::find($theme_id);
        //$locale = App::getLocale();
        $themeName = "name_".$locale;
        $media = Media::where('theme_id', $theme_id)->where('locale', $locale)->where('subcategory_id', $class)->get();
        //dd($media);
        if ($lesson){
            $test = Test::where('lesson_id', $lesson->id)->first();
        }
        else {
            $test = false;
        }

        $doTest = true;
        $hasTest = false;
        $wrongClass = false;
        if(Auth::check()){
            $user_id = Auth::user()->getAuthIdentifier();
            if ($test){
                $answers = UserAnswer::where('user_id', $user_id)->where('test_id', $test->id);
                $questions = Question::where('test_id', $test->id);

                if($answers->count() > 0 && $answers->count() < $questions->count()){
                    $doTest = $answers->count();
                } elseif ($answers->count() == $questions->count()) {
                    $doTest = false;
                }
                else $doTest = true;

                if (Auth::user()->class != $test->subcategory_id)
                    $wrongClass = true;
            }
        }

        if ($test)
            $hasTest = true;

        if ($theme_id == 6)
            return view('first.patriot', compact('theme', 'themeName', 'locale') );
        return view('first.first_musical', compact('lesson', 'quotes', 'theme', 'themeName','locale', 'media', 'class', 'doTest', 'hasTest', 'wrongClass') );

    }

    public function getLessons(Request $request)
    {
        $themes = Lesson::where('theme_id', $request['theme_id'])->where('subcategory_id', $request['class_id'])->get();
        return json_encode($themes);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lessons = Lesson::all();
        return view('admin.lessons_all', compact('lessons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $themes = Theme::all();
        $classes = Subcategory::all();
        $quarters = Quarter::all();
        $years = Year::all();
        return view('admin.create_lesson', compact('themes', 'classes', 'quarters', 'years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $data['src_ru'] = substr($data['src_ru'], -11, 11);
        $data['src_kz'] = substr($data['src_kz'], -11, 11);

        $lesson = Lesson::create($data);
        $lessons = Lesson::all();
        return redirect()->route('lessons.index', compact('lessons'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $themes = Theme::all();
        $classes = Subcategory::all();
        $quarters = Quarter::all();
        $years = Year::all();
        $lesson = Lesson::find($id);
        return view('admin.create_lesson', compact('themes', 'classes', 'quarters', 'years', 'lesson'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lesson = Lesson::find($id);
        $data = $request->all();

        $data['src_ru'] = empty($data['src_ru'])?$lesson->src_ru:substr($data['src_ru'], -11, 11);
        $data['src_kz'] = empty($data['src_kz'])?$lesson->src_ru:substr($data['src_kz'], -11, 11);


        $lesson->update($data);

        return redirect()->route('lessons.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lesson::find($id)->delete();
        return redirect()->route('lessons.index');
    }

}
