<?php

namespace App\Http\Controllers;

use App\Models\Media;
use App\Models\Subcategory;
use App\Models\Theme;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medias = Media::all();
        return view('admin.media_all', compact('medias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $themes = Theme::all();
        $classes = Subcategory::all();
        return view('admin.create_media', compact('themes', 'classes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['src'] = substr($data['src'], -11, 11);
        $media = Media::create($data);
        $medias = Media::all();

        return redirect()->route('media.index', compact('medias'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $themes = Theme::all();
        $classes = Subcategory::all();
        $media = Media::find($id);
        return view('admin.create_media', compact('themes', 'classes', 'media'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $media = Media::find($id);

        $data['src'] = empty($data['src'])?$media->src:substr($data['src'], -11, 11);

        $media->update($data);
        $medias = Media::all();

        return redirect()->route('media.index', compact('medias'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Media::find($id)->delete();
        return redirect()->route('media.index');
    }
}
