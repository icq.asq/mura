<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $test_id = $data['test_id'];

        if(!isset($data['is_correct_1']) && !isset($data['is_correct_2']) && !isset($data['is_correct_3'])){
            $error = "Вы не отметили верный ответ!";
            return view('admin.create_question', compact('test_id', 'error', 'data'));
        }

        $question = Question::create([
                'test_id' => $data['test_id'],
                'answers_number' => $data['answers_number'],
                'ball' => $data['ball'],
                'text_ru' => $data['text_ru'],
                'text_kz' => $data['text_kz'],
            ]
        );

        $answers = [];

        for ($i = 1; $i < 4; $i++){

            $is_correct = isset($data['is_correct_'.$i])?1:0;

            $answers[] = Answer::create([
               'question_id' => $question->id,
               'is_correct' => $is_correct,
               'text_ru' => $data['answer_text_ru_'.$i],
               'text_kz' => $data['answer_text_kz_'.$i],
            ]);

        }

        return view('admin.create_question', compact('test_id'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
