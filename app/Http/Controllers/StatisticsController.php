<?php

namespace App\Http\Controllers;

use App\Models\Quarter;
use App\Models\Subcategory;
use App\Models\Theme;
use App\Models\User;
use App\Models\UserAnswer;
use App\Models\Year;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{

    protected $begin_date;
    protected $end_date;

    public function __construct()
    {
        $this->begin_date = date('2020-01-01 00:00:00');
        $this->end_date = date('Y-m-d 23:59:59');
    }

    public function getRegister(Request $request)
    {

        $begin = (!empty($request->begin)?$request->begin." 00:00:00":$this->begin_date);
        $end = (!empty($request->end)?$request->end." 23:59:59":$this->end_date);

        //dd($begin." ".$end);

        $users = User::byRegister($begin, $end)->select(DB::raw('count(*) as num, class'))
            ->groupBy('class')->get();

        return view('admin.statistics.general', compact('users'));


    }

    public function getBalls(Request $request)
    {

        //$begin = (!empty($request->begin)?$request->begin." 00:00:00":$this->begin_date);
        //$end = (!empty($request->end)?$request->end." 23:59:59":$this->end_date);

        $year = (!empty($request->year))?$request->year:1;

        //$balls = UserAnswer::byRegister($begin, $end);

        $balls = UserAnswer::byYear($year);

        if (!empty($request->quarter)){
            $balls = $balls->byQuarter($request->quarter);
        }

        if (!empty($request->class)){
            $balls = $balls->byClass($request->class);
        }

        if (!empty($request->theme)){
            $balls = $balls->leftjoin('tests', 'tests.id', '=', 'users_answers.test_id')->
                leftjoin('lessons', 'lessons.id', '=', 'tests.lesson_id')->
                    where('lessons.theme_id', $request->theme);
        }

        if (!empty($request->user)){
            $balls = $balls->byUser($request->user);
        }

       $balls = $balls->groupBy('user_id')->selectRaw('user_id, sum(ball) as balls')->
            with('user')->orderBy('balls', 'DESC');


        if (!empty($request->num) && empty($request->user)){
            $balls = $balls->limit(intval($request->num));
        }

       $balls = $balls->get();

       $themes = Theme::all();
       $classes = Subcategory::all();
       $quarters = Quarter::all();
       $years = Year::all();
        return view('admin.statistics.balls', compact('balls', 'themes', 'classes', 'quarters', 'years'));

    }


}
