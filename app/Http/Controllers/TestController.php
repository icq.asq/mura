<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use App\Models\Subcategory;
use App\Models\Test;
use App\Models\Theme;
use App\Models\UserAnswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{

    public function showTest($locale = false, $class, $lesson, $num )
    {
        $user_id = Auth::user()->getAuthIdentifier();

        $test = Test::where('lesson_id', $lesson)->where('subcategory_id', $class)->first();

        $questions = Question::where('test_id', $test->id)->get();
        $question = $questions[$num];

        $answers = Answer::where('question_id', $question->id)->get();

        $text = "text_".App::getLocale();

        $num_next = ++$num;
        $count = $questions->count();
        if ($num_next == $questions->count())
            $finish = true;
        else $finish = false;

        return view('test', compact('user_id', 'test', 'text', 'question', 'num_next', 'finish', 'count', 'answers'));
    }

    public function finishTest($locale = false, $class, $lesson)
    {
        $test = Test::where('lesson_id', $lesson)->where('subcategory_id', $class)->first();
        $user_id = Auth::user()->getAuthIdentifier();

        $questions = Question::where('test_id', $test->id)->get();
        $userCorrectAnswers = UserAnswer::where('user_id', $user_id)->where('test_id', $test->id)->whereHas('answer', function($query) {
            $query->correct();
        })->get();

        $total = [
            'all' => $questions->count(),
            'correct' => $userCorrectAnswers->count(),
            'balls' => $userCorrectAnswers->sum('ball')
        ];

        $text = "text_".App::getLocale();
        return view('test', compact('test', 'total', 'text'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $themes = Theme::all();
        $classes = Subcategory::all();
        return view('admin.create_test', compact('themes', 'classes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $test = Test::create($request->all());
        $test_id = $test->id;
        return view('admin.create_question', compact('test_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
