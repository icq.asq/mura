<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use App\Models\UserAnswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class UserAnswerController extends Controller
{
    public function saveAnswer(Request $request)
    {
        $data = $request->all();

        $user_id = Auth::user()->getAuthIdentifier();

        $answer = Answer::find($data['answer_id']);
        if ($answer->is_correct)
            $ball = Question::find($data['question_id'])->ball;
        else $ball = 0;

        $checkUserAnswer = UserAnswer::where('question_id', $data['question_id'])->where('user_id', $user_id)->first();

        if (!$checkUserAnswer){
            $userAnswer = UserAnswer::create([
                'question_id' => $data['question_id'],
                'user_id' => $user_id,
                'test_id' => $data['test_id'],
                'answer_id' => $data['answer_id'],
                'ball' => $ball
            ]);
        }

        $locale = App::getLocale();


        if ($data['finish'])
            return redirect()->route('tests.finish', ['locale' => $locale, 'class' => $data['class'], 'lesson' => $data['lesson_id']]);

        return redirect()->route('tests.start', ['locale' => $locale,'class' => $data['class'], 'lesson' => $data['lesson_id'], 'num' => $data['num']]);

    }


}
