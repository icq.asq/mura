<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->segment(1)) {
            return redirect(route('index', [
                'locale' => config('app.locale')
            ]));    
        }
        app()->setLocale($request->segment(1));
        return $next($request);
    }
}
