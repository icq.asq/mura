<?php

namespace App\Services;

use App\Models\Image;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImageService
{
private Image $image;

private string $date;

    public function __construct()
    {
        $this->date = date('Y-m-d');
    }


    public function upload(UploadedFile $file): ImageService
    {

        $path = 'public/foto/backgrouds/';

        $name = Str::random(8) . '.' . $file->getClientOriginalExtension();

        $file->move($path, $name);

        $this->create($path . $name);

        return $this;
    }

    private function create(string $path)
    {
        $this->image = Image::create([
            'path' => $path
        ]);
    }

    public function image(): Image
    {
        return $this->image;
    }
}
