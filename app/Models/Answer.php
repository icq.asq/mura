<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{

    protected $fillable = [
        "question_id",
        "is_correct",
        "text_ru",
        "text_kz",
    ];

    public function scopeCorrect($query)
    {
        return $query->where('is_correct', 1);
    }

}
