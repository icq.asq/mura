<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banners';
    protected $fillable = [
        'image_id',
        'type_id',
        'number'
    ];

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public function type()
    {
        return $this->belongsTo(BannerType::class, 'type_id', 'id');
    }
}
