<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BannerType extends Model
{
    protected $table = 'banners_types';
    protected $fillable = ['type', 'name'];
}
