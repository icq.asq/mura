<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{

    protected $fillable = [
      'name_ru',
      'name_kz',
      'author_ru',
      'author_kz',
      'lecturer_ru',
      'lecturer_kz',
      'institute_ru',
      'institute_kz',
      'src_ru',
      'src_kz',
      'subcategory_id',
      'theme_id',
        'quarter_id',
        'year_id'
    ];

    public function theme()
    {
        return $this->belongsTo(Theme::class);
    }

    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class);
    }

    public function tests()
    {
        return $this->hasMany(Test::class);
    }

    public function userAnswer()
    {
        return $this->hasManyThrough(UserAnswer::class, Test::class);
    }

    public function questions()
    {
        return$this->hasManyThrough(Question::class, Test::class);
    }

    public function quarter()
    {
        $this->belongsTo(Quarter::class);
    }

    public function year()
    {
        $this->belongsTo(Year::class);
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($lesson) { // before delete() method call this
            $lesson->userAnswer()->delete();
            $lesson->questions()->delete();
            $lesson->tests()->delete();
        });
    }
}
