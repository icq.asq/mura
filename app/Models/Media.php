<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';

    protected $fillable = [
        'name',
        'description',
        'src',
        'theme_id',
        'subcategory_id',
        'locale'
    ];

    public function theme()
    {
        return $this->belongsTo(Theme::class);
    }

    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class);
    }
}
