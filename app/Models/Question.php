<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    protected $fillable = [
        "test_id",
        "answers_number",
        "ball",
        "text_ru",
        "text_kz",
    ];

}
