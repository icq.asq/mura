<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{

    protected $fillable = [
        "lesson_id",
        "subcategory_id",
        "final_date",
        "text_ru",
        "text_kz",
    ];


    public function lesson()
    {
        return $this->belongsTo(Lesson::class);
    }

    public function subcategory()
    {
        $this->belongsTo(Subcategory::class);
    }

    public function userAnswers()
    {
        return $this->hasMany(UserAnswer::class);
    }

    public function questios()
    {
        return $this->hasMany(Question::class);
    }

    public function scopeByTheme($query, $theme_id)
    {
        return $query->lesson()->where('theme_id', $theme_id);
    }

    public function scopeByClass($query, $subcategory_id)
    {
        return $query->where('subcategory_id', $subcategory_id);
    }

    public static function boot() {
        parent::boot();

        static::deleted(function($test) { // before delete() method call this
            $test->questions()->delete();
            $test->userAnswers()->delete();
        });
    }
}
