<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAnswer extends Model
{

    protected $table = "users_answers";
    protected $fillable = [
        "user_id",
        "test_id",
        "question_id",
        "answer_id",
        "ball",
    ];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function answer()
    {
        return $this->belongsTo(Answer::class);
    }

    public function isCorrect()
    {
        return $this->belongsTo(Answer::class)->correct();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function test()
    {
        return $this->belongsTo(Test::class);
    }

    public function theme($theme_id)
    {
        return $this->belongsTo(Test::class)->byTheme($theme_id);
    }

    public function scopeByRegister($query, $begin, $end)
    {
        return $query->whereBetween('users_answers.created_at', [$begin, $end]);
    }

    public function scopeByTheme($query, $theme_id)
    {
        return $query->where('theme_id', $theme_id);
    }

    public function scopeByClass($query, $subcategory_id)
    {
        return $query->whereHas('test', function ($q) use ($subcategory_id) {
            $q->where('subcategory_id', $subcategory_id);
        });
    }

    public function scopeByUser($query, $user)
    {
        return $query->whereHas('user', function ($q) use ($user) {
            $q->where('surname', 'like', $user."%")->
                orWhere('name', 'like', $user."%")->
                orWhere('email', 'like', $user."%");
        });
    }

    public function scopeByQuarter($query, $quarter)
    {
        return $query->whereHas('test', function ($q) use ($quarter) {
            $q->whereHas('lesson', function ($d) use ($quarter) {
                $d->where('quarter_id', $quarter);
            });
        });
    }

    public function scopeByYear($query, $year)
    {
        return $query->whereHas('test', function ($q) use ($year) {
            $q->whereHas('lesson', function ($d) use ($year) {
                $d->where('year_id', $year);
            });
        });
    }

}
