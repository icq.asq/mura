<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\Contacts as ContactsModel;

class Contacts extends Component
{
    public $contacts;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->contacts = ContactsModel::whereHas('description', function($q) {
                $q->where('language', app()->getLocale());
        
            })->with('description')
            ->get();
        // dd($this->contacts);

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.contacts');
    }
}
