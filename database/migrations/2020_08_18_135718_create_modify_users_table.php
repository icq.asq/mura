<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModifyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('INN')->nullable();
            $table->string('surname')->after("name")->nullable();
            $table->string('patronymic')->after("surname")->nullable();
            $table->integer('user_type')->default(1);
            $table->boolean('is_admin')->default(0);
            $table->text('avatar')->nullable();
            $table->string('birth_date')->nullable();
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->string('phone_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('INN');
            $table->dropColumn('surname');
            $table->dropColumn('patronymic');
            $table->dropColumn('user_type');
            $table->dropColumn('avatar');
            $table->dropColumn('birth_date');
            $table->dropColumn('city');
            $table->dropColumn('region');
            $table->dropColumn('phone_number');
        });
    }
}
