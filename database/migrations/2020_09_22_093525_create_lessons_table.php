<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->id();
            $table->string("name_ru");
            $table->string("name_kz");
            $table->string("author_ru")->nullable();
            $table->string("author_kz")->nullable();
            $table->string("lecturer_ru")->nullable();
            $table->string("lecturer_kz")->nullable();
            $table->string("institute_ru")->nullable();
            $table->string("institute_kz")->nullable();
            $table->string("src_ru");
            $table->string("src_kz");
            $table->unsignedInteger("subcategory_id");
            $table->unsignedInteger("theme_id");
            $table->date("begin_date")->nullable();
            $table->unsignedInteger("status");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
