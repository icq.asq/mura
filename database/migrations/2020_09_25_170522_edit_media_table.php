<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->dropColumn(['name_kz', 'src_kz', 'description_kz']);
            $table->renameColumn('name_ru', 'name');
            $table->renameColumn('src_ru', 'src');
            $table->renameColumn('description_ru', 'description');
            $table->unsignedInteger('subcategory_id');
            $table->renameColumn('lesson_id', 'theme_id');
            $table->string('locale');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
