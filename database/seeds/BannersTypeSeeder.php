<?php

use Illuminate\Database\Seeder;

class BannersTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'type' => 'main',
                'name' => 'Слайдер на главной странице'
            ],
            [
                'type' => 'help',
                'name' => 'Слайдер на странице "Помощь"'
            ],
        ];

        foreach ($types as $type){
            DB::table('banners_types')->insert([
                'type' => $type['type'],
                'name' => $type['name'],
            ]);
        }
    }
}
