<?php

use Illuminate\Database\Seeder;

class MediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $medias = [
            [
                'name' => 'Абай',
                'src' => 'HAkFPD3EHsU',
                'description' => '1 часть',
                'theme_id' => 4,
                'subcategory_id' => 11,
                'locale' => 'kz'
            ],
            [
                'name' => 'Абай',
                'src' => 'uIb3hP_tOeU',
                'description' => '2 часть',
                'theme_id' => 4,
                'subcategory_id' => 11,
                'locale' => 'kz'
            ],
            [
                'name' => 'Айман-Шолпан',
                'src' => 'dbsa0rbSIwI',
                'description' => '1 часть',
                'theme_id' => 4,
                'subcategory_id' => 6,
                'locale' => 'kz'
            ],
            [
                'name' => 'Айман-Шолпан',
                'src' => 'vmC8QoC3RO8',
                'description' => '2 часть',
                'theme_id' => 4,
                'subcategory_id' => 6,
                'locale' => 'kz'
            ],
            [
                'name' => 'Еңлік -Кебек',
                'src' => 'sB094KhPkE8',
                'description' => '1 часть',
                'theme_id' => 4,
                'subcategory_id' => 7,
                'locale' => 'kz'
            ],
            [
                'name' => 'Еңлік -Кебек',
                'src' => 'flEJMRHHObE',
                'description' => '2 часть',
                'theme_id' => 4,
                'subcategory_id' => 7,
                'locale' => 'kz'
            ],
            [
                'name' => '«Чайка»',
                'src' => 'v-ieSGV0tx0',
                'description' => 'А. Чехов. Постановка Р. Андриасяна',
                'theme_id' => 4,
                'subcategory_id' => 9,
                'locale' => 'ru'
            ],
            [
                'name' => '«На дне»',
                'src' => 'bPu7MEuov0w',
                'description' => 'М. Горький Постановка А. Кизилова. 1 часть',
                'theme_id' => 4,
                'subcategory_id' => 7,
                'locale' => 'ru'
            ],
            [
                'name' => '«На дне»',
                'src' => 'kn4DYqBvwIE',
                'description' => 'М. Горький Постановка А. Кизилова. 2 часть',
                'theme_id' => 4,
                'subcategory_id' => 7,
                'locale' => 'ru'
            ],
            [
                'name' => '«Король Лир»',
                'src' => '87720g6wpeE',
                'description' => 'В. Шекспир Постановка Р. Андриасяна',
                'theme_id' => 4,
                'subcategory_id' => 11,
                'locale' => 'ru'
            ],
            [
                'name' => '«Каштанка»',
                'src' => '1LWBecrX5EQ',
                'description' => 'Театр кукол',
                'theme_id' => 4,
                'subcategory_id' => 4,
                'locale' => 'ru'
            ],
            [
                'name' => '«Ана жүрегі»',
                'src' => 'ofTL1kme6D8',
                'description' => 'Театр кукол',
                'theme_id' => 4,
                'subcategory_id' => 10,
                'locale' => 'kz'
            ],
            [
                'name' => '«Қаңбақ шал»',
                'src' => '63Yp318_0IM',
                'description' => 'Театр кукол',
                'theme_id' => 4,
                'subcategory_id' => 3,
                'locale' => 'kz'
            ],
            [
                'name' => 'Қозы Көрпеш Баян сұлу',
                'src' => '9JdHfBFxzUc',
                'description' => 'Театральное искусство',
                'theme_id' => 4,
                'subcategory_id' => 8,
                'locale' => 'kz'
            ],
            [
                'name' => '«Достар серті»',
                'src' => 'SbOVcnXyBWA',
                'description' => 'Мюзикл',
                'theme_id' => 4,
                'subcategory_id' => 10,
                'locale' => 'kz'
            ],
            [
                'name' => '"ҚОЗЫ КӨРПЕШ – БАЯН СҰЛУ"',
                'src' => 'ZerQEADSc78',
                'description' => 'Трагедия',
                'theme_id' => 4,
                'subcategory_id' => 9,
                'locale' => 'kz'
            ],
        ];

        foreach ($medias as $media) {
            DB::table('media')->insert([
                'name' => $media['name'],
                'src' => $media['src'],
                'description' => $media['description'],
                'theme_id' => $media['theme_id'],
                'subcategory_id' => $media['subcategory_id'],
                'locale' => $media['locale'],
            ]);
        }

    }
}
