<?php

use Illuminate\Database\Seeder;

class QuarterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quartes = [
                'first quarter',
                'second quarter',
                'third quarter',
                'fourth quarter',
        ];

        foreach ($quartes as $quarte) {
            DB::table('quarters')->insert([
                'name' => $quarte
            ]);
        }
    }
}
