<?php

use Illuminate\Database\Seeder;

class QuotesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $quotes = [
            ['theme_id' => 1,
            'author' => "country description_bigger 1 name",
            'text_ru' => "country description_bigger 1",
            'text_kz' => "country description_bigger 1"],

            ['theme_id' => 2,
            'author' => "musical description_bigger 1 name",
            'text_ru' => "musical description_bigger 1",
            'text_kz' => "musical description_bigger 1"],

            ['theme_id' => 2,
            'author' => "musical description_bigger 2 name",
            'text_ru' => "musical description_bigger 2",
            'text_kz' => "musical description_bigger 2"],

            ['theme_id' => 3,
            'author' => "art description_bigger 1 name",
            'text_ru' => "art description_bigger 1",
            'text_kz' => "art description_bigger 1"],

            ['theme_id' => 3,
            'author' => "art description_bigger 2 name",
            'text_ru' => "art description_bigger 2",
            'text_kz' => "art description_bigger 2"],

            ['theme_id' => 4,
            'author' => "theater description_bigger 1 name",
            'text_ru' => "theater description_bigger 1",
            'text_kz' => "theater description_bigger 1"],

            ['theme_id' => 4,
            'author' => "theater description_bigger 2 name",
            'text_ru' => "theater description_bigger 2",
            'text_kz' => "theater description_bigger 2"],

            ['theme_id' => 5,
            'author' => "film description_bigger 1 name",
            'text_ru' => "film description_bigger 1",
            'text_kz' => "film description_bigger 1"],

            ['theme_id' => 5,
            'author' => "film description_bigger 2 name",
            'text_ru' => "film description_bigger 2",
            'text_kz' => "film description_bigger 2"],

            ['theme_id' => 6,
            'author' => "",
            'text_ru' => "patriot description_bigger 1",
            'text_kz' => "patriot description_bigger 1"],
        ];

        foreach ($quotes as $quote) {

            DB::table('quotes')->insert([
                'theme_id' => $quote['theme_id'],
                'author' => $quote['author'],
                'text_ru' => $quote['text_ru'],
                'text_kz' => $quote['text_kz']
            ]);

        }

    }
}
