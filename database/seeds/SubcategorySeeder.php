<?php

use Illuminate\Database\Seeder;

class SubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                ['category'=>1, 'subcategory'=>'1'],
                ['category'=>1, 'subcategory'=>'2'],
                ['category'=>1, 'subcategory'=>'3'],
                ['category'=>1, 'subcategory'=>'4']
            ],
            [
                ['category'=>2, 'subcategory'=>'5'],
                ['category'=>2, 'subcategory'=>'6'],
                ['category'=>2, 'subcategory'=>'7'],
                ['category'=>2, 'subcategory'=>'8']
            ],
            [
                ['category'=>3, 'subcategory'=>'9'],
                ['category'=>3, 'subcategory'=>'10'],
                ['category'=>3, 'subcategory'=>'11']
            ]
        ];
        foreach ($categories as $category) {
            foreach ($category as $item){
                DB::table('subcategories')->insert([
                    'name' => $item['subcategory'],
                    'category_id' => $item['category']
                ]);
            }
        }
    }
}
