<?php

use Illuminate\Database\Seeder;

class ThemesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $themes = [
            ['name_ru' => 'Сакральный Казахстан', 'image_ru' => 'foto/categories/country.png', 'name_kz' => 'Киелі Қазақстан', 'image_kz' => 'foto/categories/1.png'],
            ['name_ru' => 'Музыкальное искусство', 'image_ru' => 'foto/categories/musical-new.jpg', 'name_kz' => 'Музыка өнері', 'image_kz' => 'foto/categories/musical-new.png'],
            ['name_ru' => 'Изобразительное искусство', 'image_ru' => 'foto/categories/art.png', 'name_kz' => 'Бейнелеу өнері', 'image_kz' => 'foto/categories/3.png'],
            ['name_ru' => 'Театральное искусство', 'image_ru' => 'foto/categories/theater.png', 'name_kz' => 'Театр өнері', 'image_kz' => 'foto/categories/4.png'],
            ['name_ru' => 'Кинематография', 'image_ru' => 'foto/categories/film.png', 'name_kz' => 'Кинематография', 'image_kz' => 'foto/categories/2.png'],
            ['name_ru' => '"Я - патриот"', 'image_ru' => 'foto/categories/patriot.png', 'name_kz' => '«Мен Патриотпын»', 'image_kz' => 'foto/categories/5.png'],
        ];
        foreach ($themes as $theme) {
            DB::table('themes')->insert([
                'name_ru' => $theme['name_ru'],
                'image_ru' => $theme['image_ru'],
                'name_kz' => $theme['name_kz'],
                'image_kz' => $theme['image_kz'],
            ]);
        }
    }
}
