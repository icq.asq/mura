<?php

use Illuminate\Database\Seeder;

class YearsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 2020; $i < 2050; $i++){
            DB::table('years')->insert([
                'year' => $i."-".($i+1)
            ]);
        }
    }
}
