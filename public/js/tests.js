$( document ).ready(function() {
    jQuery('#class').change(function () {
        var theme_id = $('#theme').val();
        var class_id = $('#class').val();
        $.ajax({
            method: "GET",
            url: "/lessons/get-lessons",
            data: { theme_id: theme_id, class_id: class_id }
        })
            .done(function( data ) {
                data = $.parseJSON(data);
                console.log(data);
                $.each(data, function (key, value) {
                    $('#lesson').append("<option value='"+value.id+"'>"+value.name_ru+"</option>");
                })
            });
    })
});