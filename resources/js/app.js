/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import AOS from 'aos'


new Vue({
  el: '#app',
  created () {
    AOS.init()
  },
  render: h => h(App)
})
require('./bootstrap');

window.Vue = require('vue');
import Vuetify from 'vuetify'
import VueMask from 'v-mask'

Vue.use(VueMask)
Vue.mixin(require('./trans'))
Vue.use(Vuetify);
 
// Or only as a filter
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('Language', require('./components/LanguageComponent.vue').default);
Vue.component('magic-text', require('./components/MagicText.vue').default);
Vue.component('usefull-class-picker', require('./components/UsefullClassPicker.vue').default);
Vue.component('usefull-current-category', require('./components/UsefullCurrentCategory.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    data: {
        hover: false,
        aboutState: false,
        index: 0,
        images: [
            "url(/foto/bg.png)",
            "url(/foto/bg2.png)",
            "url(/foto/bg3.png)",
        ],
        backgroundImage: "url(/foto/bg.png)",
        interval: null,
        
        birth_date_menu: null,
        birth_date: null,

        scrolledEnough: false,

        editAccountModal: false,

        accountEditPassword: "",
        accountEditPasswordConfirmation: "",

        selectedUserType: "",
        selectedUserClass: "",

        userPhoneNumber: "",
    },
    mounted()  {
        if (window.user) {
            const user = JSON.parse(window.user);
            this.birth_date = user.birth_date
            this.selectedUserType = user.user_type
            this.selectedUserClass = user.user_type_class
            this.userPhoneNumber = user.phone_number
        }
        this.interval = setInterval(this.nextPicture, 5000);
        document.addEventListener('scroll', this.handleScroll)
    },
    methods: {
        editAccountModalProceed() {
            this.$refs.editAccountForm.submit()
        },
        handleScroll(e) {
            this.scrolledEnough = document.documentElement.scrollTop > 50;
        },
        nextPicture() {
            this.index += 1
            if (this.index == this.images.length) {
                this.index = 0
            }
            this.backgroundImage = this.images[this.index]
        }
    },
    computed: {
        accountEditValid () {
            return this.accountEditPassword.length > 7 && this.accountEditPassword == this.accountEditPasswordConfirmation
        }
    },
    watch: {
        hover (new_val) {
            if (new_val) {
                clearInterval(this.interval);
            } else {
                this.interval = setInterval(this.nextPicture, 5000);
            }
        }
    }
});

