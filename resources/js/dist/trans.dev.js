"use strict";

module.exports = {
  methods: {
    /**
     * Translate the given key.
     */
    __: function __(key, replace) {
      var translation,
          translationNotFound = true;

      try {
        translation = key.split('.').reduce(function (t, i) {
          return t[i] || null;
        }, window._translations[window._locale].php);

        if (translation) {
          translationNotFound = false;
        }
      } catch (e) {
        translation = key;
      }

      if (translationNotFound) {
        translation = window._translations[window._locale]['json'][key] ? window._translations[window._locale]['json'][key] : key;
      }

      _.forEach(replace, function (value, key) {
        translation = translation.replace(':' + key, value);
      });

      return translation;
    }
  }
};