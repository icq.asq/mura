<?php

return [
    'user' => 'Users',
    'tests' => 'Tests',
    'questions' => 'Questions',
    'answers' => 'Answers',
    'banners' => 'Banners',
    'posts' => 'Posts',
    'contacts' => 'Contacts'
];