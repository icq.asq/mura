<?php

return [
    'user' => 'Пайдаланушы',
    'users' => 'Пайдаланушылар', 
    'tests' => 'Сауалнамалар',
    'test' => 'Сұхбат',
    'questions' => 'Сұрақтар',
    'question' => 'Сұрақ',
    'answers' => 'Жауаптар',
    'answer' => 'Жауап беріңіз',
    'banners' => 'Баннерлер',
    'banner' => 'Баннер',
    'posts' => 'Хабарламалар',
    'post' => 'Тез',
    'contacts' => 'Байланыстар',
    'contact' => 'Байланыс',
    'editor' => 'Редактор',
    'name ' => 'Аты ',
    'banners name' => 'Баннер атауы',
    'correct' => 'Дұрыс',
    'contact name' => 'Байланыс аты',
    'posts name' => 'Хабарлама атауы',
    'question name' => 'Сұрақтың тақырыбы',
    'answer name' => 'Жауап атауы',
    'test name' => 'Тест аты',
    'end date' => 'Соңғы күн',
    'Name' => 'Аты',
    'surname' => 'Тегі',
    'patronymic' => 'әкенің есімі',
    'INN' => 'ИНН',
    'city' => 'Қала',
    'region' => 'Аймақ',
    'phone' => 'Телефон нөмірі',
    'user type' => 'Пайдаланушы түрі',
    'school' => 'Оқушы',
    'student' => 'Студент',
    'parent' => 'Ата-ана',
    'elder' => 'Зейнеткер',
    'specialist' => 'Жас маман',
    'birth date' => 'туған күні',
    'image' => 'Сурет',
    'email verified' => 'Email растау',
    'title' => 'Аты',
    'title_welcome' => '«Ұлағатты ұрпақ» мәдени-білім беру порталы',
    'is admin' => 'Админ',
    'password' => 'Пароль',
    'yapatriot' => 'Мен патриотпын',
    'main' => 'Басты бет',
    'about project' => 'Жоба жайлы',
    'help' => 'Көмек',
    'etiquette' => 'Этикет',
    'etiquette_text' => 'Не жақсы, ал не жаман? Адамдар осы сұрақтың төңірегінде әрқашан ойланатын.
    Сайттың бұл бөлігінде ұлы Абайдың адамгершілік кодексінен даналық кеңестер бар. 
    Сондай-ақ, сіздер бұл жерде қоғамдық жерлерде өзін-өзі ұстау ережесімен, этикет дегеннің не екені туралы, қандай да бір жағдайда не істеу керек екендігімен танысасыз.
    Мұның бәрі кітаптар,  өнер туындылары және кинофильмдер секілді сіздің тәрбиелі, инабатты және жоғары мәдениетті адам болуыңызға көмектесетін айшықты және қызықты бейнероликтерде берілген.',
    'etiquette_text_1' => 'Не жақсы, ал не жаман? Адамдар осы сұрақтың төңірегінде әрқашан ойланатын. Сайттың бұл бөлігінде ұлы Абайдың адамгершілік кодексінен даналық кеңестер бар.',
    'etiquette_text_2' => 'Сондай-ақ, сіздер бұл жерде қоғамдық жерлерде өзін-өзі ұстау ережесімен, этикет дегеннің не екені туралы, қандай да бір жағдайда не істеу керек екендігімен танысасыз.
    Мұның бәрі кітаптар,  өнер туындылары және кинофильмдер секілді сіздің тәрбиелі, инабатты және жоғары мәдениетті адам болуыңызға көмектесетін айшықты және қызықты бейнероликтерде берілген.',
    'helpful' => 'Пайдалы',
    'culture' => 'Мәдени-ағарту алаңы',
    'login' => 'Кіру',
    'register' => 'Тіркеу',
    'history tests' => 'Сынақ тарихы',
    'protect' => 'Барлық құқықтар заңмен қорғалады және қорғалады',
    'menu' => 'Мәзір',

    'october' => 'қазан, 2020',

    'text0' => '<span style="padding-left:100px;">Бізбен бірге өнерлі бол!</span>',
    'text1' => 'Бізбен бірге өнерлі бол!',
    'text2' => 'Сабақ күнтізбесі',
    'text3' => 'Тақырып',
    'text4' => 'Жарияланған күні',
    'text5' => 'Күй',
    'text6' => 'Бейнелеу өнері',
    'text7' => '19 қыркүйек, 2020',
    'text8' => 'Қол жетімді',
    'text9' => '3 қазан, 2020',
    'text10' => 'Күтуде',
    'text11' => '17 қазан, 2020',
    'text12' => '31 қазан, 2020',
    'text13' => '7 қараша, 2020',
    'text14' => 'Жарнамалық бейне',
    'text15' => 'Жақында сабақтар',
    'text16' => 'Сұрақ',

    'friend' => 'Құрметті достар!',
    'welcome' => '«Ұлағатты ұрпақ»',
    'spirit' => 'мәдени-білім беру порталына',
    'education' => 'қош келдіңіз',
    'wonders' => 'Бізбен бірге өнерлі бол!',
    'activity' => 'Біздің жоба мәдени-білім беру алаңы, ',
    'recreating' => 'жеке тұлғаның қалыптасуы мен дамуына қолайлы ',
    'space' => 'толыққанды болу барысында мектеп оқушыларының көп мәдениеттілігі ',
    'type' => 'өнердің барлық түрлерімен таныстыру',
    'start studing' => 'Оқуды бастаңыз',
    'scroll down' => 'Төмен айналдырыңыз',
    'first class' => '1-4 сынып',
    'fiveth class' => '5-8 сынып',
    'nineth class' => '9-11 сынып',
    'class' => 'сынып',
    'go to' => 'Өту',
    'other' => 'Басқалар',
    'registration' => 'Контентке қолжетімділік',
    'direct' => 'Біздің жоба – бұл өнердің әр түріне тарту ',
    'educations' => 'кезінде оқушылардың жан-жақты тұлға ретінде ',
    'art' => 'үйлесімді дамуы үшін қолайлы мәдени-білім беру алаңы.',
    'Kazah' => '',
    'feel' => 'өнердің барлық түріне. ',
    // 'kazah' => 'Қазақстанның мәдени мұрасына сәйкестігі ',
    // 'standart' => 'қоғамдағы белгілі бір стандарттар мен нормалар.',
    'content ' => 'Мазмұны ',
    'personal' => 'жеке мәліметтер',
    'not found' => 'Табылмады',
    'personal' => 'жеке мәліметтер',
    'category' => 'санаты',
    'repeat password' => 'Құпия сөзді қайталаңыз',
    'save' => 'үнемдеңіз',
    'tab_main' => 'Үй қойындысы',
    'tab_relations' => 'Байланыс қойындысы',
    'music' => 'Музыкалық ',
	 'history tests' => 'Тест тарихы',
    'adeptnesss' => 'өнер',
    'arts' => 'Бейнелеу ',
    'adeptness' => 'өнері',
    'cinema' => 'Кино',
    'theatre' => 'Театр ',
    'kieli' => 'Киелі ',
    'Kazakhstan' => 'Қазақстан',
    'etiquette' => 'Этикет',
    'motivation' => '«Мен Патриотпын» ',
    'patriot' => 'мотивациялық бейнелері',
    'musical' => 'Музыкалық',
    'trough' => 'Тестілеуді бастаңыз',
    'impossible' => 'Сіз бұл сынақты қайта тапсыра алмайсыз.',
    'next' => 'Келесі сұрақ',
    'video' => 'Бейне ',
    'about' => 'тест туралы',
    'target' => 'Біздің миссиямыз:',
    'schoolboy' => 'Оқушыларды биік мәдениетке тарту',
    'cultural' => 'Балалар мен жастарды адамгершілік және рухани тұрғыда тәрбиелеу',
    'active' => 'Эстетикалық талғамды қалыптастыру',
    'values' => 'Қазақстанның мәдени мұрасын құрметтеу',
    'oriented' => 'Бағдарлама кімге арналған',
    'first' => 'Науқан 1',
    'second' => 'Науқан 2',
    'third' => 'Науқан 3',
    'fourth' => 'Науқан 4',
    'subjects' => 'Бағыттардың сипаттамасы',
    'support' => 'Қазақстан Республикасының',
    'sport' => 'Мәдениет және спорт министрлігі',
    'madeni' => 'Мәдени мұра',
    'field' => 'Күнтізбелік кестеге сәйкес аптасына бір рет 1-11 сынып оқушылары үшін ұсынылған алты бағыттың нақты біреуі бойынша қазақ және орыс тілдерінде бейне-дәрістер орналасатын болады. Оқыту материалдарымен танысу үшін сізге жасыңызға қарай сәйкес топ пен сыныпты таңдап алу қажет. Бұл кезеңде тіркеуден өту керек емес. Таңдап алған соң сізге танысуға кірісуге болады. Егер одан әрі сіз тестілеуден өтуге ниеттенсеңіз, тіркеуден өту қажет болады.',
    'access' => 'Қосымша сілтемелер',
    'familiarization' => 'Көптеген бейне-дәрістерге сілтемелер тіркелген, бұл жерден сіз өткен материал бойынша қосымша ақпарат алуға мүмкіндігіңіз бар. Сілтемелерде көркем фильмдердің, мультфильмдердің, спектакльдердің, концерттердің толық нұсқасы мен отандық мәдениет ұйымдарының тәжірибелік дәрістері бар.',
    'access_test' => 'Тестілеуге қолжетімділік',
    'full' => 'Әр бейне-дәрістің соңында сіз өз біліміңізді кіші тест арқылы бекіте аласыз. Тестілер бес бағыт бойынша көзделген: «Музыкалық өнер», «Театр өнері», «Кинематография» және «Қасиетті Қазақстан». «Мен - патриотпын» бағытында тестілеу көзделмеген. Тестілеуден өту үшін сізге сайттан тіркелуден өту керек.',
    'results' => 'Нәтижелері және ынталандыру',
    'kazakhstan1' => 'Қазақстан -  біздің ұлы бабаларымыз – көшпенділер өмір сүрген шексіз еуразия даласының жүрегінде орналасқан әлемде аумағы бойынша тоғызыншы орындағы ел. ',
    'kazakhstan2' => 'Бұл бөлімде сіздер тарихы мыңдаған жылдарға дейін есептелетін, ал мәдени мұрасы - әлемдегі ең бай елдердің бірі болып саналатын қасиетті жерлер – Қазақстанның киелі жерлері туралы білетін боласыз. ',
    'Illustration1' => 'Бұл бөлімде атақты суретшілердің туындыларымен танысатын боласыз. Өнертанушылар олардың өмірі туралы, тамаша туындылардың шығу тарихы туралы әңгімелейтін болады. ',
    'Illustration2' => 'Сіздер Қазақстан бейнелеу өнерінің ірге тасын қалаушылар туралы және қазіргі заманғы шеберлердің жетістіктері туралы білетін боласыз. Сондай-ақ, сізді әлемнің атақты музейлері туралы қызықты мәліметтер күтеді, сіз олардың тамаша интерьерлері мен баға жетпес экспонаттарын көре аласыз. ',
    'Musician1' => 'Музыка ғажайып! Ол адамның шығармашылығын, оның қиялын және көз алдына елестету қабілетін дамытады. Музыка эмоциялық және рухани саланы жетілдіреді, ',
    'Musician2' => 'адамды сезімтал, нәзік жүректі етеді және ғажайып әлемге шарықтатады! Бұл бөлімде атақты шығармалардың жазылу тарихы туралы, ұлы шығармашыл тұлғалардың өмірінен қызықты фактілерді білетін боласыз. Музыкалық жанрлар мен стильдер туралы білетін боласыз.',
    'Theatry1' => 'Театр – бұл таңғажайып әлем! Ол сұлулық және мейірімділік сабақтарын береді. Бұл бөлімде қазақстандық және әлемдік театр өнерінің тарихы, ',
    'Theatry2' => 'олардың жарқын өкілдері туралы білім алатын боласыз. Сізге  авансцена, софиттер, декорациялар, кулиса және грим, театр этикеті және т.б.  кәсіби терминдердің мағынасы ашылады...  Ал ең бастысы – сіз театр сиқырын сезінесіз және оны одан артық жақсы көретін боласыз! ',
    'Cinematic1' => '«Кинематография» бөлімінде сіздерге кинофильмдер мен мультфильмдер қалай түсірілетіні туралы әңгімелеп береді.',
    'Cinematic2' => 'Сізді әлемдік кино актерлары мен режиссерлары туралы, сондай-ақ отандық кино өнерінің танымал қайраткерлері туралы қызықты әңгімелер күтеді. Осы жерде сіздер өзіңіздің сүйікті мультфильмдеріңіз бен киносыншылар ұсынған фильмдерді көре аласыз.  ',
    'Patriotic1' => 'Отанға деген махаббат – өте күшті сезім және ол әрқайсымыздың жүрегімізде. Бұл өз жанұяңа және өзің туып, өмір сүріп отырған жеріңе деген махаббат. ',
    'Patriotic2' => 'Отанды сүю дегеніміз – өз елінің патриоты болу, оны мақтан ету, оның тарихын білу, өз халқының дәстүрін құрметтеу. Бұл бөлім қазіргі Қазақстанның атақты тұлғаларының табысты тарихына арналған бейнероликтер циклін ұсынады, сонымен қатар, еліміздегі айрықша балалардың жетістіктері туралы білетін боласыз.',
    'prizes' => 'Балалардың тестілеуден ерікті түрде өтуі оның маңызды шарты болып табылады. Оқушылар арасында тестілеудің аяқталуы бойынша әр жас тобында жеңімпаздар анықталады. Оқу жылының соңында дұрыс жауап үшін едәуір жоғары ұпай жинаған ең белсенді қатысушыларды сыйлықтар мен тосынсыйлар күтеді.',
    'Kieli' => 'Киелi Казакстан',
    'kazakhstan' => 'Қазақстан - ұлы ата-бабаларымыз - көшпенділер өмір сүрген кең Еуразия даласының жүрегінде орналасқан әлемдегі тоғызыншы мемлекет.
    Бұл бөлімде сіз құпияны жасыратын ұлы қасиетті жерлер туралы білесіз - бұл Қазақстанның мыңдаған жылдық тарихы бар қасиетті жерлері, ал мәдени мұрасы әлемдегі ең бай жерлердің бірі. ',
    'illustration' => 'Бейнелеу өнері',
    'Illustration' => '«Бейнелеу өнері» бөлімінде сіз тамаша суретшілердің картиналарымен танысасыз.
    Өнертанушылар өздерінің өмірі туралы, шедеврлердің жасалу тарихы туралы айтып береді.
    Сіз Қазақстанның бейнелеу өнерінің бастауларында кім тұрғанын және заманауи шеберлердің жетістіктері туралы біле аласыз, бұл сізге өнердің ұлттық өзіндік ерекшелігін түсінуге және сезінуге мүмкіндік береді, оның дәстүрлері өткен ғасырдың көрнекті жасаушылары қалаған.
    Бұл сіздің жүрегіңізді туған мәдениетіңізге деген сүйіспеншілікке толтыратыны сөзсіз.
    Сонымен қатар, сіз әлемнің ұлы мұражайлары туралы қызықты таңдаулар таба аласыз, олардың керемет интерьерлері мен баға жетпес экспонаттарын көре аласыз.',
    'musician' => 'Музыкалық өнер',
    'Musician' => 'Музыка сиқырлы! Ол адамның шығармашылығын, қиялын және қиялын дамытады. Музыка эмоционалды-рухани саланы жақсартады, адамды сезімтал, жүрекжарды етеді және сізді сұлулық әлеміне жетелейді!
    «Музыкалық өнер» бөлімінде сіз көрнекті туындылар жасау туралы көптеген таңғажайып оқиғаларды, ұлы жасаушылардың өмірінен алынған қызықты фактілерді білесіз. Сіз музыкалық жанрлар мен стильдермен таныс боласыз.',
    'theatry' => 'Театр өнері',
    'Theatry' => 'Театр - сиқырлы әлем! Ол сұлулыққа, ізгілікке, адамгершілікке сабақ береді.
    Бұл бөлімде сіз түрлі-түсті иллюстрациялармен қызықты дәрістер түрінде қазақстандық және әлемдік театр өнерінің тарихы, олардың ең жарқын өкілдері туралы білім аласыз.
    Сіз актерлердің, режиссерлердің, драматургтердің жұмысы туралы білетін боласыз ... Сіз көптеген кәсіби терминдердің мағынасын ашасыз: процений, прожекторлар, декорациялар, сахна артындағы және макияж, театр этикеті ... Ең бастысы - сіз театрдың сиқырын сезінесіз және оны одан да жақсы көресіз!',
    'cinematic' => 'Кино',
    'Cinematic' => '«Кинематография» бөлімінен сіз фильмдер мен мультфильмдердің қалай түсірілетіні туралы білесіз.
    Сіз әлемдік киноның актерлері мен режиссерлері туралы, сондай-ақ орыс киносының көрнекті қайраткерлері туралы қызықты оқиғаларды таба аласыз.
    Мұнда сіз өзіңіздің сүйікті мультфильмдеріңізді және киносыншылар ұсынған фильмдерді көре аласыз.',
    'patriotic' => 'Мен патриотпын',
    'Patriotic' => 'Мұнда Қазақстанның заманауи көрнекті тұлғалары туралы және біздің елдің ең жақсы мектеп оқушыларының жетістіктері туралы «Сәттілік тарихы» бейнематериалдарының циклі ұсынылады.',
    'lessons' => 'Сабақтың тақырыбы',
    'supports' => 'Министрліктің қолдауымен
    мәдениет және спорт',
    'lets start' => 'Бастайық!',
    'authorization' => 'Бастау 
    үшін кіріңіз',
    'benefit' => 'Пайда және',
    'benefits' => 'артықшылықтары',
    'help content' => 'Осы жерден сіздер музыка, театр, кино, бейнелеу өнері, Қазақстанның қасиетті жерлері туралы көп қызықтар мен танымды мағлұматтар білетін боласыз. Бұл жайында тамаша лекторлар  - республикаға белгілі мәдениеттанушылар, тәжірибелі жаңашыл-оқытушылар түсіңнікті және қызықты етіп түсіндіріп береді.',
    'format' => 'Біздің порталымызда алынған білім сіздің өміріңізді қызықты әрі жарқын етеді деп сенеміз!',
    'gets' => 'Сізден күтілуде',
    'VR' => 'Қазақстанның қасиетті жерлеріне виртуалды сапар.',
    'online theatre' => 'Интернет, опера, балет,
    драмалық өнер.',
    'copyright' => 'Барлық құқықтар заңмен қорғалған және қорғалған.',
    'online cinema' => 'Қазақстандық және шетелдік интернеттегі көзқарастар
    фильмдер мен мультфильмдер.',
    'colorful' => 'Көптеген түрлі-түсті иллюстрациялар мен репродукциялар.',
    'musicus' => 'Таңғажайып музыкалық шығармалар.',
    'you can' => 'Әр бейне сабақтың соңында сіз өзіңізді тексере аласыз
    үш сұраққа жауап беріңіз. Дұрыс жауаптар үшін сізді сыйлықтар күтеді
    және жағымды тосын сыйлар!',
    'we believe' => 'Біздің порталда алынған білім деп ойлаймыз
    сіздің өміріңізді қызықты әрі жарқын етеді!',
    'draw' => 'Бізбен бірге ғажайыптар жасайық!',
    'how to' => 'Қалай өтуге болады',
    'registraation' => 'Тіркеу?',
	'first class' => '1-4 сынып',
    'fiveth class' => '5-8 сынып',
    'nineth class' => '9-11 сынып',
    'one point' => 'Оқу материалымен танысу үшін сізге сәйкес келетін төрт жас тобының біреуін таңдау керек. Өрістерді толтырыңыз және поштада тіркелуді растаңыз.',
    'two point' => 'Сізге жеті тақырыптық блокты таңдау керек,
    таңдау жасай отырып, сіз танысуға кірісе аласыз.',
    'three point' => 'Оқу материалымен танысуды аяқтағаннан кейін сіз жасай аласыз
    білімді тест тапсыру арқылы бекіту. Осыған
    Сіз сайтта толық тіркеуден өтуіңіз керек.',
    'four point' => 'Әр кезеңнің соңында мектеп оқушылары арасында жарыс,
    әр жас тобындағы жеңімпаздар. Байқау жеңімпаздары
    бағалы сыйлықтармен марапатталады.',
    'access content' => 'Мазмұнға 
    қол жеткізу',
	'go to' => 'Өту',
    'result' => 'Нәтижелер 
    мен марапаттар',
    'access tests' => 'Кіру
    тестілеуге',
    'how this' => 'Бұл қалай жұмыс істейді?',
	'programmadlya' => 'Ұзақтығы 15 минуттан бейне-сабақтар ыңғайлы онлайн-форматында 1- 11 сынып оқушыларына арналған. ',
	'waitingfor' => 'Сізді: ',
	'online show' => 'Опера, балет, драма өнерінің үздік үлгілерін онлайн көру; ',
	'online cinema' => 'Әлемдік және отандық музыкалық классиканың алтын мұрасы;',
	'illustartions' => 'Қазақстандық және шетелдік кинофильмдер мен мультфильмдердің онлайн-көрсетілімі; ',
	'musical instruments' => 'Көптеген түрлі-түсті суреттер мен атақты суреттердің репродукциялары;',
    'continue' => 'Тестті жалғастыру'
];