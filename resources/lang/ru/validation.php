<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute должен быть принят.',
    'active_url' => ':attribute не является допустимым URL.',
    'after' => ':attribute должен быть :date после :date.',
    'after_or_equal' => ':attribute должен быть :date после или равный :date.',
    'alpha' => ':attribute может содержать только буквы.',
    'alpha_dash' => ':attribute может содержать только буквы, цифры, дефисы и символы подчеркивания.',
    'alpha_num' => ':attribute может содержать только буквы и цифры.',
    'array' => ':attribute должен быть массивом.',
    'before' => ':attribute должен быть :date раньше :date.',
    'before_or_equal' => ':attribute должен быть :date до или равный :date.',
    'between' => [
        'numeric' => ':attribute должен находиться в диапазоне от :min до :max.',
        'file' => ':attribute должен быть от :min до :max килобайт.',
        'string' => ':attribute должен быть между :min и :max количеством символов.',
        'array' => ':attribute должен содержать от :min до :max количества элементов.',
    ],
    'boolean' => 'Поле :attribute должно быть истинным или ложным.',
    'confirmed' => 'Подтверждение :attribute не совпадает.',
    'date' => ':attribute не является действительной датой.',
    'date_equals' => ':attribute должен быть датой, равной дате.',
    'date_format' => ':attribute не соответствует формату формата.',
    'different' => ':attribute и :other должны быть разными.',
    'digits' => ':attribute должен состоять из :digits и цифр.',
    'digits_between' => 'Значение должно быть между :min и :max цифрами.',
    'dimensions' => 'Имеет недопустимые размеры изображения.',
    'distinct' => 'Поле имеет повторяющееся значение.',
    'email' => 'Должен быть действующий адрес электронной почты.',
    'ends_with' => 'Должно заканчиваться одним из следующих символов: :values.',
    'exists' => 'Выбрано неверно.',
    'file' => 'Это должен быть файл.',
    'filled' => 'Поле должно иметь значение.',
    'gt' => [
        'numeric' => 'Должно быть больше, чем: :value.',
        'file' => 'Должно быть больше, чем: :value килобайтов.',
        'string' => 'Должно быть больше, чем: :value символов.',
        'array' => 'Должно быть больше, чем: :value Предметов.',
    ],
    'gte' => [
        'numeric' => 'Должно быть больше или равно: :value.',
        'file' => 'Должно быть больше или равно: :value килобайтов.',
        'string' => 'Должно быть больше или равно: :value символов.',
        'array' => 'Должно быть: :value или больше.',
    ],
    'image' => 'Должно быть изображение.',
    'in' => 'Выбрано неверно.',
    'in_array' => 'Поле не существует в :other.',
    'integer' => 'Должно быть целым числом.',
    'ip' => 'Должен быть действующий IP-адрес.',
    'ipv4' => 'Должен быть действующий IPv4-адрес.',
    'ipv6' => 'Должен быть действующий адрес IPv6.',
    'json' => 'Должна быть допустимая строка JSON.',
    'lt' => [
        'numeric' => 'Должно быть меньше: :value.',
        'file' => 'Должно быть меньше: :value килобайтов.',
        'string' => 'Должно быть меньше: :value символов.',
        'array' => 'Должно быть меньше: :value предметов.',
    ],
    'lte' => [
        'numeric' => 'Должно быть меньше или равно: :value.',
        'file' => 'Должно быть меньше или равно: :value килобайтов.',
        'string' => 'Должно быть меньше или равно: :value символов.',
        'array' => 'Должно быть не более: :value предметов.',
    ],
    'max' => [
        'numeric' => 'Не может быть больше, чем: :max.',
        'file' => 'Не может быть больше, чем: :max килобайтов.',
        'string' => 'Не может быть больше, чем: :max символов.',
        'array' => 'Не может быть больше, чем: :max предметов.',
    ],
    'mimes' => 'Это должен быть файл типа: :values.',
    'mimetypes' => 'Это должен быть файл типа: :values.',
    'min' => [
        'numeric' => 'Должно быть как минимум: :min.',
        'file' => 'Должно быть как минимум: :min килобайтов.',
        'string' => 'Должно быть как минимум: :min символов.',
        'array' => 'Должно быть как минимум: :min предметов.',
    ],
    'not_in' => 'Выбрано неверно.',
    'not_regex' => 'Формат неверный.',
    'numeric' => 'Должен быть числом.',
    'password' => 'Пароль неверен.',
    'present' => 'Обязательное поле.',
    'regex' => 'Формат неверный.',
    'required' => 'Поле обязательно для заполнения.',
    'password_required' => 'Пароль обязателен для заполнения.',
    'required_if' => 'Поле обязательно, когда :other есть :value.',
    'required_unless' => 'Поле обязательно для заполнения, если :other есть в :values.',
    'required_with' => 'Поле обязательно, когда :values настоящее.',
    'required_with_all' => 'Поле обязательно, когда :values настоящее.',
    'required_without' => 'Поле обязательно, когда :values нет.',
    'required_without_all' => 'Поле является обязательным, если ни одно из :values настоящее.',
    'same' => 'И :other должен соответствовать.',
    'size' => [
        'numeric' => 'Должно быть: :size.',
        'file' => 'Должно быть: :size килобайтов.',
        'string' => 'Должно быть: :size символов.',
        'array' => 'Должен содержать: :size предметов.',
    ],
    'starts_with' => 'Должно начинаться с одного из следующих: :values.',
    'string' => 'Должен быть строкой.',
    'timezone' => 'Должна быть действующая зона.',
    'unique' => 'Уже есть существующий.',
    'uploaded' => 'Не удалось загрузить.',
    'url' => 'Формат неверный.',
    'uuid' => 'Должен быть действительный UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'заказное сообщение',
        ],
        'email' => [
            'unique' => 'Такой пользователь уже существует'
        ],
        'password' => [
            'confirmed' => 'Пароли не совпадают.',
            'min' => [
                'numeric' => 'Минимальная длина пароля: :min символов.',
            ],
            'required' => "Пароль обязателен для заполнения."
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
