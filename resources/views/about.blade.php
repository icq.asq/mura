<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mmura</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('foto/favicon.ico') }}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/about.css') }}">
    <link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new_change_about.css') }}">

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179617123-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-179617123-1');
    </script>
</head>

<body>
    <div id="app">
        <v-app>
            <div class="bluish-bg">
                <div class="pt-0 up page_main slider">
                    @if(isset($banners) && $banners->count() > 0)
                        @foreach($banners as $banner)
                            <div class="slider__img" data-bg="{{ asset($banner->image->path) }}"></div>
                        @endforeach
                    @else
                        <div class="slider__img" data-bg="{{ asset('foto/backgrounds/bgr1.png') }}"></div>
                        <div class="slider__img" data-bg="{{ asset('foto/backgrounds/bgr2.png') }}"></div>
                        <div class="slider__img" data-bg="{{ asset('foto/backgrounds/bgr3.png') }}"></div>
                    @endif
                <!--
                <div
                    class="pt-0 up page_main"
                    @mouseover="hover = true"
                    @mouseleave="hover = false"
                    v-bind:style="{ 'background-image': backgroundImage }"
                >
				-->
                    @include('components.simplified_menu')
                    <div class="content container z-index-4" style="position:relative;">
                        <div class="typewriter animated_text">
                            <h1>{{ __('admin.text1') }}</h1>
                        </div>
                        <!--<magic-text
                            :wonders="[
                                '{{ __('admin.wonders') }}'
                            ]"
                        ></magic-text>-->
                        <div class="fat_text">
                            {{ __('admin.friend') }}<br>
                            {{ __('admin.welcome') }} <br>
                            {{ __('admin.spirit') }} <br>
                            {{ __('admin.education') }}
                        </div>
                        <div class="mini_text">
                            {{-- {{ __('admin.activity') }}<br>
                            {{ __('admin.recreating') }}<br>
                            {{ __('admin.space') }}<br>
                            {{ __('admin.type')}} --}}
                        </div>
                        <!--<a href="@auth {{ routex('office') }} @else {{ routex('register') }} @endauth" class="mt-10 d-inline-block studying">
                        <button>
                            {{ __('admin.start studing') }}
                        </button>
                    </a>-->
                        <div class="scroll">
                            {{-- <i class="fas fa-arrow-down"></i>
                            <span class="scroll_down">{{ __('admin.scroll down') }}</span> --}}
                        </div>
                        <div class="classes">
                            @foreach($categories as $category)
                                <div class="resize-acceptor">
                                    <a href="{{ routex('categories', $category->id) }}/">
                                        <div class="class">
                                            <div class="left">
                                                <img src="{{ asset($category->image) }}">
                                            </div>
                                            <div class="right">
                                                <span class="class_text">{{ $category->name}}   {{ __('admin.class') }}</span>
                                                <div class="down">
                                                <!--<span class="class_text_down">{{ __('admin.go to') }}</span>-->
                                                    <i class="fas fa-angle-right"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <!-- Изменения вносил тут (снизу - 16.09) -->
                <div class="computer_block">
                    <div class="container">
                        <div class="secondSection">
                            <h1 class="title">{{ __('admin.target')}}</h1>
                            <div class="goalBox flex">
                                <div class="goal my-3" data-aos="fade-right">
                                    <div class="circle">01</div>
                                    <p>{{ __('admin.schoolboy')}}</p>
                                </div>
                                <div class="goal my-3" data-aos="fade-left">
                                    <div class="circle">02</div>
                                    <p>{{ __('admin.cultural')}}</p>
                                </div>
                                <div class="goal my-3" data-aos="fade-right">
                                    <div class="circle">03</div>
                                    <p>{{ __('admin.active')}}</p>
                                </div>
                                <div class="goal my-3" data-aos="fade-left">
                                    <div class="circle">04</div>
                                    <p>{{ __('admin.values')}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mobile_block">
                    <div class="container">
                        <div class="secondSection">
                            <h1 class="title">{{ __('admin.target')}}</h1>
                            <div class="goalBox flex">
                                <div class="goal my-3" data-aos="">
                                    <div class="circle">01</div>
                                    <p>{{ __('admin.schoolboy')}}</p>
                                </div>
                                <div class="goal my-3" data-aos="">
                                    <div class="circle">02</div>
                                    <p>{{ __('admin.cultural')}}</p>
                                </div>
                                <div class="goal my-3" data-aos="">
                                    <div class="circle">03</div>
                                    <p>{{ __('admin.active')}}</p>
                                </div>
                                <div class="goal my-3" data-aos="">
                                    <div class="circle">04</div>
                                    <p>{{ __('admin.values')}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Изменения вносил тут (сверху - 16.09) -->
                <div class="container">
                    <!-- Изменения вносил тут (снизу - 16.09) -->
                    <div class="computer_block">
                        <div class="thirdSection">
                            <div class="about col-6" data-aos="fade-right">
                                <h1 class="title">{{ __('admin.oriented')}}</h1>
                                <p class="about-text">{{ __('admin.programmadlya')}}</p>
                            </div>
                            <div class="third_right_block col-6" data-aos="fade-left">
                                <h1>{{ __('admin.waitingfor')}}</h1>
                                <div class="encouragementBox">

                                    <div class="encouragement first">
                                        <span></span>
                                        <div class="box">
                                            <p>{{ __('admin.online show')}}</p>
                                        </div>
                                    </div>
                                    <div class="encouragement second">
                                        <span></span>
                                        <div class="box">
                                            <p>{{ __('admin.online cinema')}}</p>
                                        </div>
                                    </div>
                                    <div class="encouragement third">
                                        <span></span>
                                        <div class="box">
                                            <p>{{ __('admin.illustartions')}}</p>
                                        </div>
                                    </div>
                                    <div class="encouragement fourth">
                                        <span></span>
                                        <div class="box">
                                            <p>{{ __('admin.musical instruments')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mobile_block">
                        <div class="thirdSection">
                            <div class="about col-12" data-aos="">
                                <h1 class="title">{{ __('admin.oriented')}}</h1>
                                <p class="about-text">{{ __('admin.programmadlya')}}</p>
                            </div>
                        </div>
                        <div class="thirdSection">
                            <div class="third_right_block col-12" data-aos="">
                                <h1>{{ __('admin.waitingfor')}}</h1>
                                <div class="encouragementBox">

                                    <div class="encouragement first">
                                        <span></span>
                                        <div class="box">
                                            <p>{{ __('admin.online show')}}</p>
                                        </div>
                                    </div>
                                    <div class="encouragement second">
                                        <span></span>
                                        <div class="box">
                                            <p>{{ __('admin.online cinema')}}</p>
                                        </div>
                                    </div>
                                    <div class="encouragement third">
                                        <span></span>
                                        <div class="box">
                                            <p>{{ __('admin.illustartions')}}</p>
                                        </div>
                                    </div>
                                    <div class="encouragement fourth">
                                        <span></span>
                                        <div class="box">
                                            <p>{{ __('admin.musical instruments')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Изменения вносил тут (сверху - 16.09) -->

                    <div class="fourthSection">
                        <h1 class="title">{{ __('admin.subjects')}}</h1>
                        <div class="topicBox">
                            <div class="topic col-12 col-sm-6 col-md-4">
                                <span class="topic_span">01</span>
                                <div class="box">
                                    <h4>{{ __('admin.Kieli')}}</h4>
                                    <p>
                                        <span class="text-hover">{{ __('admin.kazakhstan1')}}</span>
                                        <br>
                                        <span class="ellipsis"><u>Подробнее</u></span>
                                        <span class="text-opacity">{{ __('admin.kazakhstan2')}}</span>
                                    </p>
                                </div>
                            </div>
                            <div class="topic col-12 col-sm-6 col-md-4">
                                <span class="topic_span">02</span>
                                <div class="box">
                                    <h4>{{ __('admin.illustration')}}</h4>
                                    <p>
                                        <span class="text-hover">{{ __('admin.Illustration1')}}</span>
                                        <br>
                                        <span class="ellipsis"><u>Подробнее</u></span>
                                        <span class="text-opacity">{{ __('admin.Illustration2')}}</span>
                                    </p>
                                </div>
                            </div>
                            <div class="topic col-12 col-sm-6 col-md-4">
                                <span class="topic_span">03</span>
                                <div class="box">
                                    <h4>{{ __('admin.musician')}}</h4>
                                    <p>
                                        <span class="text-hover">{{ __('admin.Musician1')}}</span>
                                        <br>
                                        <span class="ellipsis"><u>Подробнее</u></span>
                                        <span class="text-opacity">{{ __('admin.Musician2')}}</span>
                                    </p>
                                </div>
                            </div>
                            <div class="topic col-12 col-sm-6 col-md-4">
                                <span class="topic_span">04</span>
                                <div class="box">
                                    <h4>{{ __('admin.theatry')}}</h4>
                                    <p>
                                        <span class="text-hover">{{ __('admin.Theatry1')}}</span>
                                        <br>
                                        <span class="ellipsis"><u>Подробнее</u></span>
                                        <span class="text-opacity">{{ __('admin.Theatry2')}}</span>
                                    </p>
                                </div>
                            </div>
                            <div class="topic col-12 col-sm-6 col-md-4">
                                <span class="topic_span">05</span>
                                <div class="box">
                                    <h4>{{ __('admin.cinematic')}}</h4>
                                    <p>
                                        <span class="text-hover">{{ __('admin.Cinematic1')}}</span>
                                        <br>
                                        <span class="ellipsis"><u>Подробнее</u></span>
                                        <span class="text-opacity">{{ __('admin.Cinematic2')}}</span>
                                    </p>
                                </div>
                            </div>
                            <div class="topic col-12 col-sm-6 col-md-4">
                                <span class="topic_span">06</span>
                                <div class="box">
                                    <h4>{{ __('admin.patriotic')}}</h4>
                                    <p>
                                        <span class="text-hover">{{ __('admin.Patriotic1')}}</span>
                                        <br>
                                        <span class="ellipsis"><u>Подробнее</u></span>
                                        <span class="text-opacity">{{ __('admin.Patriotic2')}}</span>
                                    </p>
                                </div>
                            </div>
                            <div class="topic col-12 col-sm-6 col-md-6">
                                <span class="topic_span">07</span>
                                <div class="box">
                                    <h4>{{ __('admin.etiquette')}}</h4>
                                    <p>
                                        <span class="text-hover">{{ __('admin.etiquette_text_1')}}</span>
                                        <br>
                                        <span class="ellipsis"><u>Подробнее</u></span>
                                        <span class="text-opacity">{{ __('admin.etiquette_text_2')}}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('components.footer')
            </div>
        </v-app>
        <div class="socials-fixed">
            <ul class="d-none d-lg-none d-xl-block list-unstyled text-center">
                @auth
                <li class="list-item">
                    <a href="{{ routex('office.feedback.form') }}" target="_blank" class="tooltipster tooltipstered">
                        <img class="scale-1-7" width="32px" src="{{ asset('foto/feedback.png') }}">
                    </a>
                </li>
                @endauth
                <li class="list-item">
                    <a href="https://m.facebook.com/uurpaq?ref=m_nux_wizard" target="_blank" class="tooltipster tooltipstered">
                        <img width="32px" src="{{ asset('foto/facebook.svg') }}">
                    </a>
                </li>
                <li class="list-item">
                    <a href="https://www.youtube.com/channel/UCFRb1r_ShVMPT-L7B_nxBMg"
                        target="_blank" class="tooltipster tooltipstered">
                        <img width="32px" src="{{ asset('foto/youtube.svg') }}">
                    </a>
                </li>
                <li class="list-item">
                    <a href="https://instagram.com/madeni_m.kz?igshid=150uo13p9eyfn" target="_blank"
                        class="tooltipster tooltipstered">
                        <img width="32px" src="{{ asset('foto/insta.svg') }}">
                    </a>
                </li>
                {{-- <li class="list-item">
                <a href="#" target="_blank" class="tooltipster tooltipstered">
                    <img width="32px" src="{{ asset('foto/whatsapp.svg') }}">
                </a>
                </li>
                <li class="list-item">
                    <a href="#" target="_blank" class="tooltipster tooltipstered">
                        <img width="32px" src="{{ asset('foto/vk.svg') }}">
                    </a>
                </li> --}}
            </ul>
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        function burgerMenu(icon) {
            let mobileMenu = document.querySelector('.mobile-menu');
            icon.classList.toggle("change");
            console.log(mobileMenu.style.left);
            if (mobileMenu.style.left == '' || mobileMenu.style.left == '100%') {
                mobileMenu.style.left = '0';
            } else {
                mobileMenu.style.left = '100%';
            }
        }

        let slider = Slider({ imgClass: "slider__img" });
        slider.init();

        function Slider({
        imgClass,
        prevClass = "prev",
        activeClass = "active",
        nextClass = "next",
        attributeName = "data-bg",
        }) {
        let slider = {};

        slider.imgClass = imgClass;
        slider.prevClass = prevClass;
        slider.activeClass = activeClass;
        slider.nextClass = nextClass;
        slider.attributeName = attributeName;

        slider.fillBackgrounds = function () {
            document
            .querySelectorAll(`.${imgClass}`)
            .forEach(
                (el) =>
                (el.style.backgroundImage = `url('${el.getAttribute(
                    attributeName
                )}')`)
            );
        };

        slider.setClasses = function () {
            let active =
            document.querySelector(`.${imgClass}.${activeClass}`) ||
            document.querySelector(`.${imgClass}`);

            active.classList.add(activeClass);

            let next =
            document.querySelector(`.${imgClass}.${nextClass}`) ||
            document.querySelector(
                `.${imgClass}.${activeClass} + .${imgClass}`
            ) ||
            document.querySelector(`.${imgClass}`);

            next.classList.add(nextClass);
        };

        slider.startSliding = function () {
            let interval;

            function anime() {
            let active = document.querySelector(`.${imgClass}.${activeClass}`);

            if (!active) {
                clearInterval(interval);
                return;
            }

            let next =
                document.querySelector(`.${imgClass}.${nextClass}`) ||
                document.querySelector(
                `.${imgClass}.${activeClass} + .${imgClass}`
                ) ||
                document.querySelector(`.${imgClass}`);

            if (next === active) {
                clearInterval(interval);
                return;
            }

            let nextNext =
                document.querySelector(
                `.${imgClass}.${nextClass} + .${imgClass}`
                ) || document.querySelector(`.${imgClass}`);

            next.classList.add(activeClass);
            next.classList.remove(nextClass);

            active.classList.remove(activeClass);
            active.classList.add(prevClass);

            setTimeout(() => {
                active.classList.remove(prevClass);
                nextNext.classList.add(nextClass);
            }, 1000);
            }

            interval = setInterval(anime, 5000);
        };

        slider.init = function () {
            this.fillBackgrounds();
            this.setClasses();
            this.startSliding();
        };

        return slider;
        }
    </script>

    <script>		
        AOS.init({
            
            disable: false, 
            startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
            initClassName: 'aos-init', // class applied after initialization
            animatedClassName: 'aos-animate', // class applied on animation
            useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
            disableMutationObserver: false, // disables automatic mutations' detections (advanced)
            debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
            throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)


            // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
            offset: 120, // offset (in px) from the original trigger point
            delay: 0, // values from 0 to 3000, with step 50ms
            duration: 400, // values from 0 to 3000, with step 50ms
            easing: 'ease', // default easing for AOS animations
            once: false, // whether animation should happen only once - while scrolling down
            mirror: false, // whether elements should animate out while scrolling past them
            anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation

        });
    </script>
</body>

</html>