@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
@endsection

@section('content')
<div class="office">
    @include('components.simplified_menu')
    <div class="center container">
        <div class="foto">
            <img src="{{ $user->avatar ? asset($user->avatar) : asset('foto/Слой_22.png') }}" id='avatar' style="border-radius:50px; height:616px;">
            <div class="right">
                <div class="cabinet">
					<div class="accountHeader">{{ __('main.cabinet') }}</div>
                    <div class='flex cabinet-item'>
                        <img src="{{ asset('foto/testy.png') }}" class='cabinet-img' alt="">
                        <div class='cabinet-item-body'>
                            <div>
                                <i class="fa fa-list-alt fs-24"></i>
                            </div>
                            <div class="fs-19" style="margin-top:6px;">
                                {{ __('main.test_history') }}
                            </div>
                            <a href="{{ routex('office.history') }}" class='cabinet-item-button' style="margin-top:8px;">
                                {{ __('main.test_visit') }}
                            </a>
                        </div>
                    </div>
                    <div class='flex cabinet-item'>
                        <img src="{{ auth()->user()->avatar ? asset(auth()->user()->avatar) : asset('foto/student.png') }}" alt="" class='cabinet-img'>
                        <div class='cabinet-item-body'>
                            <div>
                                <i class="fa fa-user fs-24"></i>
                            </div>
                            <div class="fs-19" style="margin-top:6px;">
                                {{ __('main.profile_edit') }}
                            </div>
                            <a href="{{ routex('personal') }}" class='cabinet-item-button' style="margin-top:8px;">
                                {{ __('main.test_visit') }}
                            </a>
                        </div>
                    </div>
                    <div class='flex cabinet-item mb-10'>
                        <img src="{{ asset('foto/write.png') }}" class='cabinet-img' alt="">
                        <div class='cabinet-item-body'>
                            <div>
                                <i class="fa fa-comments fs-24"></i>
                            </div>
                            <div class="fs-19" style="margin-top:6px;">
                                {{ __('main.test_feedback') }}
                            </div>
                            <a href="{{ routex('office.feedback') }}" class='cabinet-item-button' style="margin-top:8px;">
                                {{ __('main.test_visit') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.footer')
</div>
@endsection
