<div id="container" class="container">
    <div class="admin_block">
        <div class="admin_block-pannel">
            <ul>
                <li><a href="{{ route('lessons.index') }}">Уроки</a></li>
                <li><a href="{{ route('tests.create') }}">Добавить тест</a></li>
                <li><a href="{{ route('media.index') }}">Медиа-ссылки</a></li>
                <li><a href="{{ route('banners.index') }}">Баннеры</a></li>
                <li><a href="{{ route('statistics.register') }}">Общая статистика</a></li>
                <li><a href="{{ route('statistics.balls') }}">Статистика по баллам</a></li>
                <li>
                    <a href="#"
                       onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"
                    >
                        <button class='vhod'>
                            {{ __('main.logout') }}
                        </button>
                    </a>
                    <form id="logout-form" action="{{ routex('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>