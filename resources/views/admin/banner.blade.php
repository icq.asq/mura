@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/musical.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
    @include('admin.admin_menu')
    <div class="admin_block-body">
        <div class="admin-add-block">
                    @if(isset($error))
                        <h3 style="color: red;">{{ $error }}</h3>
                    @endif
                    <form action="{{ isset($banner)?route('banners.update', $banner->id):route('banners.store') }}" method="post" enctype="multipart/form-data">
                        @if(isset($banner))
                            @method('PUT')
                        @endif
                        @csrf

                        <h2>Тип</h2>
                        <div>
                            <select name="type_id">
                                @foreach($types as $type)
                                    <option value="{{ $type->id }}" {{ isset($banner)&&$banner->type_id==$type->id?"selected":"" }} >{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>

                            @if(isset($banner))
                            <div>
                                <img src="{{ asset($banner->image->path) }}" width="300px" height="auto">
                            </div>
                            @endif

                        <h2>Загрузить новое изображение</h2>
                        <div>
                            <input accept="image/png,image/jpeg, image/jpg" type="file" name="img"
                                    {{ (isset($banner) && empty($banner->image->id)) ? 'required' : '' }}>
                        </div>

                        <hr>
                        <div class="next-questions">
                            <button type="submit">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
