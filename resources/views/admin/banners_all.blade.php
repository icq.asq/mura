@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/musical.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
    @include('admin.admin_menu')
    <div class="admin_block-body">
        <div class="admin-add-block">
            <h2>Добавленные баннеры</h2>
            <a href="{{ route('banners.create') }}">Добавить баннер</a>
            <div id="questions" style="margin-top: 0px;">
                <table border="1" style="width: 100%">
                    <thead>
                    <tr>
                        <td>Тип</td>
                        <td>Изображение</td>
                        <td>Изменить</td>
                        <td>Удалить</td>
                    </tr>
                    </thead>
                    @if($banners->count()>0)
                        @foreach($banners as $banner)
                            <tr>
                                <td>{{ $banner->type->name }}</td>
                                <td>
                                    <img src="{{ asset($banner->image->path) }}" width="100px" height="50px">
                                </td>
                                <td>
                                    <a href="{{ route('banners.edit', $banner->id) }}">Изменить</a>
                                </td>
                                <td>
                                    <form method="post" action="{{ route('banners.destroy', $banner->id) }}">
                                        @method('delete')
                                        @csrf
                                        <button type="submit">X</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection