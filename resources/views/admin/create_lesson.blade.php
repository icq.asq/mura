@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/musical.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
    @include('admin.admin_menu')
    <div class="admin_block-body">
        <div class="admin-add-block">
            <form action="{{ isset($lesson)?route('lessons.update', $lesson->id):route('lessons.store') }}" method="post">
                @csrf
                @if(isset($lesson))
                    @method('put')
                @endif

                <h4>По какой теме урок</h4>
                <select style="width: 100%;" name="theme_id" required id="theme">
                    @foreach($themes as $theme)
                        <option value="{{ $theme->id }}"
                                {{ (isset($lesson)&&$lesson->theme_id==$theme->id)?"selected":"" }}>
                            {{ $theme->name_ru }}</option>
                    @endforeach
                </select>
                <br><br>

                <h4>Учебная четверть и год</h4>
                <select style="width: 45%;" name="quarter_id" required id="quarter">
                    @foreach($quarters as $quarter)
                        <option value="{{ $quarter->id }}"
                                {{ (isset($lesson)&&$lesson->quarter_id==$quarter->id)?"selected":"" }}>
                            {{ __('main.'.$quarter->name) }}
                        </option>
                    @endforeach
                </select>

                <select style="width: 45%;" name="year_id" required id="year">
                    @foreach($years as $year)
                        <option value="{{ $year->id }}"
                                {{ (isset($lesson)&&$lesson->year_id==$year->id)?"selected":"" }}>
                            {{ $year->year }}
                        </option>
                    @endforeach
                </select>
                <br><br>

                <h4>Для какого класса</h4>
                <select style="width: 100%;" name="subcategory_id" required id="class">
                    @foreach($classes as $class)
                        <option value="{{ $class->id }}"
                        {{ (isset($lesson)&&$lesson->subcategory_id==$class->id)?"selected":"" }}>
                            {{ $class->name }}
                        </option>
                    @endforeach
                </select>
                <br><br>

                <h4>Название урока на русском (обязательно)</h4>
                <textarea style="width: 100%;" name="name_ru" required>{{ isset($lesson)?$lesson->name_ru:"" }}</textarea>
                <br><br>

                <h4>Название урока на казахском (обязательно)</h4>
                <textarea style="width: 100%;" name="name_kz" required>{{ isset($lesson)?$lesson->name_kz:"" }}</textarea>
                <br><br>

                <h4>Автор урока на русском</h4>
                <input style="width: 100%;" name="author_ru" value={{ isset($lesson)?$lesson->author_ru:"" }}>
                <br><br>

                <h4>Автор урока на казахском</h4>
                <input style="width: 100%;" name="author_kz" value={{ isset($lesson)?$lesson->author_kz:"" }}>
                <br><br>

                <h4>Лектор урока на русском</h4>
                <input style="width: 100%;" name="lecturer_ru" value={{ isset($lesson)?$lesson->lecturer_ru:"" }}>
                <br><br>

                <h4>Лектор урока на казахском</h4>
                <input style="width: 100%;" name="lecturer_kz" value={{ isset($lesson)?$lesson->lecturer_kz:"" }}>
                <br><br>

                <h4>Организация на русском</h4>
                <input style="width: 100%;" name="institute_ru"  value={{ isset($lesson)?$lesson->institute_ru:"" }}>
                <br><br>

                <h4>Организация на казахском</h4>
                <input style="width: 100%;" name="institute_kz" value={{ isset($lesson)?$lesson->institute_kz:"" }}>
                <br><br>

                <h4>Ссылка на урок на Youtube.com на русском языке {{ isset($lesson)?"":"(обязательно)" }}</h4>
                @if(isset($lesson))
                    <iframe width="30%" height="20%" style="border-radius: 10px;" src="https://www.youtube.com/embed/{{ $lesson->src_ru }}?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                @endif
                <input style="width: 100%;" name="src_ru" placeholder="{{ isset($lesson)?"Новая ссылка":""}}" {{ isset($lesson)?"":"required"}}>
                <br><br>

                <h4>Ссылка на урок на Youtube.com на казахском языке {{ isset($lesson)?"":"(обязательно)" }}</h4>
                @if(isset($lesson))
                    <iframe width="30%" height="20%" style="border-radius: 10px;" src="https://www.youtube.com/embed/{{ $lesson->src_kz }}?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                @endif
                <input style="width: 100%;" name="src_kz" placeholder="{{ isset($lesson)?"Новая ссылка":""}}" {{ isset($lesson)?"":"required"}}>
                <br><br>
                
                <button type="submit">Сохранить</button>
            </form>
        </div>
    </div>
@endsection
