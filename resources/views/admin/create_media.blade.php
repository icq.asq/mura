@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/musical.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
    @include('admin.admin_menu')
    <div class="admin_block-body">
        <div class="admin-add-block">
            <form action="{{ isset($media)?route('media.update', $media->id):route('media.store') }}" method="post">
                @csrf

                @if(isset($media))
                    @method('put')
                @endif

                <h4>Тематика</h4>
                <select style="width: 100%;" name="theme_id" required id="theme">
                    @foreach($themes as $theme)
                        <option value="{{ $theme->id }}" {{ (isset($media)&&$media->theme_id==$theme->id)?"selected":"" }}>{{ $theme->name_ru }}</option>
                    @endforeach
                </select>
                <br><br>

                <h4>Для какого класса</h4>
                <select style="width: 100%;" name="subcategory_id" required id="class">
                    @foreach($classes as $class)
                        <option value="{{ $class->id }}" {{ (isset($media)&&$media->subcategory_id==$class->id)?"selected":"" }}>{{ $class->name }}</option>
                    @endforeach
                </select>
                <br><br>

                <h4>Заголовок к ссылке (обязательно)</h4>
                <input style="width: 100%;" name="name" required value="{{ isset($media)?$media->name:"" }}">
                <br><br>

                <h4>Краткое описание (чья постановка, часть и тд)</h4>
                <textarea style="width: 100%;" name="description" required>{{ isset($media)?$media->description:"" }}
                </textarea>
                <br><br>

                <h4>Ссылка на Youtube.com {{ isset($media)?"":"(обязательно)" }}</h4>
                @if(isset($media))
                    <iframe width="30%" height="20%" style="border-radius: 10px;" src="https://www.youtube.com/embed/{{ $media->src }}?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                @endif
                <input style="width: 100%;" name="src" placeholder="{{ isset($media)?"Новая ссылка":""}}" {{ isset($media)?"":"required"}}>
                <br><br>

                <h4>Язык медиа-ссылки</h4>
                <select style="width: 100%" name="locale" required>
                    <option value="ru" {{ (isset($media)&&$media->locale=="ru")?"selected":"" }}>русский</option>
                    <option value="kz" {{ (isset($media)&&$media->locale=="kz")?"selected":"" }}>казахский</option>
                </select>
                <br><br>

                <button type="submit">Сохранить</button>
            </form>
        </div>
    </div>
@endsection
