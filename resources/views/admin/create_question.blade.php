@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/musical.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
    @include('admin.admin_menu')
        <div class="admin_block-body">
            <div class="admin-add-block">
                <a href="{{ routex('main') }}">Завершить</a>
                @if(isset($error))
                    <h3 style="color: red;">{{ $error }}</h3>
                @endif
                <form action="{{ routex('questions.store') }}" method="post">
                    @csrf
                    <input type="hidden" name="test_id" value="{{ $test_id }}">
                    <input type="hidden" name="answers_number" value="3">
                    <input type="hidden" name="ball" value="1">

                    <h2>Текст вопроса на русском</h2>
                    <div>
                        <textarea style="width: 100%;" name="text_ru" required>{{ isset($data)?$data['text_ru']:"" }}</textarea>
                    </div>

                    <h2>Текст вопроса на казахском</h2>
                    <div>
                        <textarea style="width: 100%;" name="text_kz" required>{{ isset($data)?$data['text_kz']:"" }}</textarea>
                    </div>

                    <hr>

                    <h2>Варианты ответов:</h2>

                    <div>
                        <h3>Ответ №1.</h3>
                        <input type="checkbox" name="is_correct_1" id="one">
                        <label for="one">Правильный</label>
                        <textarea style="width: 100%;" name="answer_text_ru_1" required placeholder="Текст ответа на русском языке">{{ isset($data)?$data['answer_text_ru_1']:"" }}</textarea>
                        <textarea style="width: 100%;" name="answer_text_kz_1" required placeholder="Текст ответа на казахском языке">{{ isset($data)?$data['answer_text_kz_1']:"" }}</textarea>
                    </div>
                    <hr>
                    <div>
                        <h3>Ответ №2.</h3>
                        <input type="checkbox" name="is_correct_2" id="two">
                        <label for="two">Правильный</label>
                        <textarea style="width: 100%;" name="answer_text_ru_2" required placeholder="Текст ответа на русском языке">{{ isset($data)?$data['answer_text_ru_2']:"" }}</textarea>
                        <textarea style="width: 100%;" name="answer_text_kz_2" required placeholder="Текст ответа на казахском языке">{{ isset($data)?$data['answer_text_kz_2']:"" }}</textarea>
                    </div>
                    <hr>
                    <div>
                        <h3>Ответ №3.</h3>
                        <input type="checkbox" name="is_correct_3" id="three">
                        <label for="three">Правильный</label>
                        <textarea style="width: 100%;" name="answer_text_ru_3" required placeholder="Текст ответа на русском языке">{{ isset($data)?$data['answer_text_ru_3']:"" }}</textarea>
                        <textarea style="width: 100%;" name="answer_text_kz_3" required placeholder="Текст ответа на казахском языке">{{ isset($data)?$data['answer_text_kz_3']:"" }}</textarea>
                    </div>
                    <hr>
                    <div class="next-questions">
                        <button type="submit">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
