@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/musical.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
    @include('admin.admin_menu')
    <div class="admin_block-body">
        <div class="admin-add-block">
            <form action="{{ routex('tests.store') }}" method="post">
                @csrf
                <h4>По какой теме тест</h4>
                <select style="width: 100%;" name="theme_id" required id="theme">
                    @foreach($themes as $theme)
                        <option value="{{ $theme->id }}">{{ $theme->name_ru }}</option>
                    @endforeach
                </select>
                <br><br>

                <h4>Для какого класса</h4>
                <select style="width: 100%;" name="subcategory_id" required id="class">
                    <option></option>
                    @foreach($classes as $class)
                        <option value="{{ $class->id }}">{{ $class->name }}</option>
                    @endforeach
                </select>
                <br><br>

                <h4>К какому уроку</h4>
                <select style="width: 100%;" name="lesson_id" required id="lesson">
                    <option></option>
                </select>
                <br><br>

                <h4>Описание теста на русском (необязательно)</h4>
                <textarea style="width: 100%;" name="text_ru"></textarea>
                <br><br>

                <h4>Описание теста на казахском (необязательно)</h4>
                <textarea style="width: 100%;" name="text_kz"></textarea>
                <br><br>

                <button type="submit">Сохранить</button>
            </form>
        </div>
    </div>
@endsection
