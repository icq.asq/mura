@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/musical.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
    @include('admin.admin_menu')
            <div class="admin_block-body">
                <div class="admin-add-block">

                </div>
            </div>
        </div>
    </div>
@endsection
