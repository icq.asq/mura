@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/musical.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
    @include('admin.admin_menu')
    <div class="admin_block-body" style="overflow-x: auto;">
        <div class="admin-body-header">
            <h3>Добавленные уроки</h3>
            <a href="{{ routex('lessons.create') }}">Добавить урок</a>
        </div>
        <table border="1" class="table-lessons">
            <thead>
                <tr>
                    <td>Название (RU)</td>
                    <td>Название (KZ)</td>
                    <td>Видео (RU)</td>
                    <td>Видео (KZ)</td>
                    <td>Тематика</td>
                    <td>Класс</td>
                    <td>Изменить</td>
                    <td>Удалить</td>
                </tr>
            </thead>
            @if($lessons->count()>0)
                @foreach($lessons as $lesson)
                    <tr>
                        <td>{{ $lesson->name_ru }}</td>
                        <td>{{ $lesson->name_kz }}</td>
                        <td>
                            <a href="https://www.youtube.com/embed/{{ $lesson->src_ru }}?rel=0&showinfo=0" target="_blank">https://www.youtube.com/embed/{{ $lesson->src_ru }}?rel=0&showinfo=0</a>
                        </td>
                        <td>
                            <a href="https://www.youtube.com/embed/{{ $lesson->src_kz }}?rel=0&showinfo=0" target="_blank">https://www.youtube.com/embed/{{ $lesson->src_ru }}?rel=0&showinfo=0</a>
                        </td>
                        <td>{{ $lesson->theme->name_ru }}</td>
                        <td>{{ $lesson->subcategory->name }}</td>
                        <td>
                            <a href="{{ route('lessons.edit', $lesson->id) }}">Изменить</a>
                        </td>
                        <td>
                            <form method="post" action="{{ route('lessons.destroy', $lesson->id) }}">
                                @method('delete')
                                @csrf
                                <button type="submit">X</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @endif
        </table>
    </div>
@endsection