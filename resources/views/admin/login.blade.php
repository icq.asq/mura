@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/musical.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
    <style>
        .admin_block-body {
            width: 100%;
            text-align: center;
        }
        .admin_block-body .admin-add-block {
            display: inline;
        }
    </style>
@endsection

@section('content')
    <div class="admin_block-body">
        <div class="admin-add-block">
            <span>
                @if($errors)
                    {{ $errors->error->first() }}
                @endif
            </span>
            <form method="POST" action="{{ route('admin.isAdmin') }}">
                @csrf
                <label for="email">E-mail</label>
                <input type="text"  id="email" name="email" placeholder="Введите Ваш E-mail">

                <div class="d-flex w-260 justify-content-between">
                    <label for="password">Пароль</label>
                </div>
                <input type="password" id="password" name="password" required>

                <button class="registration">{{ __("main.login") }}</button>
            </form>
        </div>
    </div>

@endsection



