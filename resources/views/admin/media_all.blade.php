@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/musical.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
    @include('admin.admin_menu')
    <div class="admin_block-body" style="overflow-x: auto;">
        <div class="admin-body-header">
            <h3>Добавленные медиа-ссылки</h3>
            <a href="{{ routex('media.create') }}">Добавить медиа-ссылку</a>
        </div>
        <table border="1" class="table-media">
            <thead>
            <tr>
                <td>Название</td>
                <td>Ссылка</td>
                <td>Описание</td>
                <td>Тематика</td>
                <td>Класс</td>
                <td>Язык</td>
                <td>Изменить</td>
                <td>Удалить</td>
            </tr>
            </thead>
            @if($medias->count()>0)
                @foreach($medias as $media)
                    <tr>
                        <td>{{ $media->name }}</td>
                        <td>
                            <a href="https://www.youtube.com/embed/{{ $media->src }}?rel=0&showinfo=0" target="_blank">https://www.youtube.com/embed/{{ $media->src }}?rel=0&showinfo=0</a>
                        </td>
                        <td>
                            {{ $media->description }}
                        </td>
                        <td>{{ $media->theme->name_ru }}</td>
                        <td>{{ $media->subcategory->name }}</td>
                        <td>{{ $media->locale }}</td>
                        <td>
                            <a href="{{ route('media.edit', $media->id) }}">Изменить</a>
                        </td>
                        <td>
                            <form method="post" action="{{ route('media.destroy', $media->id) }}">
                                @method('delete')
                                @csrf
                                <button type="submit">X</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @endif
        </table>
    </div>
@endsection