@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/musical.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
    @include('admin.admin_menu')
    <div class="admin_block-body" style="overflow-x: auto;">
        <div class="admin-body-header">
            <h3>Общая статистика</h3>
            <form action="{{ route('statistics.balls') }}" method="get">
                <div>
                    <!--<input type="date" name="begin" value="{{ request()->has('begin')?request()->begin:"" }}">
                    <input type="date" name="end" value="{{ request()->has('end')?request()->end:"" }}">-->
                    <span>Четверть</span>
                    <select name="quarter">
                        <option></option>
                        @foreach($quarters as $quarter)
                            <option value="{{ $quarter->id }}"
                            {{ (request()->has('quarter')&&request()->quarter==$quarter->id)?"selected":"" }}>{{ __('main.'.$quarter->name) }}</option>
                        @endforeach
                    </select>

                    <span>Учебный год</span>
                    <select name="year">
                        @foreach($years as $year)
                            <option value="{{ $year->id }}"
                            {{ (request()->has('year')&&request()->year==$year->id)?"selected":"" }}>{{ $year->year }}</option>
                        @endforeach
                    </select>

                    <span>Тематика</span>
                    <select name="theme">
                        <option></option>
                        @foreach($themes as $theme)
                            <option value="{{ $theme->id }}"
                                    {{ (request()->has('theme')&&request()->theme==$theme->id)?"selected":"" }}>{{ $theme->name_ru }}</option>
                        @endforeach
                    </select>

                    <span>Класс</span>
                    <select name="class">
                        <option></option>
                        @foreach($classes as $class)
                            <option value="{{ $class->id }}"
                                    {{ (request()->has('class')&&request()->class==$class->id)?"selected":"" }}>{{ $class->name }}</option>
                        @endforeach
                    </select>
                    <input placeholder="ФИО или email" name="user" value="{{ (request()->has('user')&&!empty(request()->user))?request()->user:"" }}">
                    <input placeholder="Количество учеников" type="number" name="num" value="{{ (request()->has('num')&&!empty(request()->num))?request()->num:"" }}">
                    <button type="submit">Показать</button>
                </div>

            </form>
        </div>
        <br>
        <table border="1" class="table-lessons">
            <thead>
            <tr>
                <td>№</td>
                <td>Ученик</td>
                <td>Количество баллов</td>
            </tr>
            </thead>
            @if(isset($balls) && $balls->count() > 0)
                @foreach($balls as $ball)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $ball->user->fio() }}</td>
                        <td>{{ $ball->balls }}</td>
                    </tr>
                @endforeach
            @endif

        </table>
    </div>
@endsection