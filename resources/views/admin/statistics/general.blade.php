@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/musical.css') }}">
    <link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
    @include('admin.admin_menu')
    <div class="admin_block-body" style="overflow-x: auto;">
        <div class="admin-body-header">
            <h3>Общая статистика</h3>
            <form action="{{ route('statistics.register') }}" method="get">
                <input type="date" name="begin">
                <input type="date" name="end">
                <button type="submit">Показать</button>
            </form>
        </div>
        <table border="1" class="table-lessons">
            <thead>
            <tr>
                <td>Класс</td>
                <td>Количество</td>
            </tr>
            </thead>
            @if(isset($users) && count($users) > 0)
                @foreach($users as $user)
                <tr>
                    <td>{{ $user->class }}</td>
                    <td>{{ $user->num }}</td>
                </tr>
                @endforeach
            @endif

        </table>
    </div>
@endsection