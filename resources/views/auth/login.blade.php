@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/register.css') }}">
@endsection

@section('content')
<div>
    @include('components.simplified_menu')
    <div class="backImage"><img src="{{ asset('foto/51826f43be70c82df44fe403fd849276.jpg') }}" alt=""></div>

    <div class="container">
        <div class="main">
            <h1>{{ __('admin.login')}}</h1>
            @if(count($errors) > 0)
                <span style="color: red">{{ $errors->first() }}</span>
                @endif
            <form method="POST" action="{{ routex('login') }}">
                @csrf
                <label for="email">E-mail</label>
                <input type="text"  id="email" name="email" placeholder="Введите Ваш E-mail">

                <div class="d-flex w-260 justify-content-between">
                    <label for="password">Пароль</label>
                    <a href="{{ routex('password.request') }}" class='text-dark font-weight-bold'>
                        {{ __('main.Reset Password') }}
                    </a>
                </div>
                <input 
                    type="password" 
                    id="password" 
                    name="password" 
                    placeholder="Придумайте пароль"
                    class='
                    @error('password') is-invalid @enderror
                    '
                    required 
                    autocomplete="new-password"
                >

                <button class="registration">{{ __("main.login") }}</button>
                <div class="socials">
                    <a href="#">
                        <img src="{{asset('foto/Icon_google.svg')}}" alt="Google">
                    </a>
                    <a href="#">
                        <img src="{{asset('foto/Icon_fb.svg')}}" alt="Facebook">
                    </a>
                    <a href="#">
                        <img src="{{asset('foto/Icon_vk.svg')}}" alt="VK">
                    </a>
                </div>
            </form>
        </div>
        <div class="imgBox"><img src="{{ asset('foto/children.png') }}" alt=""></div>  
    </div>
</div>
@endsection
