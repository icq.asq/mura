@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/register.css') }}">
@endsection

@section('content')
<div>
    @include('components.simplified_menu')
    <div class="backImage"><img src="{{ asset('foto/51826f43be70c82df44fe403fd849276.jpg') }}" alt=""></div>

    <div class="container mt-10">
        <div class="main">
            <h1>{{ __('main.Reset Password') }}</h1>
            <h5 class='reset-password description'>{{ __('main.Reset Password Description') }}</h5>
            <form method="POST" action="{{ routex('password.email') }}">
                @csrf
                <label for="email">E-mail</label>
                <input type="text"  id="email" name="email" placeholder="Введите Ваш E-mail">

                <button class="registration">
                    {{ __('main.send') }}
                </button>
            </form>
        </div>
        <div class="imgBox"><img src="{{ asset('foto/children.png') }}" alt=""></div>  
    </div>
    {{-- <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Reset Password') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ routex('password.email') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Send Password Reset Link') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
