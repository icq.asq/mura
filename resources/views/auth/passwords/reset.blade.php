@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/register.css') }}">
@endsection

@section('content')
<div>
    @include('components.simplified_menu')
    <div class="backImage"><img src="{{ asset('foto/51826f43be70c82df44fe403fd849276.jpg') }}" alt=""></div>

    <div class="container">
        <div class="main">
            <h1>{{ __('main.resetPass') }}</h1>
            @if(count($errors) > 0)
                <span style="color: red">{{ $errors->first() }}</span>
            @endif
            <form method="POST" action="{{ routex('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <input type="hidden" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email">
                <label for="password">{{ __('main.password') }}</label>
                <input 
                    type="password" 
                    id="password" 
                    name="password" 
                    placeholder="Придумайте пароль"
                    required 
                    autocomplete="new-password"
                >

                <label for="confirm">{{ __('main.passwordRepeat') }}</label>
                <input 
                    type="password" 
                    id="confirm" 
                    name="password_confirmation" 
                    placeholder="Повторите пароль"
                    required 
                    autocomplete="new-password"
                >

                <button class="registration">
                    {{ __('main.passwordDoReset') }}
                </button>
            </form>
        </div>
        <div class="imgBox"><img src="{{ asset('foto/children.png') }}" alt=""></div>  
    </div>
</div>
@endsection
