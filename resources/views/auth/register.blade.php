@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/register.css') }}">
@endsection

@section('content')
<div>
    @include('components.simplified_menu')
    <div class="backImage"><img src="{{ asset('foto/51826f43be70c82df44fe403fd849276.jpg') }}" alt=""></div>

    <div class="container">
        <div class="main">
            <h1>Регистрация</h1>

            <form method="POST" action="{{ routex('register') }}">
                @csrf
                <label for="surname">Фамилия</label>
                <input type="text" required autocomplete="surname" autofocus id="surname" name="surname" placeholder="Фамилия">

                <label for="name">Имя</label>
                <input type="text" value="" required id="name" name="name" placeholder="Как имя">

                <label for="patronymic">Отчество</label>
                <input type="text" id="patronymic" name="patronymic" placeholder="Ваше отчество">

                <label for="email">E-mail</label>
                <input type="email" required id="email" name="email" placeholder="Введите Ваш E-mail">

                <label for="birth_date">Дата рождения</label>
                <input type="date"  id="birth_date" name="birth_date" value="2000-01-01" min="2000-01-01" max="2016-01-01" placeholder="Дата рождения">

                <label for="region">Область</label>
                <input type="text"  id="region" name="region" placeholder="Область">

                <label for="city">Город</label>
                <input type="text"  id="city" name="city" placeholder="Город">

                <label for="class">Класс</label>
                <input type="number"  id="class" name="class" placeholder="Класс" required>

                <label for="phone_number">Номер телефона</label>
                <input v-mask="'+7(###) ###-##-##'" id="phone_number" name='phone_number' placeholder="Номер телефона" v-model='userPhoneNumber' type="text" >
                <!-- <input type="text" id="phone_number" name="phone_number" pattern="2[0-9]{3}-[0-9]{3}" placeholder="Номер телефона"> -->

                <label for="password">Пароль</label>
                <input type="password" id="password" name="password" placeholder="Придумайте пароль" required autocomplete="new-password">

                <label for="confirm">Пароль</label>
                <input type="password" id="confirm" name="password_confirmation" placeholder="Повторите пароль" required autocomplete="new-password">

                <button class="registration">Зарегистрироваться</button>
                <div class="socials">
                    <a href="#">
                        <img src="{{asset('foto/Icon_google.svg')}}" alt="Google">
                    </a>
                    <a href="#">
                        <img src="{{asset('foto/Icon_fb.svg')}}" alt="Facebook">
                    </a>
                    <a href="#">
                        <img src="{{asset('foto/Icon_vk.svg')}}" alt="VK">
                    </a>
                </div>
            </form>
        </div>
        <div class="imgBox"><img src="{{ asset('foto/children.png') }}" alt=""></div>
        
    {{-- <div class="row justify-content-center">        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ routex('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> --}}
    
    
   
    </div>
</div>

@endsection
