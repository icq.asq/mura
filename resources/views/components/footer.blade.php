<?php $route = Route::current(); ?>
<div class="end">
    <div class="circle-footer"></div>
    <div class="inform container">
        {{-- @guest
        <div class="top_top_info">
            <div class="footer_reg">
                <div class="text_foot">
                    {{ __('admin.lets start')}}
                </div>
                <div class="foot_register">
                    {{ __('admin.authorization')}}
                    @if($route->action['as'] != 'login')
                    <a href="{{ routex('login') }}" class="logi">
                        <button class="vhod">
                            {{ __('admin.login') }}
                        </button>
                    </a>
                    @endif
                    @if($route->action['as'] != 'register')
                    <a href="{{ routex('register') }}" class="regis">
                        <button class="reg">
                            {{ __('admin.register') }}
                        </button>
                    </a>
                    @endif
                </div>
            </div>
        </div>
        @endguest --}}
        <div class="top_inform">
            <div class="footer-block">
                <x-contacts/>
                <div class="bi" style="font-size:19px;">
                    info@madeni-m.kz
                </div>
            </div>
            <div class="footer-block">
                <div class="big">
                    {{ __('admin.menu') }}
                </div>
                <div class="bi">
                    <a href="{{ routex('index') }}">{{ __('admin.main') }}</a>
                </div>
                <div class="bi">
                    <a href="{{ routex('about') }}">{{ __('admin.about project') }}</a>
                </div>
                <div class="bi">
                    <a href="{{ routex('help') }}">{{ __('admin.help') }}</a>
                </div>
                <div class="bi">
                    <a href="{{ routex('etiquette') }}">{{ __('admin.etiquette') }}</a>
                </div>
                <div class="bi">
                    <a href="{{ routex('patriot') }}">{{ __('admin.yapatriot') }}</a>
                </div>
                <!--
				<div class="bi">
					<a href="{{ routex('usefull') }}">{{ __('admin.helpful') }}</a>
				</div>
				-->
            </div>
            <div class="footer-block">
                <img src="{{ asset('foto/muragerb.png')}}" width="46px">
                <div class="bigi">Мәдени мұра</div>
                <div class="bii">{{ __('admin.copyright') }}</div>

                <div>
                    <a href="https://siteup.kz/">
                        <img src="{{ asset('foto/logositeup.png')}}" width="100px" class="logofooter">
                    </a>
                    <div class="bii" style="font-size:13px;margin-top:6px;">Разработано web-студией Siteup</div>
                </div>
                
				<!-- <div class="footer_logos">
                    <div class="support">
                        <img src="{{ asset('foto/photo5361654640967528633.png')}}">
                        {{ __('admin.supports')}}
                    </div>
                    <div class="footer_logo">
                        <img src="{{ asset('foto/update_logo.png')}}">
                            Ұлағатты<br> 
                            ұрпақ
                    </div>
                </div>    -->
            </div>
        </div>
    </div>
</div>