<?php $route = Route::current(); ?>
<div class="mobile-menu">
    <ul>
        <li><a href="{{ routex('index') }}">{{ __('admin.main') }}</a></li>
        <li><a href="{{ routex('about') }}">{{ __('admin.about project') }}</a></li>
        <li><a href="{{ routex('help') }}">{{ __('admin.help') }}</a></li>
        <li><a href="{{ routex('etiquette') }}">{{ __('admin.etiquette') }}</a></li>
        <li><a href="{{ routex('patriot') }}">{{ __('admin.yapatriot') }}</a></li>
        <br>
        <div class='laguage'>
            <Language current='{{ app()->getLocale() }}'></Language>
        </div>
        <br>
        @auth
        <li><img src="{{ auth()->user()->avatar ? asset(auth()->user()->avatar) : asset('foto/student.png') }}" alt="" class='min-ava'><a href="{{ routex('office') }}" class="tablo">{{ __('main.cabinet') }}</a></li>
        <li><a href="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" >{{ __('main.logout') }}</a></li>
        <form id="logout-form" action="{{ routex('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        @else
        @if($route->action['as'] != 'login')
        <li><a href="{{ routex('login') }}">{{ __('admin.login') }}</a></li>
        @endif
        @if($route->action['as'] != 'register')
        <li><a href="{{ routex('register') }}">{{ __('admin.register') }}</a></li>
        @endif
        @endauth
    </ul>
</div>