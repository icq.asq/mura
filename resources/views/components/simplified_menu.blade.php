<?php $route = Route::current(); ?>

<div class="top_menu mobi-to_top">
    <div class="up_menu container space-between to_top_except">
        <div class="up_menu left">
            <div class="mobile_block">
                <img src="{{ asset('foto/muragerb.png')}}">
            </div>
            <span class="computer_block">
                <img src="{{ asset('foto/photo5361654640967528633.png')}}">
                <div class="menu_text">
                    {{ __('admin.support')}}<br>
                    {{ __('admin.sport')}}
                </div>
            </span>
        </div>
        <div class="up_menu-centered">
            <div class="">
                <span class="up_menu name">{{ __('admin.madeni')}} <br></span>
                <!--{{ __('admin.culture') }}-->
            </div>
        </div>
        <div class="container-menu" onclick="burgerMenu(this)">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div>
        <div class='laguage'>
            <Language current='{{ app()->getLocale() }}'></Language>
        </div>
    </div>
</div>
<div class="snapper" :class='[{to_top: scrolledEnough}]'>
    <div class="menu container box-shadow: 10px 10px 50px #bcc9c9;">
        <div class="down_menu flex v-center space-between">
            <div class="flex v-center left-part">
                <a href="{{ routex('index') }}">
                    <img src="{{ asset('foto/logo1.png')}}">
                </a>
                <a class="tablo" href="{{ routex('index') }}">{{ __('admin.main') }}</a>      
                <a class="tablo" href="{{ routex('about') }}">{{ __('admin.about project') }}</a>   
                <a class="tablo" href="{{ routex('help') }}">{{ __('admin.help') }}</a>
                <a class="tablo" href="{{ routex('etiquette') }}">{{ __('admin.etiquette') }}</a>
                <a class="tablo" href="{{ routex('patriot') }}">{{ __('admin.yapatriot') }}</a>
            </div>
            @auth
            <div class="flex right-part z-index-4"> 
                <a href="#"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"
                >
                    <button class='vhod'>
                        {{ __('main.logout') }}
                    </button>
                </a>
                <form id="logout-form" action="{{ routex('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                <a href="{{ routex('office') }}" class="tablo account">
                    <img src="{{ auth()->user()->avatar ? asset(auth()->user()->avatar) : asset('foto/Слой_22.png') }}" alt="">
                    <div>
                        <div>
                            {{ auth()->user()->fio() }}
                        </div>
                        <div>
                            {{ __('main.cabinet') }}
                        </div>
                    </div>
                </a>
                <Language current='{{ app()->getLocale() }}' class='to_top_only' ></Language>
            </div>
            @else
            <div class="flex right-part z-index-4"> 
                @if($route->action['as'] != 'login')
                <a href="{{ routex('login') }}">
                    <button class="vhod">
                        {{ __('admin.login') }}
                    </button>     
                </a>
                @endif
                @if($route->action['as'] != 'register')
                <a href="{{ routex('register') }}">
                    <button class="reg">
                        {{ __('admin.register') }}
                    </button>
                </a>
                @endif
                <Language current='{{ app()->getLocale() }}' class='to_top_only' ></Language>
            </div>
            @endauth
        </div>
    </div>
</div>

@if (session('notification'))
    <div class="container alert alert-danger">
        <div class="ml-5">
            {!! session('notification') !!}
        </div>
        <?php session()->forget('notification') ?>
    </div>
@endif
@if (session('status'))
    <div class="alert alert-success" role="alert">
        <div class="ml-5">
            {{ session('status') }}
        </div>
    </div>
@endif

<div class="mobile-menu">
    <ul>
        <li><a href="{{ routex('index') }}">{{ __('admin.main') }}</a></li>
        <li><a href="{{ routex('about') }}">{{ __('admin.about project') }}</a></li>
        <li><a href="{{ routex('help') }}">{{ __('admin.help') }}</a></li>
        <li><a href="{{ routex('etiquette') }}">{{ __('admin.etiquette') }}</a></li>
        <li><a href="{{ routex('patriot') }}">{{ __('admin.yapatriot') }}</a></li>
        <br>
        <div class='laguage'>
            <Language current='{{ app()->getLocale() }}'></Language>
        </div>
        <br>
        @auth
        <li><img src="{{ auth()->user()->avatar ? asset(auth()->user()->avatar) : asset('foto/student.png') }}" alt="" class='min-ava'><a href="{{ routex('office') }}" class="tablo">{{ __('main.cabinet') }}</a></li>
        <li><a href="" onclick="event.preventDefault(); document.getElementById('logout-form2').submit();" >{{ __('main.logout') }}</a></li>
        <form id="logout-form2" action="{{ routex('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        @else
        @if($route->action['as'] != 'login')
        <li><a href="{{ routex('login') }}">{{ __('admin.login') }}</a></li>
        @endif
        @if($route->action['as'] != 'register')
        <li><a href="{{ routex('register') }}">{{ __('admin.register') }}</a></li>
        @endif
        @endauth
    </ul>
</div>

<noscript><div><img src="https://mc.yandex.ru/watch/67932340" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

@section('scripts')
<script>
    function burgerMenu(icon) {
        let mobileMenu = document.querySelector('.mobile-menu');
        icon.classList.toggle("change");
        if (mobileMenu.style.left == '' || mobileMenu.style.left == '100%') {
            mobileMenu.style.left = '0';
        } else {
            mobileMenu.style.left = '100%';
        }
    }

    loadFile = function(event) {
        var output = document.getElementById('avatar');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src) // free memory
        }
    };

    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(67932340, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
@endsection