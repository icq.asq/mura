@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
<link rel="stylesheet" href="{{ asset('css/musical.css') }}">
<link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
<div class="bg-musical">
    @include('components.simplified_menu')
    <div class='mt-5 musical'>
        <div class="computer_block">
            <div id="container" class="container musicas">
                <div class="music-art">
                    <div class="about-music-art">
                        <div id="music">
                            <h1 style="z-index: 2;">Этикет</h1>
                            <div id="line-one"
                                style="width: 215px; height: 10px; background-color: #f5d545; margin-top: -14px; z-index: -2; ">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="quote col-12 col-md-6">
                    <blockquote>
                        <p>{{ __('admin.etiquette_text') }}</p>
                        <!-- {{-- <p style="text-align: right;font-style:italic;font-weight: 600;">{{ $quote->author?__('main.'.$quote->author):"" }}</p> --}} -->
                    </blockquote>
                </div>
            </div>

            <br><br>

            <div class="container_iframe">
                <!-- <div class="iframe_wrapper">
                    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/IWjPxzsNFj4?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="iframe_wrapper">
                    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/MHzWTTgPPVA?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div> -->
            </div>
        </div>
        <div class="mobile_block">
            <div id="container" class="container musicas">
                <div class="music-art">
                    <div class="about-music-art">
                        <div id="music">
                            <h1 style="z-index: 2;">Этикет</h1>
                            <div id="line-one"
                                style="width: 215px; height: 10px; background-color: #f5d545; z-index: -2; ">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="quote col-12 col-md-6">
                <blockquote>
                    <p>{{ __('admin.etiquette_text') }}</p>
                    <!-- {{-- <p style="text-align: right;font-style:italic;font-weight: 600;">{{ $quote->author?__('main.'.$quote->author):"" }}</p> --}} -->
                </blockquote>
            </div>
            <div class="container" style="margin-top: 20px;">
                <!-- <center>
                    <iframe width="100%" height="60%" src="https://www.youtube.com/embed/IWjPxzsNFj4?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <iframe width="100%" height="60%" src="https://www.youtube.com/embed/MHzWTTgPPVA?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </center> -->
            </div>
        </div>
    </div>
    @include('components.footer')
</div>
@endsection