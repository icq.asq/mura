@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
@endsection

@section('content')
<div class="office">
    @include('components.simplified_menu')
    <div class="center container">
        <div class="foto">
            <img src="{{ $user->avatar ? asset($user->avatar) : asset('foto/pexels-daria-shevtsova-698928.jpg') }}" id='avatar' style="border-radius: 50px; height:400px;">
            <div class="right">
                <div class="accountHeader">{{ __('main.test_feedback') }}</div>
                <div class="cabinet">
                    {{ __('main.feedback_content') }}
                </div>
                <div class="go-back" style="margin-top: 36px;">
					<a href="{{ routex('office.feedback.form') }}" class='btn-yellow' style="text-decoration: none;">
                        {{ __("main.ask") }}
                    </a>
                </div>
				<div class="go-back" style="position: absolute; bottom: 0px; right:0; margin-right:40px;">
                    <a href="{{ routex('office') }}" class='f-1'>
                        <i class="fa fa-long-arrow-alt-left"></i> {{ __("main.back") }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    @include('components.footer')
</div>
@endsection