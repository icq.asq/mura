@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
@endsection

@section('content')
<div class="office">
    @include('components.simplified_menu')
    <div class="center container">
        <div class="foto">
            <img src="{{ $user->avatar ? asset($user->avatar) : asset('foto/pexels-daria-shevtsova-698928.jpg') }}" id='avatar' style="border-radius: 50px;">
            <div class="right">
                <div class="right_container">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="{{ routex('personal.post') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="avatar" class='d-none' id="file">
                        <div class="accountHeader"> {{ __('main.test_feedback') }}</div>
                        <div class="info_edit">
                            <div class="top">{{ __('main.subject') }}</div>
                            <input name='subject' placeholder="{{ __('main.subject') }}" type="text">
                        </div>
                        <div class="info_edit">
                            <div class="top">{{ __('main.message') }}</div>
                            <v-textarea
                                name="message"
                            ></v-textarea>
                        </div>
                        <div class="info_edit">
                            <v-file-input 
                                multiple 
                                label="{{ __('main.attached_files') }}"
                                chips 
                                show-size
                                counter 
                            ></v-file-input>
                        </div>
                        <button type='submit' class="btn-yellow">
                            {{ __('main.send') }}
                        </button>
                    </form>
                </div>
                <div class="go-back">
                    <a href="{{ routex('office.feedback') }}" class='f-1'>
                        <i class="fa fa-long-arrow-alt-left"></i> {{ __("main.back") }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    @include('components.footer')
</div>
@endsection