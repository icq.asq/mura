@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
<link rel="stylesheet" href="{{ asset('css/musical.css') }}">
<link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
<div class="{{ $theme->background }}">
    @include('components.simplified_menu')
    <div class='mt-5 musical'>
        <div id="container" class="container musicas">
            <div class="music-art">
                <div class="about-music-art">
                    <div id="music">
                        <h1 style="z-index: 2;">{{ $theme->$themeName}}</h1>
                        <div id="line-one"
                            style="width: 215px; height: 10px; background-color: #f5d545; margin-top: -14px; z-index: -2; ">
                        </div>
                    </div>
                    @if($lesson)
                        <div class="about-art">
                            @if($locale=="ru" && $lesson->author_ru != "" || $locale=="kz" && $lesson->author_kz)
                            <p>Автор: {{ $locale=="ru"?$lesson->author_ru:$lesson->author_kz }}</p>
                            @endif
                            <p>Лектор: {{ $locale=="ru"?$lesson->lecturer_ru:$lesson->lecturer_kz }}</p>
                        </div>
                    @endif
                </div>
            </div>
			
            <div class="quote col-12 col-md-6" style="margin-right:100px;">
                @foreach($quotes as $quote)
                    <blockquote>
                        <p>{!! trans('main.'.$quote->text_ru) !!}</p>
                        <p style="text-align: right;font-style:italic;font-weight: 600;">{{ $quote->author?__('main.'.$quote->author):"" }}</p>
                    </blockquote>
                @endforeach
            </div>

            {{-- <div class="img-box img-music">
            </div> --}}

        </div>

        <br><br>
        @if($lesson)
            <div style="text-align: center;">
                <h1>{{ __('main.lesson') }} № 1</h1>
            </div>
            <div style="text-align: center; width:80%; margin-left:auto; margin-right:auto;">
                <h3><b>{{ __('main.themeLesson')}}</b> {{ $locale=="ru"?$lesson->name_ru:$lesson->name_kz }}</h3>
            </div>


            <div id="container-two">
                <div class="computer_block">
                    <iframe width="890" height="470" style="border-radius: 25px;" src="https://www.youtube.com/embed/{{ $locale=="ru"?$lesson->src_ru:$lesson->src_kz }}?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="mobile_block">
                    <br>
                    <iframe width="100%" height="100%" style="border-radius: 10px;" src="https://www.youtube.com/embed/{{ $locale=="ru"?$lesson->src_ru:$lesson->src_kz }}?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        @endif
        @if($media && $media->count()>0)
            <div class="mt-10 container additional_materials_container">

                <h1>{{ __('main.additional_materials')}}</h1>

                @foreach($media as $m)
                <!-- само модальное окно -->
                <div id="ModalOpen{{$loop->iteration}}" class="Window">
                    <div>
                        <a href="#close" title="Закрыть" class="close">x</a>
                        <iframe width="620" height="440" src="https://www.youtube.com/embed/{{ $m->src }}?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>

                <div class="additional_materials_block">
                    <div class='additional_material'>
                        <div class="level mr-5">{{ $loop->iteration }}</div>
                        <div class="material_name mr-15"><b>{{ $m->name }}</b></div>
                        <div class="lector f-1 mr-5">{{ $m->description }}</div>
                        <div class="class f-1 mr-5">{{ $m->subcategory_id." ".__('main.class') }} </div>
                        <div class="goto">
                            <b>
                                <a href="#ModalOpen{{$loop->iteration}}">
                                    Перейти <i class="fa fa-long-arrow-alt-right"></i>
                                </a>
                            </b>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        @endif
        @if($lesson)
            @if($hasTest)
                <div class='take_the_test'>
                    <div class="go-for-test-back-image-shadow">
                        <h1>{{ __('admin.trough')}}</h1>
                        @if(!$doTest)
                            <p>{{ __('admin.impossible')}}</p>
                        @elseif($wrongClass)
                            <p>{{ __('admin.wrong class')}}</p>
                        @else
                        <br>
                        <a href="{{ routex('tests.start', ['class' => $class, 'lesson' => $lesson->id, 'num' => $doTest!==true?$doTest:0]) }}" class="btn-yellow">
                            @if($doTest === true)
                                {{ __('admin.go to')}}
                            @else
                                {{ __('admin.continue') }}
                            @endif
                        </a>
                        @endif
                    </div>
                </div>
            @endif
        @else
            <div class='take_the_test'>
                <div class="go-for-test-back-image-shadow">
                    <h1>{{ __('main.start date') }}</h1>
                    <h1>{{ date('d.m.Y', strtotime($theme->start)) }}</h1>
                    <br>
                </div>
            </div>
        @endif
    </div>    
    @include('components.footer')
</div>
@endsection