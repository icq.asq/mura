@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
@endsection

@section('content')
<?php $route = Route::current(); ?>
@include('components.simplified_menu')
<div class="container">
    <div class="category_inners">

        @foreach($themes as $theme)

        <div class="category_holder">
            <a href="{{ routex('lessons', ['category' => $url['category'], 'class' => $url['class'], 'theme' => $theme->id])}}/">
                <div class="category_inner">
                    <div class="top_image">
                        <img src="{{ asset($theme->$image) }}">
                    </div>
                    <div class="bottom_content">
                        <span class="category_title">{{ $theme->$name }}</span><br>
                        <div class="bot">
                            <span class="category_down"></span>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        @endforeach
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
    <div class="mt-10 mb-10 go-back-big-block">
        <a href="{{ url()->previous() != url()->current() && $route->action['as'] != 'first_category' ? url()->previous() : routex('index') }}" style="color:#1976d2 !important;font-size: 16px;">
            <i class="fa fa-long-arrow-alt-left"></i> Назад
        </a>
    </div>
</div>
@include('components.footer')
@endsection