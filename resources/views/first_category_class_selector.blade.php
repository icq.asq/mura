@extends('layouts.main')

@section('content')
<?php $route = Route::current(); ?>
<div class='d-flex bluish-bg flex-direction-column min-h-100'>
    @include('components.simplified_menu')
    <div class="container f-1">
        <div class="category_inners for_four">

            @foreach($subcategories as $subcategory)

                <div class="category_holder">
                    <a href="{{ routex('categories', ['category' => $subcategory->category_id, 'class' => $subcategory->id]) }}">
                    <div class="category_inner" style="padding: 30px;">                         
                        <div style="font-size: 20px;">
                                <span class="category_title"> {{$subcategory->name.  __('main.class') }}</span><br>
                            </div>
                            <div class="visit_arrow" style="padding-left: 70px; font-size: 18px;">
                                {{ __('admin.go to') }} <i class="fa fa-long-arrow-alt-right"></i>
                            </div>
                        </div>
                    </a>
                </div>

            @endforeach

            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
        <div class="go-back-big-block mt-10">
            <a href="{{ url()->previous() != url()->current() && $route->action['as'] != 'third_category.class' ? url()->previous() : routex('index') }}" style="color:#1976d2 !important;font-size: 16px;">
                <i class="fa fa-long-arrow-alt-left"></i> Назад
            </a>
        </div>
    </div>
    <div class="computer_block">
        <br><br><br><br><br><br><br><br><br><br><br>
    </div>
    @include('components.footer')
</div>
@endsection