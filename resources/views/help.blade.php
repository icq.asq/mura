@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/help.css') }}">
<link rel="stylesheet" href="{{ asset('css/new_change_help.css') }}">
<link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
<style>

    .slices {
        position: absolute;
        width: 400px;
        background: {{ asset($banners[0]->image->path) }} center center no-repeat;
        border-radius: 30px;
        background-size: 400px;
        cursor: pointer;
    }

    .slices .slice {
        position: relative;
        z-index: 2;
        float: left;
        box-sizing: border-box;
        width: 50px;
        height: 560px;
        background: {{ asset($banners[0]->image->path) }} center center no-repeat;
        border-radius: 30px;
        background-size: 400px;
        -webkit-transition: all 1s ease-in-out;
        transition: all 1s ease-in-out;
    }
</style>
@endsection

@section('content')
@include('components.simplified_menu')

<div class="container">
    <div id="profit">
        <div class="profit">
            <div class="vertical-line"></div>

            <div class="profit-and-advantage">
                <p>{{ __('admin.benefit')}}<br>
                {{ __('admin.benefits')}}</p>
                
            </div>

            <div class="about-profit-and-advantage">
                <p>{{ __('admin.help content')}}<br>
                {{ __('admin.format')}}</p>
           
            </div>

        </div>
    </div>


    <div id="what-you-got">
        <div class="what-you-got">
            <div class="you col-12 col-md-6">
                <div class="you-got">
                    <div class="vertical-line-two"></div>
                    <p>{{ __('admin.gets')}}</p>
                </div>
                <div class="about-what-you-got">
                    <div class="vertical-stick"></div>
                    <div class="inspire">
                        <div class="inspire-number">1</div>
                        <p data-aos="fade-right">{{ __('admin.VR') }}</p>
                    </div>
                    <div class="inspire">
                        <div class="inspire-number">2</div>
                        <p data-aos="fade-right">{{ __('admin.online theatre') }}</p>
                    </div>
                    <div class="inspire">
                        <div class="inspire-number">3</div>
                        <p data-aos="fade-right">{{ __('admin.online cinema') }}</p>
                    </div>
                    <div class="inspire">
                        <div class="inspire-number">4</div>
                        <p data-aos="fade-right">{{ __('admin.colorful') }}</p>
                    </div>
                    <div class="inspire">
                        <div class="inspire-number">5</div>
                        <p data-aos="fade-right">{{ __('admin.musicus') }}</p>
                    </div>

                </div>
                <div class="help_info">
                    {{ __('admin.you can')}}<br>
                    <br>
                    {{ __('admin.we believe')}}
                </div>
            </div>
            <div class="help_img">
                <!-- <img src="{{ asset('/foto/backgrounds/b1.png')}}"> -->
                <div class="frame">
                    <div class="slices" onclick="void(0);">
                        <div class="slice slice-1"></div>
                        <div class="slice slice-2"></div>
                        <div class="slice slice-3"></div>
                        <div class="slice slice-4"></div>
                        <div class="slice slice-5"></div>
                        <div class="slice slice-6"></div>
                        <div class="slice slice-7"></div>
                        <div class="slice slice-8"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="how-to-register">
    <div class="container how-to-register">

        <div class="how-to">
            <div class="vertical-line"></div>
            <div class="profit-and-advantage">
                <p>{{ __('admin.how to')}}</p>
                <p>{{ __('admin.registraation')}}</p>
            </div>
        </div>

        <div class="help">
            <div class="points">

                <div class="one point">
                    <div class="pointed">
                        <div class="point-number">1</div>
                        <p>{{ __('admin.access content')}}</p>
                    </div>
                    <div class="description">
                        <p>{{ __('admin.one point')}}</p>
                    </div>
                </div>

                <div class="one point">
                    <div class="pointed">
                        <div class="point-number">2</div>
                        <p>{{ __('admin.additional links')}}</p>
                    </div>
                    <div class="description">
                        <p>{{ __('admin.two point')}}</p>
                    </div>
                </div>

                <div class="one point">
                    <div class="pointed">
                        <div class="point-number">3</div>
                        <p>{{ __('admin.access tests')}}</p>
                    </div>
                    <div class="description">
                        <p>{{ __('admin.three point')}}</p>
                    </div>
                </div>

                {{-- <div class="one point">
                    <div class="point-number">4</div>
                    <p>Пункт четвертый</p>
                    <div class="description">
                        <p>{{ __('admin.four point')}}</p>
                    </div>
                </div> --}}

                <div class="one point">
                    <div class="pointed">
                        <div class="point-number">4</div>
                        <p>{{ __('admin.result')}}</p>
                    </div>
                    <div class="description">
                        <p>{{ __('admin.four point')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
<script>
    var slides = document.querySelectorAll('#allSlides .slide_item');
    var currentSlide = 0;
    setInterval(nextSlide, 2000);
    var classs = '';

    if (slides) {
        console.log(slides.length);
    }

    function nextSlide() {
        slides[currentSlide].className = "slide_item";
        currentSlide = (currentSlide+1) % slides.length;
        slides[currentSlide].className = "slide_item showing";
        classs = slides[currentSlide].getAttribute("class")
        console.log(currentSlide)
    }
</script>
@endsection
@include('components.footer')
@endsection