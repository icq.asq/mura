<?php $route = Route::current(); ?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mmura</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    @yield("styles")
</head>
<body>
    <div id="app">
        <v-app >
            @yield('content')
        </v-app>
    </div>

    <script>
        window._locale = '{{ app()->getLocale() }}';
    </script>
    @yield('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/tests.js') }}"></script>

</body>
</html>