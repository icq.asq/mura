<?php $route = Route::current(); ?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('foto/favicon.ico') }}">
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <title>@yield('title')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('css/table.css') }}">
	<link rel="stylesheet" href="{{ asset('css/new_change.css') }}">
    @yield("styles")

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-179617123-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-179617123-1');
    </script>

</head>
<body>
    <div id="app">
        <v-app >
            @yield('content')
        </v-app>
    </div>
    @if($route->action['as'] != 'usefull')
    <div class="socials-fixed">
        <ul class="d-none d-lg-none d-xl-block list-unstyled text-center">
            @auth
                <li class="list-item">
                    <a href="/ru/office/feed-back/send" target="_blank" class="tooltipster tooltipstered" style="padding:0px;">
                        <img class="scale-1-7" width="32px" src="{{ asset('foto/feedback.png') }}">
                    </a>
                </li>
            @endauth
            <li class="list-item">
                <a href="https://m.facebook.com/uurpaq?ref=m_nux_wizard" target="_blank" class="tooltipster tooltipstered" style="padding:0px;">
                    <img width="32px" src="{{ asset('foto/facebook.svg') }}">
                </a>
            </li>
            <li class="list-item">
                <a href="https://www.youtube.com/channel/UCFRb1r_ShVMPT-L7B_nxBMg" target="_blank" class="tooltipster tooltipstered" style="padding:0px;">
                    <img width="32px" src="{{ asset('foto/youtube.svg') }}">
                </a>
            </li>
            <li class="list-item">
                <a href="https://instagram.com/madeni_m.kz?igshid=150uo13p9eyfn" target="_blank" class="tooltipster tooltipstered" style="padding:0px;">
                    <img width="32px" src="{{ asset('foto/insta.svg') }}">
                </a>
            </li>
            {{-- <li class="list-item">
                <a href="#" target="_blank" class="tooltipster tooltipstered" style="padding:0px;">
                    <img width="32px" src="{{ asset('foto/whatsapp.svg') }}">
                </a>
            </li>
            <li class="list-item">
                <a href="#" target="_blank" class="tooltipster tooltipstered" style="padding:0px;">
                    <img width="32px" src="{{ asset('foto/vk.svg') }}">
                </a>
            </li> --}}
        </ul>
    </div>

    <noscript><div><img src="https://mc.yandex.ru/watch/67932340" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	
	<!--
 <div class="socials-fixed">
        <ul class="d-none d-lg-none d-xl-block list-unstyled text-center">
            @auth
                <li class="list-item">
                    <a href="{{ routex('office.feedback.form') }}" target="_blank" class="tooltipster tooltipstered">
                        <img class="scale-1-7" width="32px" src="{{ asset('foto/feedback.png') }}">
                    </a>
                </li>
            @endauth
            <li class="list-item">
                <a href="https://www.facebook.com/gtasbergenova" target="_blank" class="tooltipster tooltipstered">
                    <img width="32px" src="{{ asset('foto/facebook.svg') }}">
                </a>
            </li>
            <li class="list-item">
                <a href="https://www.youtube.com/channel/UC3rfysi6YdfEXJmjlJoq_Ag?view_as=subscriber" target="_blank" class="tooltipster tooltipstered">
                    <img width="32px" src="{{ asset('foto/youtube.svg') }}">
                </a>
            </li>
            <li class="list-item">
                <a href="https://www.instagram.com/diamondtas/?hl=ru" target="_blank" class="tooltipster tooltipstered">
                    <img width="32px" src="{{ asset('foto/insta.svg') }}">
                </a>
            </li>
            {{-- <li class="list-item">
                <a href="#" target="_blank" class="tooltipster tooltipstered">
                    <img width="32px" src="{{ asset('foto/whatsapp.svg') }}">
                </a>
            </li>
            <li class="list-item">
                <a href="#" target="_blank" class="tooltipster tooltipstered">
                    <img width="32px" src="{{ asset('foto/vk.svg') }}">
                </a>
            </li> --}}
        </ul>
    </div>
	-->

    @endif
    <script>
        window._locale = '{{ app()->getLocale() }}';
        window._translations = {!! cache('translations') !!};
    </script>
    @yield('scripts')
    <script src="{{ asset('js/app.js') }}"></script>
	<script>

    if (document.querySelector('.slider__img')) {
        let slider = Slider({ imgClass: "slider__img" });
        slider.init();

        function Slider({
        imgClass,
        prevClass = "prev",
        activeClass = "active",
        nextClass = "next",
        attributeName = "data-bg",
        }) {
        let slider = {};

        slider.imgClass = imgClass;
        slider.prevClass = prevClass;
        slider.activeClass = activeClass;
        slider.nextClass = nextClass;
        slider.attributeName = attributeName;

        slider.fillBackgrounds = function () {
            document
            .querySelectorAll(`.${imgClass}`)
            .forEach(
                (el) =>
                (el.style.backgroundImage = `url('${el.getAttribute(
                    attributeName
                )}')`)
            );
        };

        slider.setClasses = function () {
            let active =
            document.querySelector(`.${imgClass}.${activeClass}`) ||
            document.querySelector(`.${imgClass}`);

            active.classList.add(activeClass);

            let next =
            document.querySelector(`.${imgClass}.${nextClass}`) ||
            document.querySelector(
                `.${imgClass}.${activeClass} + .${imgClass}`
            ) ||
            document.querySelector(`.${imgClass}`);

            next.classList.add(nextClass);
        };

        slider.startSliding = function () {
            let interval;

            function anime() {
            let active = document.querySelector(`.${imgClass}.${activeClass}`);

            if (!active) {
                clearInterval(interval);
                return;
            }

            let next =
                document.querySelector(`.${imgClass}.${nextClass}`) ||
                document.querySelector(
                `.${imgClass}.${activeClass} + .${imgClass}`
                ) ||
                document.querySelector(`.${imgClass}`);

            if (next === active) {
                clearInterval(interval);
                return;
            }

            let nextNext =
                document.querySelector(
                `.${imgClass}.${nextClass} + .${imgClass}`
                ) || document.querySelector(`.${imgClass}`);

            next.classList.add(activeClass);
            next.classList.remove(nextClass);

            active.classList.remove(activeClass);
            active.classList.add(prevClass);

            setTimeout(() => {
                active.classList.remove(prevClass);
                nextNext.classList.add(nextClass);
            }, 1000);
            }

            interval = setInterval(anime, 5000);
        };

        slider.init = function () {
            this.fillBackgrounds();
            this.setClasses();
            this.startSliding();
        };

        return slider;
        }
    }
// You can also pass an optional settings object
// below listed default settings

(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(67932340, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });

AOS.init({
  // Global settings:
  disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
  startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
  initClassName: 'aos-init', // class applied after initialization
  animatedClassName: 'aos-animate', // class applied on animation
  useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
  disableMutationObserver: false, // disables automatic mutations' detections (advanced)
  debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
  throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)
  

  // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
  offset: 120, // offset (in px) from the original trigger point
  delay: 0, // values from 0 to 3000, with step 50ms
  duration: 400, // values from 0 to 3000, with step 50ms
  easing: 'ease', // default easing for AOS animations
  once: false, // whether animation should happen only once - while scrolling down
  mirror: false, // whether elements should animate out while scrolling past them
  anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation
});
</script>
</body>
</html>