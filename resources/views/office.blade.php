@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
@endsection

@section('content')
<div class="office tests_history">
    @include('components.simplified_menu')
    <div class="center container">
        <div class="foto">
            <img src="{{ $user->avatar ? asset($user->avatar) : asset('foto/pexels-daria-shevtsova-698928.jpg') }}" id='avatar' style="border-radius: 50px;">
            <div class="right">
                <div class="accountHeader">{{ __('admin.history tests') }}</div>
                <div class="points">
                    <div class='gained'>
                        {{ __('main.points_gained') }}
                    </div>
                    <div class="score">
                        <i class="fa fa-star"></i>
                        <span class='score-first'>300</span>/500
                    </div>
                </div>
                <div class="all_test">
                    <v-expansion-panels>
                        <v-expansion-panel>
                            <v-expansion-panel-header>{{ __("main.April") }}</v-expansion-panel-header>
                            <v-expansion-panel-content>
                                <div class='history-test-items'>
                                    <div class='history-test-item'>
                                        <div class="lesson-number">12</div>
                                        <div class="lesson-name">
                                            Музыкальное исскуство
                                        </div>
                                        <div class="rating">
                                            <div class="stars">
                                                <i class="fa fa-star on"></i>
                                                <i class="fa fa-star on"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            20/25
                                        </div>
                                    </div>
                                    <div class='history-test-item'>
                                        <div class="lesson-number">12</div>
                                        <div class="lesson-name">
                                            Музыкальное исскуство
                                        </div>
                                        <div class="rating">
                                            <div class="stars">
                                                <i class="fa fa-star on"></i>
                                                <i class="fa fa-star on"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            20/25
                                        </div>
                                    </div>
                                </div>
                                <div class='lower-part'>
                                    <div class="points-part">
                                        <div class="score">
                                            <span class='total'>{{ __('main.total') }}:</span>
                                            <span class='score-first'>300</span>/500
                                        </div>
                                        <v-progress-linear value='30' color="amber">
                                        </v-progress-linear>
                                    </div>

                                    <a href="">{{ __('main.download') }}</a>
                                </div>
                            </v-expansion-panel-content>
                        </v-expansion-panel>
                    </v-expansion-panels>
                </div>
                    {{-- @foreach($history as $item)
                    <div class="test">
                        <img src="{{ asset('foto/411407-PDSS93-904.jpg')}}">
                        <div class="test_info">
                            <div class="top_text">{{ $item->test->description->text ?? "nema" }}</div>
                            <div class="bottom">{{ $item->end_test }}</div>
                        </div>
                    </div>
                    @endforeach --}}
                <div class="go-back">
                    <a href="{{ routex('office') }}" class='f-1'>
                        <i class="fa fa-long-arrow-alt-left"></i> {{ __("main.back") }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    @include('components.footer')
</div>
@endsection
