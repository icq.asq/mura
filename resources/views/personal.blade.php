@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
@endsection

@section('content')
<div class="office">
    @include('components.simplified_menu')
    <div class="center container">
        <div class="foto">
            <div class="computer_block">
                <label for='file' class="pencil">
                    <i class="fas fa-pencil-alt"></i>             
                </label>
            </div>
            <img src="{{ $user->avatar ? asset($user->avatar) : asset('foto/Слой_22.png') }}" id='avatar' style="border-radius: 50px;">
            <div class="right">
                <div class="right_container">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form ref="editAccountForm" action="{{ routex('personal.post') }}" method="POST" enctype="multipart/form-data" style="margin-bottom: 20px;">
                        @csrf
                        <div class="accountHeader"> {{ __('admin.personal') }}</div>
                        <div class="info_edit">
                            <div class="top">
                                @if ($user->fio())
                                <v-tooltip bottom>
                                    <template v-slot:activator="{ on, attrs }">
                                        <span
                                            v-bind="attrs"
                                            v-on="on"
                                        >
                                            <i class="fa fa-exclamation-triangle"></i> {{ __('main.fio') }}
                                        </span>
                                    </template>
                                    <span>{{ __('main.editableForOnce') }}</span>
                                </v-tooltip>
                                @else
                                <i class="fa fa-exclamation-triangle"></i> {{ __('main.fio') }}
                                @endif
                            </div>
                            <input name='fio' placeholder="{{ __('main.fio') }}" value='{{ $user->fio() }}' @if ($user->fio()) disabled="disabled" @endif  type="text">
                        </div>
						<!-- Пункт с ИИН
                        <div class="info_edit">
                            <div class="top">
                                @if ($user->INN)                                
                                <v-tooltip bottom>
                                    <template v-slot:activator="{ on, attrs }">
                                        <span
                                            v-bind="attrs"
                                            v-on="on"
                                        >
                                            <i class="fa fa-exclamation-triangle"></i> {{ __('main.inn') }}
                                        </span>
                                    </template>
                                    <span>{{ __('main.editableForOnce') }}</span>
                                </v-tooltip>
                                @else
                                    <i class="fa fa-exclamation-triangle"></i> {{ __('main.inn') }}
                                @endif
                            </div>
                            <input name='INN' placeholder="{{ __('main.inn') }}" value='{{ $user->INN }}' @if ($user->INN) disabled="disabled" @endif type="text">
                        </div> -->
                        <div class="info_edit birthday">
                            <div class="top">
                                @if ($user->birth_date)
                                <v-tooltip bottom>
                                    <template v-slot:activator="{ on, attrs }">
                                        <span
                                            v-bind="attrs"
                                            v-on="on"
                                        >
                                        <i class="fa fa-exclamation-triangle"></i> {{ __('main.birthday') }}
                                        </span>
                                    </template>
                                    <span>{{ __('main.editableForOnce') }}</span>
                                </v-tooltip>
                                @else
                                    <i class="fa fa-exclamation-triangle"></i> {{ __('main.birthday') }}
                                @endif
                            </div>
                            <v-menu
                                ref="birth_date_menu"
                                v-model="birth_date_menu"
                                :close-on-content-click="false"
                                :return-value.sync="birth_date_menu"
                                transition="scale-transition"
                                offset-y
                                min-width="290px"
                            >
                                <template v-slot:activator="{ on, attrs }">
                                <v-text-field
                                    v-model="birth_date"
                                    readonly
                                    @if ($user->birth_date) :disabled="true" @endif
                                    class='custom-field-smallify'
                                    v-on="on"
                                    :solo="true" 
                                    :flat="true" 
                                    :dense="true"
                                ></v-text-field>
                                </template>
                                <v-date-picker 
                                    v-model="birth_date" 
                                    no-title 
                                    scrollable
                                >
                                <v-spacer></v-spacer>
                                <v-btn text color="primary" @click="birth_date_menu = false">Cancel</v-btn>
                                <v-btn text color="primary" @click="$refs.birth_date_menu.save(birth_date)">OK</v-btn>
                                </v-date-picker>
                            </v-menu>
                        </div>
                        <div class="info_edit">
                            <div class="top">
                                <v-tooltip bottom>
                                    <template v-slot:activator="{ on, attrs }">
                                        <span
                                            v-bind="attrs"
                                            v-on="on"
                                        >
                                        <i class="fa fa-exclamation-triangle"></i> {{ __('main.email') }}
                                        </span>
                                    </template>
                                    <span>{{ __('main.editableForOnce') }}</span>
                                </v-tooltip>
                            </div>
                            <input name='email' placeholder="{{ __('main.email') }}" value='{{ $user->email }}' disabled="disabled" type="text">
                        </div>
                        <div class="mobile_block">
                            <div class="info_edit">
                                <div class="top">Загрузить аватар</div>
                                <input type="file" id="file" name="avatar" accept="image/*" onchange="loadFile(event)" class='d-none'>
                            </div>
                        </div>
                        <div class="info_edit">
                            <div class="top">{{ __('admin.region') }}</div>
                            <input name='region' placeholder="{{ __('admin.region') }}" value='{{ $user->region }}' type="text">
                        </div>
                        <div class="info_edit">
                            <div class="top">{{ __('admin.city') }}</div>
                            <input name='city' placeholder="{{ __('admin.city') }}" value='{{ $user->city }}' type="text">
                        </div>
						<!-- Скрываем возможность менять категорию
                        <div class="info_edit">
                            <div class="top">{{ __('admin.category') }}</div>
                            <v-select
                                :solo="true"
                                :flat="true"
                                class='custom-field-smallify'
                                :items="[
                                    {
                                        text: '{{ __('admin.school') }}',
                                        value: 1
                                    },
                                    {
                                        text: '{{ __('admin.student') }}',
                                        value: 2
                                    },
                                    {
                                        text: '{{ __('admin.parent') }}',
                                        value: 3
                                    },
                                    {
                                        text: '{{ __('admin.elder') }}',
                                        value: 4
                                    },
                                    {
                                        text: '{{ __('admin.specialist') }}',
                                        value: 5
                                    }
                                ]"
                                :menu-props="{ contentClass: 'default-menu' }"
                                v-model="selectedUserType"
                            ></v-select>
                            <input type="hidden" name="user_type" v-model="selectedUserType">
                        </div>
						-->
                        <div class="info_edit" v-if='selectedUserType == 1'>
                            <div class="top">{{ __('main.class') }}</div>
                            <v-select
                                :solo="true"
                                :flat="true"
                                class='custom-field-smallify'
                                :items="[
                                    {
                                        text: '1 {{ __('main.class') }}',
                                        value: 1
                                    },
                                    {
                                        text: '2 {{ __('main.class') }}',
                                        value: 2
                                    },
                                    {
                                        text: '3 {{ __('main.class') }}',
                                        value: 3
                                    },
                                    {
                                        text: '4 {{ __('main.class') }}',
                                        value: 4
                                    },
                                    {
                                        text: '5 {{ __('main.class') }}',
                                        value: 5
                                    },
                                    {
                                        text: '6 {{ __('main.class') }}',
                                        value: 6
                                    },
                                    {
                                        text: '7 {{ __('main.class') }}',
                                        value: 7
                                    },
                                    {
                                        text: '8 {{ __('main.class') }}',
                                        value: 8
                                    },
                                    {
                                        text: '9 {{ __('main.class') }}',
                                        value: 9
                                    },
                                    {
                                        text: '10 {{ __('main.class') }}',
                                        value: 10
                                    },
                                    {
                                        text: '11 {{ __('main.class') }}',
                                        value: 11
                                    }
                                ]"
                                :menu-props="{ contentClass: 'default-menu' }"
                                v-model="selectedUserClass"
                            ></v-select>
                            <input type="hidden" name="user_type_class" v-model="selectedUserClass" value=" @if($user->class) {{ $user->class }} @endif">
                        </div>
                        <div class="info_edit">
                            <div class="top">{{ __('main.phone_number') }}</div>
                            <input 
                                v-mask="'+7(###) ###-##-##'" 
                                name='phone_number' 
                                placeholder="{{ __('main.phone_number') }}" 
                                v-model='userPhoneNumber' 
                                type="text"
                                value="@if($user->phone_number){{ $user->phone_number }}@endif"
                            >
                        </div>
                        <div class="info_edit">
                            <div class="top">
                                {{ __('admin.password') }}
                            </div>
                            <input name='password' required="required" v-model="accountEditPassword" placeholder="Введите ваш пароль" type="password">
                        </div>
                        <div class="info_edit">
                            <div class="top">{{ __('admin.repeat password') }}</div>
                            <input name='password_confirmation' v-model="accountEditPasswordConfirmation" required="required" placeholder="Введите ваш пароль" type="password">
                        </div>
                        <v-dialog v-model="editAccountModal" persistent max-width="290">
                            <template v-slot:activator="{ on, attrs }">
								<button type="button" v-bind="attrs" v-on="on" class="btn-yellow">
                                    {{ __('admin.save') }}
								</button>
                            </template>
                            <v-card>
                                <v-card-title class="headline">{{ __('main.warning') }}</v-card-title>
                                <v-card-text v-if="accountEditValid">{{ __('main.warningEditMessage') }}</v-card-text>
                                <v-card-text v-else>{{ __("main.accountEditNotValid") }}</v-card-text>
                                <v-card-actions>
                                    <v-spacer></v-spacer>
                                    <template v-if="accountEditValid">
                                        <v-btn color="green darken-1" text @click="editAccountModal = false">{{ __('main.cancel') }}</v-btn>
                                        <v-btn color="green darken-1" text @click="editAccountModalProceed()">{{ __("main.confirm") }}</v-btn>
                                    </template>
                                    <v-btn v-else color="green darken-1" text @click="editAccountModal = false">{{ __('main.ok') }}</v-btn>
                                </v-card-actions>
                            </v-card>
                        </v-dialog>
                    </form>
                </div>
				<a href="{{ routex('office') }}" class='f-1' style="position: absolute; bottom: 0; right: 50px; margin-bottom: 26px;">
					<i class="fa fa-long-arrow-alt-left"></i> {{ __("main.back") }}
				</a>
				<!--
                <div class="go-back" style="max-width: 280px; position: absolute;
bottom: 0;
right: 50px;">
                    <a href="{{ routex('office') }}" class='f-1 '>
                        <i class="fa fa-long-arrow-alt-left"></i> {{ __("main.back") }}
                    </a>
                </div>-->
            </div>
        </div>
    </div>
    @include('components.footer')
</div>
@endsection