@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
<link rel="stylesheet" href="{{ asset('css/musical.css') }}">
<link rel="stylesheet" href="{{ asset('css/new_change_country.css') }}">
@endsection

@section('content')
<div class="bg-musical">
    @include('components.simplified_menu')
    <div class='mt-5 musical'>
        <div id="container" class="container musicas">
            <div class="music-art">
                <div class="about-music-art">
                    <div id="music">
                        <h1 style="z-index: 2;">Тест</h1>
                        <div id="line-one"
                            style="width: 215px; height: 10px; background-color: #f5d545; margin-top: -14px; z-index: -2; ">
                        </div>
                    </div>
                    <div class="about-art">
                        <p>
                        {{ $test->$text }}
                        </p>
                    </div>
                </div>
            </div>
        </div>

        @if(isset($total))
            <div id="questions" style="margin-top: 0px;">
                <div class="quest">
                    <h3>Результат тестирования: {{ $total['correct'] }} верных ответа из {{ $total['all'] }}</h3>
                    <h3>Ваши баллы: {{ $total['balls'] }}</h3>
                </div>
            </div>
        @else
            <div id="questions" style="margin-top: 0px;">
            <div class="quest">
                <div class="questions-number">{{ __('admin.text16') }} {{ $num_next }} / {{ $count }}</div>
                <br>
                <h2>{{ $question->$text }}</h2>

                <form action="{{ routex('answer.save') }}" method="post">
                    @csrf
                    <input type="hidden" name="lesson_id" value="{{ $test->lesson_id }}">
                    <input type="hidden" name="class" value="{{ $test->subcategory_id }}">
                    <input type="hidden" name="num" value="{{ $num_next }}">
                    <input type="hidden" name="question_id" value="{{ $question->id }}">
                    <input type="hidden" name="test_id" value="{{ $test->id }}">
                    <input type="hidden" name="finish" value="{{ $finish }}">

                    @foreach($answers as $answer)
                    <div class="quests">
                        <div class="one">
                            <div class="one-logo logo-quest">
                                <p>{{ $loop->iteration==1?"a":($loop->iteration==2?"b":"c") }}</p>
                            </div>
                            <input name="answer_id" style="margin-right:7px;" id="a{{ $answer->id }}" type="radio" value="{{ $answer->id }}" {{ $loop->first?"":"" }}>
                            <p class="margin">
                                <label for="a{{ $answer->id }}" style="cursor:pointer;" class="margin">{{ $answer->$text }}</label>
                            </p>
                        </div>
                    </div>
                    @endforeach

                <hr>

                    <div class="next-questions">
                        <button style="font-size:20px;" type="submit">{{ $finish?"Завершить": __('admin.next') }}</button>
                    </div>
                </form>

            </div>
        </div>
        @endif
    </div>
    @include('components.footer')
</div>
@endsection
