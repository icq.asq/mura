@extends('layouts.main')

@section('content')
<div class="office">
    @include('components.simplified_menu')
    <div class="center container position-relative">
        <div class="circle-usefull"></div>
        <div class="d-flex usefull-container">
            <div class='usefull-title'>
                <h3>
                    {{ __('main.cinema') }}
                </h3>
            </div>
            <div class='usefull-description'>{{ __('main.cinema_description') }}</div>
            <div class='usefull-description-big'>{{ __('main.cinema_description_bigger') }}</div>
            <div class='usefull-image'>
                <img src="{{ asset('foto/411407-PDSS93-904.jpg') }}" alt="">
            </div>
        </div>
    </div>
    @include('components.footer')
</div>
@endsection