<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="la la-user nav-icon"></i> {{ __('admin.user') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('test') }}'><i class='nav-icon la la-question'></i> {{ __('admin.tests') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('question') }}'><i class='nav-icon la la-question'></i> {{ __('admin.questions') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('answer') }}'><i class='nav-icon la la-question'></i> {{ __('admin.answers') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('banners') }}'><i class='nav-icon la la-file'></i> {{ __('admin.banners') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('posts') }}'><i class='nav-icon la la-file'></i> {{ __('admin.posts') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('contacts') }}'><i class='nav-icon la la-phone'></i> {{ __('admin.contacts') }}</a></li>
