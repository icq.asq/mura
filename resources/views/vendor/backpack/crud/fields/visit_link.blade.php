<div class="form-group col-md-4">
    {{-- {{ dump($crud) }} --}}
    {{-- {{ dump($entry) }} --}}
    <label>{!! $field['label'] !!}</label>
    <div class='d-flex align-items-center h-50'>
        <input 
        type='hidden' 
        name='{{ $field['name'] }}' 
        value="{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}"
        class='spec_data'>
        <a class='spec_link' onclick='goEditId(event);'>
            {{ $field['label_link'] }} {{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}
        </a>
    </div>
    {{-- {{ dd($field) }} --}}
</div>

@if ($crud->fieldTypeNotLoaded($field))
@push('crud_fields_scripts')
<script>
    function goEditId(e) {
        e.preventDefault();
        window.location.href = jQuery(e.target).parent().find(".spec_data").val()
    }
</script>
@endpush
@endif