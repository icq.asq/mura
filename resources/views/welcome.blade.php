@extends('layouts.main')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/myStyles.css') }}">
@endsection

@section('title') {{ __('admin.title_welcome') }} @endsection

@section('content')
@include('components.mobile-menu')
<!--
<div 
    class="up page_main" 
    @mouseover="hover = true"
    @mouseleave="hover = false" 
    v-bind:style="{ 'background-image': backgroundImage }">
-->
<div class="preloader">
    <div class="loading">
        <div class="percent">0%</div>
        <label class="text">{!! __('admin.text0') !!}</label>
        <div class="progress-bar">
            <div class="progress"></div>
        </div>
    </div>
</div>
<script type="application/javascript"></script>

<div class="computer_block">
    <div class="up page_main h120vh slider">
        @include('components.simplified_menu')
        @if ($errors->any())
            <div class="container alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('notification'))
            <div class="container alert alert-danger">
                <div class="ml-5">
                    {!! session('notification') !!}
                </div>
                <?php session()->forget('notification') ?>
            </div>
        @endif
        @if(isset($banners) && $banners->count() > 0)
            @foreach($banners as $banner)
                <div class="slider__img" data-bg="{{ asset($banner->image->path) }}"></div>
            @endforeach
        @else
            <div class="slider__img" data-bg="{{ asset('foto/backgrounds/bgr1.png') }}"></div>
            <div class="slider__img" data-bg="{{ asset('foto/backgrounds/bgr2.png') }}"></div>
            <div class="slider__img" data-bg="{{ asset('foto/backgrounds/bgr3.png') }}"></div>
        @endif
        <div class="content container z-index-4" style="position:relative;">
        <div class="typewriter animated_text">
                <h1>{{ __('admin.text1') }}</h1>
            </div>
            <!--<magic-text
                :wonders="[
                    '{{ __('admin.wonders') }}'
                ]"
            ></magic-text>-->
            <div class="fat_text">
                {{ __('admin.friend') }}<br>
                {{ __('admin.welcome') }} <br>
                {{ __('admin.spirit') }} <br>
                {{ __('admin.education') }}
            </div>
            <div class="mini_text">
                {{-- {{ __('admin.activity') }}<br>
                {{ __('admin.recreating') }}<br>
                {{ __('admin.space') }}<br>
                {{ __('admin.type')}} --}}
            </div>
            <!--<a href="@auth {{ routex('office') }} @else {{ routex('register') }} @endauth" class="mt-10 d-inline-block studying">
                <button>
                    {{ __('admin.start studing') }}
                </button>
            </a>-->
            <div class="scroll">
                {{-- <i class="fas fa-arrow-down"></i>
                <span class="scroll_down">{{ __('admin.scroll down') }}</span> --}}
            </div>
            <div class="classes">

                @foreach($categories as $category)
                    <div class="resize-acceptor">
                        <a href="{{ routex('categories', $category->id) }}/">
                            <div class="class">
                                <div class="left">
                                    <img src="{{ asset($category->image) }}">
                                </div>
                                <div class="right">
                                    <span class="class_text">{{ $category->name}}   {{ __('admin.class') }}</span>
                                    <div class="down">
                                        <!--<span class="class_text_down">{{ __('admin.go to') }}</span>-->
                                        <i class="fas fa-angle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>


    <div class="container">
        <div class="up_text d-flex justify-content-start align-items-center mt-10">
            <div class="yellow_stick">
            </div>
            {{ __('admin.text2') }}
        </div>
    <!-- тут я начинаю верстать таблицу -->
    <!-- 
    Классы, которые я использовал:
    .table-col-first = класс для ячейков первого столбца
    .table-col-second = класс для ячейков первого столбца
    -->
    <table class="table table-borderless">
        <thead>
            <tr>
                <th class="text-left w-45">{{ __('admin.text3') }}</th>
                <th class="text-left w-45">{{ __('admin.text4') }}</th>
                <th class="text-center w-10">{{ __('admin.text5') }}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="table-col-first align-middle">
                    <div>
                        <img src="{{ asset('foto/table/first-icon.png') }}" alt="">
                        {{ __('admin.text6') }}
                    </div>
                </td>
                <td class="table-col-second align-middle">
                    <div class="flex">
                        <div class="icon icon-calender"></div>
                        <div>3 {{ __('admin.october') }}</div> 
                    </div>
                </td>
                <td class="text-right align-middle">
                    <div class="flex justify-content-end align-items-center">
                        <div>{{ __('admin.text8') }}</div>
                        <div class="icon icon-success"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="table-col-first align-middle">
                    <div>
                        <img src="{{ asset('foto/table/second-icon.png') }}" alt="">
                        {{ __('admin.musician') }}
                    </div>
                </td>
                <td class="table-col-second align-middle">
                    <div class="flex">
                        <div class="icon icon-calender"></div>
                        <div>10 {{ __('admin.october') }}</div> 
                    </div>
                </td>
                <td class="text-right align-middle">
                    <div class="flex justify-content-end align-items-center">
                        <div>{{ __('admin.text8') }}</div>
                        <div class="icon icon-success"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="table-col-first align-middle">
                    <div>
                        <img src="{{ asset('foto/table/third-icon.png') }}" alt="">
                        {{ __('admin.theatry') }}
                    </div>
                </td>
                <td class="table-col-second align-middle">
                    <div class="flex">
                        <div class="icon icon-calender"></div>
                        <div>17 {{ __('admin.october') }}</div> 
                    </div>
                </td>
                <td class="text-right align-middle">
                    <div class="flex justify-content-end align-items-center">
                        <div>{{ __('admin.text8') }}</div>
                        <div class="icon icon-success"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="table-col-first align-middle">
                    <div>
                        <img src="{{ asset('foto/table/fourth-icon.png') }}" alt="">
                        {{ __('admin.cinema') }}
                    </div>
                </td>
                <td class="table-col-second align-middle">
                    <div class="flex">
                        <div class="icon icon-calender"></div>
                        <div>24 {{ __('admin.october') }}</div> 
                    </div>
                </td>
                <td class="text-right align-middle">
                    <div class="flex justify-content-end align-items-center">
                        <div>{{ __('admin.text10') }}</div>
                        <div class="icon icon-error"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="table-col-first align-middle">
                    <div>
                        <img src="{{ asset('foto/table/fifth-icon.png') }}" alt="">
                        Киелі Қазақстан
                    </div>
                </td>
                <td class="table-col-second align-middle">
                    <div class="flex">
                        <div class="icon icon-calender"></div>
                        <div>31 {{ __('admin.october') }}</div> 
                    </div>
                </td>
                <td class="text-right align-middle">
                    <div class="flex justify-content-end align-items-center">
                        <div>{{ __('admin.text10') }}</div>
                        <div class="icon icon-error"></div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- тут я заканчиваю верстать таблицу -->  
    </div>

    <div class="center ">
        <div class="proekt">
            <div data-aos="fade-down-right" class="left" style="width: 900px;">
                <div class="play-text">{{ __('admin.text14') }}</div>
                <a href="#ModalOpen" class="play-button flex justify-content-center align-items-center">
                    <img src="{{ asset('foto/play-icon.png') }}" alt="" class="play-icon">
                </a>
            </div>
            <div data-aos="fade-left" class="right">
                <div class="up_text">
                    <div class="yellow_stick">
                    </div>
                    {{ __('admin.about project') }}
                </div>
                <div class="down_text">
                    {{ __('admin.direct') }}
                    {{ __('admin.educations') }}
                    {{ __('admin.art') }}
                    {{ __('admin.Kazah') }}
                    {{-- {{ __('admin.kazah') }}
                    {{ __('admin.standart') }} --}}
                </div>
            </div>
        </div>
        <div class='timeline-wrapper mt-10 container'>
            <div class="circle-timeline"></div>
            <div class="up_text d-flex justify-content-center align-items-center p-l-250">
                <div class="yellow_stick">
                </div>
                {{ __('admin.how this') }}
            </div>
            <v-timeline  data-aos="fade-up"
                class='timeline-main mt-10'
                :align-top="true"
                reverse="reverse" 
            >
                <v-timeline-item 
                    class="text-right"
                >
                <template v-slot:icon data-aos="fade-right">
                    </template>
                    <span class='shadow-counter' data-aos="fade-right">01</span>
                    <h5 class='mt-1' data-aos="fade-right">{{ __('admin.registration') }}</h5>
                    <p data-aos="fade-right" style="font-size:21px;">{{ __('admin.field')}}</p>
                </v-timeline-item >
                <v-timeline-item data-aos="fade-left">
                    <span class='shadow-counter left' data-aos="fade-left">02</span>
                    <h5 class='mt-1' data-aos="fade-left">{{ __('admin.access') }}</h5>
                    <p data-aos="fade-left" style="font-size:21px;">{{ __('admin.familiarization')}}</p>
                </v-timeline-item>
                <v-timeline-item class="text-right" data-aos="fade-right">
                    <span class='shadow-counter' data-aos="fade-right">03</span>
                    <h5 class='mt-1' data-aos="fade-right">{{ __('admin.access_test')}}</h5>
                    <p data-aos="fade-right" style="font-size:21px;">{{ __('admin.full')}}</p>
                </v-timeline-item>
                <v-timeline-item data-aos="fade-right">
                    <span class='shadow-counter left' data-aos="fade-left">04</span>
                    <h5 class='mt-1' data-aos="fade-left">{{ __('admin.results') }}</h5>
                    <p data-aos="fade-left" style="font-size:21px;">{{ __('admin.prizes')}}</p>
                </v-timeline-item>
            </v-timeline>
        </div>
    </div>
</div>

<div class="mobile_block">
    <div class="up page_main h120vh slider">
        @include('components.simplified_menu')
        @if ($errors->any())
            <div class="container alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('notification'))
            <div class="container alert alert-danger">
                <div class="ml-5">
                    {!! session('notification') !!}
                </div>
                <?php session()->forget('notification') ?>
            </div>
        @endif
        <div class="slider__img" data-bg="{{ asset('foto/backgrounds/bgr1.png') }}"></div>
        <div class="slider__img" data-bg="{{ asset('foto/backgrounds/bgr2.png') }}"></div>
        <div class="slider__img" data-bg="{{ asset('foto/backgrounds/bgr3.png') }}"></div>
        <div class="content container z-index-4" style="position:relative;">
        <div class="typewriter animated_text">
                <h1>{{ __('admin.text1') }}</h1>
            </div>
            <!--<magic-text
                :wonders="[
                    '{{ __('admin.wonders') }}'
                ]"
            ></magic-text>-->
            <div class="fat_text">
                {{ __('admin.friend') }}<br>
                {{ __('admin.welcome') }} <br>
                {{ __('admin.spirit') }} <br>
                {{ __('admin.education') }}
            </div>
            <div class="mini_text">
                {{-- {{ __('admin.activity') }}<br>
                {{ __('admin.recreating') }}<br>
                {{ __('admin.space') }}<br>
                {{ __('admin.type')}} --}}
            </div>
            <!--<a href="@auth {{ routex('office') }} @else {{ routex('register') }} @endauth" class="mt-10 d-inline-block studying">
                <button>
                    {{ __('admin.start studing') }}
                </button>
            </a>-->
            <div class="scroll">
                {{-- <i class="fas fa-arrow-down"></i>
                <span class="scroll_down">{{ __('admin.scroll down') }}</span> --}}
            </div>
            <div class="classes">

                @foreach($categories as $category)
                    <div class="resize-acceptor">
                        <a href="{{ routex('categories', $category->id) }}/">
                            <div class="class">
                                <div class="left">
                                    <img src="{{ asset($category->image) }}">
                                </div>
                                <div class="right">
                                    <span class="class_text">{{ $category->name}}   {{ __('admin.class') }}</span>
                                    <div class="down">
                                        <!--<span class="class_text_down">{{ __('admin.go to') }}</span>-->
                                        <i class="fas fa-angle-right"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="container">
        <div class="up_text d-flex justify-content-start align-items-center mt-10">
            <div class="yellow_stick">
            </div>
            {{ __('admin.text2') }}
        </div>
    <!-- тут я начинаю верстать таблицу -->
    <!-- 
    Классы, которые я использовал:
    .table-col-first = класс для ячейков первого столбца
    .table-col-second = класс для ячейков первого столбца
    -->
    <div class="table-responsive">
        <table class="table table-borderless table-responsive">
            <thead>
                <tr>
                    <th class="text-left w-45">{{ __('admin.text3') }}</th>
                    <th class="text-left w-45">{{ __('admin.text4') }}</th>
                    <th class="text-center w-10">{{ __('admin.text5') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="table-col-first align-middle">
                        <div>
                            <img src="{{ asset('foto/table/first-icon.png') }}" alt="">
                            {{ __('admin.text6') }}
                        </div>
                    </td>
                    <td class="table-col-second align-middle">
                        <div class="flex">
                            <div class="icon icon-calender"></div>
                            <div>3 {{ __('admin.october') }}</div> 
                        </div>
                    </td>
                    <td class="text-right align-middle">
                        <div class="flex justify-content-end align-items-center">
                            <!-- <div>{{ __('admin.text8') }}</div> -->
                            <div class="icon icon-success"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="table-col-first align-middle">
                        <div>
                            <img src="{{ asset('foto/table/second-icon.png') }}" alt="">
                            {{ __('admin.musician') }}
                        </div>
                    </td>
                    <td class="table-col-second align-middle">
                        <div class="flex">
                            <div class="icon icon-calender"></div>
                            <div>10 {{ __('admin.october') }}</div> 
                        </div>
                    </td>
                    <td class="text-right align-middle">
                        <div class="flex justify-content-end align-items-center">
                            <!-- <div>{{ __('admin.text10') }}</div> -->
                            <div class="icon icon-success"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="table-col-first align-middle">
                        <div>
                            <img src="{{ asset('foto/table/third-icon.png') }}" alt="">
                            {{ __('admin.theatry') }}
                        </div>
                    </td>
                    <td class="table-col-second align-middle">
                        <div class="flex">
                            <div class="icon icon-calender"></div>
                            <div>17 {{ __('admin.october') }}</div> 
                        </div>
                    </td>
                    <td class="text-right align-middle">
                        <div class="flex justify-content-end align-items-center">
                            <!-- <div>{{ __('admin.text10') }}</div> -->
                            <div class="icon icon-success"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="table-col-first align-middle">
                        <div>
                            <img src="{{ asset('foto/table/fourth-icon.png') }}" alt="">
                            {{ __('admin.cinema') }}
                        </div>
                    </td>
                    <td class="table-col-second align-middle">
                        <div class="flex">
                            <div class="icon icon-calender"></div>
                            <div>24 {{ __('admin.october') }}</div> 
                        </div>
                    </td>
                    <td class="text-right align-middle">
                        <div class="flex justify-content-end align-items-center">
                            <!-- <div>{{ __('admin.text10') }}</div> -->
                            <div class="icon icon-error"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="table-col-first align-middle">
                        <div>
                            <img src="{{ asset('foto/table/fifth-icon.png') }}" alt="">
                            Киелі Қазақстан
                        </div>
                    </td>
                    <td class="table-col-second align-middle">
                        <div class="flex">
                            <div class="icon icon-calender"></div>
                            <div>31 {{ __('admin.october') }}</div> 
                        </div>
                    </td>
                    <td class="text-right align-middle">
                        <div class="flex justify-content-end align-items-center">
                            <!-- <div>{{ __('admin.text10') }}</div> -->
                            <div class="icon icon-error"></div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- тут я заканчиваю верстать таблицу -->  
    </div>
    <div class="center">
        <div class="proekt2">
            <div data-aos="fade-down-right" class="left">
                <div class="play-text">{{ __('admin.text14') }}</div>
                <a href="#ModalOpen" class="play-button justify-content-center align-items-center">
                    <img src="{{ asset('foto/play-icon.png') }}" alt="" class="play-icon">
                </a>
            </div>
        </div>
        <div class="proekt">
            <div class="right">
                <div class="up_text">
                    <div class="yellow_stick">
                    </div>
                    {{ __('admin.about project') }}
                </div>
                <div class="down_text">
                    {{ __('admin.direct') }}
                    {{ __('admin.educations') }}
                    {{ __('admin.art') }}
                    {{ __('admin.Kazah') }}
                    {{-- {{ __('admin.kazah') }}
                    {{ __('admin.standart') }} --}}
                </div>
            </div>
        </div>


        <div class='timeline-wrapper mt-10 container'>
            <div class="circle-timeline"></div>
            <div class="up_text d-flex justify-content-left align-items-left p-l-250">
                <div class="yellow_stick">
                </div>
                {{ __('admin.how this') }}
            </div>
            <v-timeline  data-aos=""
                class='timeline-main mt-10'
                :align-top="true"
                reverse="reverse" 
            >
            <v-timeline-item class="">
                <template v-slot:icon data-aos="">
                    </template>
                    <span class='shadow-counter left' data-aos="">01</span>
                    <h5 class='mt-1' data-aos="">{{ __('admin.registration') }}</h5>
                    <p data-aos="" style="font-size:16px;">{{ __('admin.field')}}</p>
                </v-timeline-item>
                <v-timeline-item data-aos="">
                    <span class='shadow-counter left' data-aos="">02</span>
                    <h5 class='mt-1' data-aos="">{{ __('admin.access') }}</h5>
                    <p data-aos="" style="font-size:16px;">{{ __('admin.familiarization')}}</p>
                </v-timeline-item>
                <v-timeline-item class="" data-aos="">
                    <span class='shadow-counter left' data-aos="">03</span>
                    <h5 class='mt-1' data-aos="">{{ __('admin.access_test')}}</h5>
                    <p data-aos="" style="font-size:16px;">{{ __('admin.full')}}</p>
                </v-timeline-item>
                <v-timeline-item data-aos="">
                    <span class='shadow-counter left' data-aos="">04</span>
                    <h5 class='mt-1' data-aos="">{{ __('admin.results') }}</h5>
                    <p data-aos="" style="font-size:16px;">{{ __('admin.prizes')}}</p>
                </v-timeline-item>
            </v-timeline>
        </div>
    </div>
</div>

<!-- Модальное окно -->
<div id="ModalOpen" class="Window">
    <div>
        <a href="#close" title="Закрыть" class="close">x</a>
        <iframe class="window-video" src="https://www.youtube.com/embed/XXcM0l44ipo?rel=0&showinfo=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

@include('components.footer')
@endsection

@section('scripts')
<script>

    let slider = Slider({ imgClass: "slider__img" });
    slider.init();

    function Slider({
    imgClass,
    prevClass = "prev",
    activeClass = "active",
    nextClass = "next",
    attributeName = "data-bg",
    }) {
    let slider = {};

    slider.imgClass = imgClass;
    slider.prevClass = prevClass;
    slider.activeClass = activeClass;
    slider.nextClass = nextClass;
    slider.attributeName = attributeName;

    slider.fillBackgrounds = function () {
        document
        .querySelectorAll(`.${imgClass}`)
        .forEach(
            (el) =>
            (el.style.backgroundImage = `url('${el.getAttribute(
                attributeName
            )}')`)
        );
    };

    slider.setClasses = function () {
        let active =
        document.querySelector(`.${imgClass}.${activeClass}`) ||
        document.querySelector(`.${imgClass}`);

        active.classList.add(activeClass);

        let next =
        document.querySelector(`.${imgClass}.${nextClass}`) ||
        document.querySelector(
            `.${imgClass}.${activeClass} + .${imgClass}`
        ) ||
        document.querySelector(`.${imgClass}`);

        next.classList.add(nextClass);
    };

    slider.startSliding = function () {
        let interval;

        function anime() {
        let active = document.querySelector(`.${imgClass}.${activeClass}`);

        if (!active) {
            clearInterval(interval);
            return;
        }

        let next =
            document.querySelector(`.${imgClass}.${nextClass}`) ||
            document.querySelector(
            `.${imgClass}.${activeClass} + .${imgClass}`
            ) ||
            document.querySelector(`.${imgClass}`);

        if (next === active) {
            clearInterval(interval);
            return;
        }

        let nextNext =
            document.querySelector(
            `.${imgClass}.${nextClass} + .${imgClass}`
            ) || document.querySelector(`.${imgClass}`);

        next.classList.add(activeClass);
        next.classList.remove(nextClass);

        active.classList.remove(activeClass);
        active.classList.add(prevClass);

        setTimeout(() => {
            active.classList.remove(prevClass);
            nextNext.classList.add(nextClass);
        }, 1000);
        }

        interval = setInterval(anime, 5000);
    };

    slider.init = function () {
        this.fillBackgrounds();
        this.setClasses();
        this.startSliding();
    };

    return slider;
    }
</script>
@endsection