<?php

use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\TestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('backpack.auth.password.reset.token');
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

Route::group([
    'prefix' => '{locale?}', 
    'where' => ['locale' => '[a-zA-Z]{2}'],
    'middleware' => 'setlocale'
], function() { 
    Route::get('/', "HomeController@main")->name('index');

    Route::get('/category/{category}/{class?}', 'CategoryController@showCategory')->name('categories');

    Route::get('/lessons/{category}/{class}/{theme}/{lesson?}', 'LessonController@showLesson')->name("lessons");

    Auth::routes(['verify' => true]);

    Route::group([
        'middleware' => ['auth', 'verified:index'],
        'prefix' => 'office',
    ], function() {
        Route::get('/', 'AccountController@index')->name('office');    
        Route::get('/history', 'AccountController@history')->name('office.history');    
        Route::get('/feed-back', 'AccountController@feedback')->name('office.feedback');    
        Route::get('/feed-back/send', 'AccountController@feedbackForm')->name('office.feedback.form');    
        Route::get('/edit', 'AccountController@edit')->name('personal');
        Route::post('/edit', 'AccountController@editPost')->name('personal.post');
    });

    Route::group([
        'middleware' => ['auth', 'verified:index']
    ], function () {
        Route::get('/tests/{class}/{lesson}/{num?}', 'TestController@showTest')->name("tests.start");
        Route::get('/finish/{class}/{lesson}', 'TestController@finishTest')->name("tests.finish");
        Route::post('/answer/save', 'UserAnswerController@saveAnswer')->name('answer.save');
    });

    Route::get ("/about", 'CatalogController@about')->name('about');
    
    Route::get ("/help", 'CatalogController@help')->name('help');

    Route::get ("/etiquette", 'CatalogController@etiquette')->name('etiquette');

    Route::get ("/patriot", 'CatalogController@patriot')->name('patriot');
    
    Route::get ("/usefull", 'CatalogController@usefull')->name('usefull');

});

Route::get('adminka/login', 'AccountController@adminLogin')->name('admin.login');
Route::post('adminka/login', 'AccountController@isAdmin')->name('admin.isAdmin');

Route::group([
    'middleware' => ['is.admin'],
    'prefix' => 'adminka',
], function() {
    Route::get("/", 'HomeController@showAdmin')->name("main");
    Route::resources([
        'tests' => TestController::class,
        'questions' => QuestionController::class,
        'answers' => AnswerController::class,
        'lessons' => LessonController::class,
        'media' => MediaController::class,
        'banners' => BannerController::class
    ]);
    Route::get('statistics/register/', 'StatisticsController@getRegister')->name('statistics.register');
    Route::get('statistics/balls/', 'StatisticsController@getBalls')->name('statistics.balls');
});




/*Route::prefix('adminka')->group(
    function () {
    Route::get("/", 'HomeController@showAdmin')->name("main");
    Route::resources([
        'tests' => TestController::class,
        'questions' => QuestionController::class,
        'answers' => AnswerController::class,
        'lessons' => LessonController::class,
        'media' => MediaController::class,
        'banners' => BannerController::class
    ]);
    Route::get('statistics/register/', 'StatisticsController@getRegister')->name('statistics.register');
    Route::get('statistics/balls/', 'StatisticsController@getBalls')->name('statistics.balls');
});*/

Route::get('lessons/get-lessons', 'LessonController@getLessons');

